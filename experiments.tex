\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphics}
\usepackage{graphicx}

%\usepackage[]{algorithm2e}


\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{claim}{Claim}
\newtheorem{definition}{Definition}
\newtheorem{problem}{Problem}
\newtheorem{observation}{Observation}
\newtheorem{corollary}{Corollary}
\newtheorem{fact}{Fact}

\newcommand{\h}{Hausdorff distance}
\newcommand{\fr}{Fr\'echet distance}
\newcommand{\Fr}{Fr\'echet Distance}
\newcommand{\dfr}{discrete Fr\'echet distance}
\newcommand{\Dfr}{Discrete Fr\'echet Distance}
\newcommand{\wfr}{weak Fr\'echet distance}
\newcommand{\Wfr}{Weak Fr\'echet Distance}
\newcommand{\cfr}{continuous Fr\'echet distance}
\newcommand{\fsd}{free space diagram}
\newcommand{\eps}{\varepsilon}
\newcommand{\IR}{\mathbb{R}}
\newcommand{\IQ}{\mathbb{Q}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\etal}{\ \textit {et al.}}

\newenvironment{proof}{{\noindent \bf Proof.}}{{\hfill \ensuremath \boxtimes} \vspace{1em}}

\title
{ Computing Group Diagrams for Representing Trajectories
  %\thanks{}
}

\author
{
  Maike Buchin%\footnote{RUB}
  \and
  Bernhard Kilgus
}

\date{\today}

\newcommand{\marrow}{\marginpar[\hfill$\longrightarrow$]{$\longleftarrow$}}
\newcommand{\remark}[3]{\textsc{#1 #2:} \marrow\textsf{#3}}
\newcommand{\maike}[2][says]{\remark{Maike}{#1}{#2}}
\newcommand{\bernhard}[2][says]{\remark{Bernhard}{#1}{#2}}
\newcommand{\comment}[1]{\marrow\textsf{#1}}
\newcommand{\todo}[2][]{\remark{TODO}{#1}{#2}}
\newcommand{\takeout}[1]{} %or #1


\begin{document}

\maketitle

\begin{abstract}
We propose the group diagram as a representation for multiple trajectories,
and give hardness results and algorithms for computing it.
\end{abstract}

\section{Introduction}
Given a set of trajectories of entities traveling together over some parts, we propose the group diagram as a compact representation.

\todo{extend introduction}

%A group of entities simply refers to a set of entities. We typically assume these to have similar movement paths, either in time and space (i.e. traveling together), or simply in space (i.e., following the same route). More formally, the distance between the trajectories is small, depending on some distance measure (spatial or spatio-temporal). Here distance within a group can mean either the (maximum) pairwise distance, or all entities are linked by a small distance (density-connected).
%%
%To represent a group of entities, we see (at least) three possibilities:
%\begin{itemize}
%  \item individually: store all $n$ trajectories
%  \item representative as one group: store a representative trajectory and possibly a size/shape at each time stamp
%  \item representative as group system: store several groups in one graph, where edges represent representative trajectories
%\end{itemize}
%A group system represents several (overlapping) groups, an example is the grouping structure. For defining a group system we need to choose the distance measure, the inner group distance, and how representatives of a group may be chosen (e.g., freely, using vertices or parts of edges, or a simplification of the input).
%
%We will consider the following tasks:
%detecting groups, representing groups, computing similarity, segmentation, and detecting intr- and inter-group patterns.
%For detecting groups several algorithms have been proposed, such as flocks, swarms, etc. Hence we concentrate on the following tasks. First, we discuss representations of groups, and how to compute these.

%One group can be represented by a ``middle'' trajectory and a size/shape at each point in time.

%% overview
In the following we first formally define the group diagram and then consider computing it using the \fr, equal and similar time distance.

\section{Definition}

For representing several (overlapping, i.e., splitting and merging) groups we define a group-diagram.
We propose the following general definition:
\begin{definition}\label{def:gd}
 A group diagram is a (directed) geometric graph that represents all input trajectories. We say the graph represents a trajectory if there is a similar path (under some similarity measure) in the graph.
 We say a group diagram is minimal if it has minimal total edge length, minimal total number of breakpoints along the edges or a minimal number of edges.
\end{definition}


\section{Experiments}
To investigate the usability of our definition of a group diagram and the described algorithms for real world data we perform experiments on data of migrating birds using the Python programming language. All the data is provided by the Max-Planck-Institute for Ornithology on \textit{movebank.org}. We use the Python library \textit{gmplot} to directly draw the edges of the group diagram on \textit{google maps}. 

\subsection{White Fronted Geese}
The data set \textit{Geese MPIO Kolguev2016} contains the data of several families of white fronted geese (anser albifrons), where all members of one family share the same two letters at the beginning of their individual name. Tracking starts in November 2016 and data is still being collected (June 2017). In particular the data contains the spring migration from North Western Europe to North Eastern Europe which we investigate here. Most of the families consists of five individuals (mother, father and three young animals). Each data point provides information of time and position and a numerical error value of the position. Thus, we can exclude data points with a high position error for our investigations.\\
The distance between two entities is computed based on their positions on the earth's surface only. The altitude is not taken into account. For migrating white fronted geese this is a reasonable approach as they are flying constantly flapping on a more or less constant altitude. 
For species using updrafts for gaining height and gliding afterwards a distance measure based on position and altitude is more appropriate. \\
\bernhard{Andrea, does this sound reasonable for you?}


A group diagram shows when a subgroup (also one entity only) separates from the rest of the group (or from a subgroup) and when a subgroup joins another subgroup. In the data of the migrating white fronted geese these split and merge events only happen on a relatively small scale compared to the total geographic extent of the data. If a family member splits from the rest of the family on a larger scale it is very unlikely that a reunion of this member with the rest of the family or parts of the family occurs. This phenomenon is typical for families of migrating birds. If, for instance, a young animal separates from its parents this separation is final. However, detecting and visualizing split and merge events on a smaller scale is an interesting application of the group diagram. When is the family flying close together, so that it can be represented by only one member and when do we need more representatives? For which distance does the family stay "together" the whole time or a given percentages of the whole observation period? 

\subsubsection{Bounded Equal-Time and Similar-Time Distance}
We compute group diagrams based on bounded equal-time and $\alpha$-similar time distance as similarity measure. 
From the data set \textit{Geese MPIO Kolguev2016} we look at the Wo-family with the individuals (Wolka F, Wouter M, Wolde, J Wolodja  J, Wobke J) and investigate their migration from Denmark to Lithuania. We exclude the young animal "Wobke" as there is no data after the 30th December. \\
What are reasonable distance thresholds for computing group diagrams for the equal- and similar-time distance criteria?  First note, that the family is migrating within a swarm of several families and individuals. Choosing a small distance of about 10 meters the resulting group diagram illustrates where the family, or some individuals of the family where traveling close together within the swarm. Depending on the size of the swarm a larger distance  threshold (extent of the swarm) is still reasonable for defining "traveling together" as the individuals might only stay at different positions within the swarm. One can choose a distance greater than the extend of the swarm to detect where individuals or subgroups split from the swarm or merge with the swarm. \bernhard{Andrea, do swarms of white fronted geese have a typical average extent or does the size and extent differ a lot for each swarm?}\\

As mentioned above, on a larger scale, the family stays together while migrating from Denmark to Lithuania, see Figure \ref{fig:WFamily_640_total} where the distance threshold was set to 640 meters. The whole diagram basically consist of one migration route and only for a few time stamps more than one representative is needed.\\ 

\begin{figure}
\includegraphics[scale=0.4]{Figures/Experiments/WFamily/640_0_total}
\caption{Group diagram for bounded equal-time distance with a distance threshold of 640 m}
\label{fig:WFamily_640_total}
\end{figure}

We did computed the group diagram for the migration of the Wo-Family for distances $d = [10,20,40,80,160,320,640]$ meters and for each distance we set the allowed time shift to $\alpha = [0,10,30]$ seconds.\\

\paragraph{Average Number of Representatives}
In Figure \ref{fig:distance_vs_number_representatives} the average number of representatives needed to represent the whole family (left) and the percentage of time stamps when one representative is enough to represent the whole family (right) is plotted against the distance threshold for bounded equal-time distance as similarity criteria.  Observations:\\
- three different, decreasing slopes in the curve "percentage of time traveled together" (between 10 and 80 m, between 80 and 320 m and between 320 and 640 m)\\
- flying separated changes from systematically to exceptional\\
- curve as a sign of the extend of the swarm? 
\bernhard{Andrea, my idea is that the curves might be an indication for the extent of the swarm. For distances from 10 to 80 m the percentage of time when the family travels together (represented by one representative) increases quickly, between 80 m and 300 m slower, but quicker than between 320 m. Might it be possible to conclude that the extent of the swarm varies from 80 to 300 m? For larger distance thresholds more than one representative is needed only when one entity splits from the swarm.} 

\begin{figure}
\includegraphics[scale=0.45]{Figures/Experiments/WFamily/plot_distance_number_representatives_time_together}
\caption{average number of representatives needed (left) and percentages of time when the family was flying together (left) for different distance thresholds }
\label{fig:distance_vs_number_representatives}
\end{figure}

\paragraph{Equal-Time Distance vs. Similar-Time Distance}
Figure \ref{fig:10m_no_timeshift_vs_timeshift} gives an example where allowing a time shift while maintaining the distance threshold leads to a smaller diagram in terms of number of representatives needed. The left part shows a part of the group diagram for a distance threshold of 10 m. The upper part shows the group diagram for equal-time distance and the lower part the diagram for an allowed time shift of maximal 10 seconds. In this group diagram only two representative is enough whereas for bounded equal-time distance four representatives are needed. The right part of the figure illustrates the effect on the diagram if the allowed distance is doubled. In this case we obtain almost the same diagram as for allowing a small time shift. For the distance of 20 m and a time shift of 10 seconds the whole family can be represented by only one representative.

\begin{figure}
\includegraphics[scale=0.08]{Figures/Experiments/WFamily/distance10_20_difference_timeshift}
\caption{One sequence of the group diagram for distances $d=10$m (left) and $d=20$m (right) with no time shift allowed (upper pictures) and a time shift of 10 seconds (lower pictures)}
\label{fig:10m_no_timeshift_vs_timeshift}
\end{figure}

Figure \ref{fig:distance_vs_number_representatives_equal_similar} shows the average number of representatives needed for different distances simultaneously for equal-time distance and $\alpha$-similar-time distance for an allowed time shift of 10 seconds (left hand side). On the right hand side of the figure the difference of the number of representatives needed for equal- and similar-time distance is shown. For small distances (10 m and 20 m) the impact of allowing a time shift of 10 seconds is greater than the impact of doubling the distance. As distance is growing it becomes the dominating parameter for the size of the group diagram. 
Possible reasons:\\
- hint for V-Formation, "line-formation" instead of "parallel-formation"\\
- recognizing flying in V-formation: small diagram for similar-time distance and big diagram for equal-time distance for same distances in both case\\
\bernhard{Andrea, also for these ideas it would be great to know if the swarms have a typical average extend and maybe also a typical average angle in their V-formation}

\begin{figure}
\includegraphics[scale=0.4]{Figures/Experiments/WFamily/plot_distance_number_representatives_equal_similar_time}
\caption{distance vs. the average number of representatives needed for equal-time distance and a time shift of 10 seconds}
\label{fig:distance_vs_number_representatives_equal_similar}
\end{figure}

\paragraph{Migration Above Water and Over Solid Ground}
During the migration one can observe that when the family is flying over surfaces of water they tend to separate more from each other than while flying over solid ground. One example of this phenomenon is shown in Figure \ref{fig:split_merge_over_water} for a bounded equal-time distance of 160 meters. 

\begin{figure}
\includegraphics[scale=0.65]{Figures/Experiments/WFamily/160_0_over_lake}
\caption{Family members split when flying over a lake and merge after passing the lake}
\label{fig:split_merge_over_water}
\end{figure}


\paragraph{Further observations}
- for 10 m the representatives representing all entities is changing $\rightarrow$ For this distance pairwise distance (possible to choose just the first representative in the list) is smaller than threshold, but distance to one representative is below the threshold. 

\section*{Discussion}


\end{document} 