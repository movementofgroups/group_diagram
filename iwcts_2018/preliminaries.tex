\section{Preliminaries}\label{sec:prelim}

First, we introduce the terminology we use and then define group diagrams.
%\paragraph*{Terminology}
%We will use the following terms:
A \emph{trajectory} is an ordered sequence of positions in time and space.
A \emph{subtrajectory} refers to an arbitrary but connected part of the trajectory starting and ending at arbitrary points along the trajectory. We call a subtrajectory starting and ending at vertices a \emph{restricted} subtrajectory. A subtrajectory that is not necessarily restricted is an \emph{arbitrary} subtrajectory. A \emph{segment} refers to a subtrajectory consisting only of two consecutive vertices of the trajectory and the linear interpolation between them.

A \emph{cluster} is a set of trajectories that are similar (under some similarity measure) to one \emph{representative} of the cluster.
A cluster representative is thus a subtrajectory within a cluster to which all other subtrajectories in the cluster have distance at most $d$. A \emph{cluster curve} is a subtrajectory within some cluster. % (not necessarily starting and ending at vertices).
Given a cluster representative $\tau$ we denote by $c(\tau)$ the set of all cluster curves of the corresponding cluster. For a set of representatives $A$, $c(A)$ is the union of all cluster curves represented by a representative in $A$.

We propose the following general definition for a group diagram:
\begin{definition}\label{def:gd}
 A group diagram (GD) is a geometric graph with vertices augmented by a temporal component, that represents all input trajectories $\mathcal{T}$.
 We say the graph represents a trajectory $T \in \mathcal{T}$ if there is a similar path $P$ in the graph,
 that is $T$ and (the geometric representation of) $P$ are similar under a given similarity measure.
 We say a GD is minimal if it is minimal in size, either with respect to its number
 of edges or the total length of edges.
\end{definition}

The definition allows to make several choices:
how to represent vertices and edges of the GD, the similarity measure, and whether to minimize the number or the total (geographical) length of the edges.
%
We consider GD that are built from the input trajectories, i.e., edges of the GD are represented by subtrajectories of the input and two edges share a vertex if the endpoints of the corresponding subtrajectories are within distance $d$ from each other. Endpoints of edges with no $d$-distance neighbor have degree one.
Vertices in the graph are thus embedded as the set of endpoints of incident edges.
We will use such graphs in the following.
Note that we could transform these into planar embedded graphs, for instance by connecting all points of a point set $S$ of a vertex to the midpoint of $S$.
% or its convex hull.
Note also that we require representatives to consist of parts of the input as these give realistic representatives. % (not requiring this would allow representatives to go through impassable regions).

As similarity measure, we consider three popular measures on trajectories: the \fr, equal-, and similar-time distance. 
All of these measure the similarity as the maximum distance under certain alignments of the temporal component: 
equal-time aligns equal time stamps, similar-time allows a bounded shift in time, and the \fr allows arbitrary shifts but still respecting the ordering in time (see the respective sections for precise definitions).
%
The generality of our definition allows to apply it in different settings, e.g. to entities moving at the same or at different times.

Minimizing the number of edges or their total length seems intuitively reasonable.
However, both can lead to unwanted effects and we need to make further requirements to prevent these.
The effects are the following (see Figure~\ref{fig:strangeEffects} for an illustration):
\begin{itemize}
\label{list:additional_conditions}
  \item When minimizing number of edges, if two trajectories are close to each other only partially, a smallest possible group diagram consist of the two trajectories rather than representing their similar parts with a joint edge.
  \item When minimizing length, even for a single trajectory the shortest representative would be a series of points at distance $2d$.
\end{itemize}

%Figure \ref{fig:strangeEffects} illustrates these effects. A minimal group diagram for the given trajectories is highlighted in red.

%\begin{figure}
%\includegraphics[scale=0.6]{Figures/strange_effects}
%\centering
%	\caption{Unintuitive group diagrams (highlighted in red) fulfilling the criteria of minimal edge length (left) and minimal number of edges (right).}
%	\label{fig:strangeEffects}
%\end{figure}

\begin{figure}
\includegraphics[width=0.47\textwidth]{Figures/strange_effects}
	\caption{Unintuitive GD with minimal number of edges (left) and minimal edge length (right).}
	\label{fig:strangeEffects}
\end{figure}

To prevent these effects, we introduce the following requirements for the two different minimization goals:
\begin{itemize}
  \item For minimizing number of edges, we require that the minimality criterion is fulfilled also for every local part of the input:
Given a GD $\mathcal{G}$, an arbitrary subtrajectory $\tau$ of one of the representatives in $\mathcal{G}$, and the corresponding cluster $c(\tau)$. We use $G_\tau^* := c(c(\tau))$ to denote the union of all clusters represented by a curve in $c(\tau)$.
We intersect $G_\tau^*$ with the GD $\mathcal{G}$ to obtain all representatives $A_\tau := \mathcal{G} \cap G_\tau^*$.
The \emph{local minimality criterion} now requires that for each $\tau$ as described above the resulting set $A_\tau$ is a minimal representation for $c(A_\tau)$.

\item For minimizing length, we require that no clusters are artificially split up to reduce the length.
   Formally, we require that no subgraph of the GD can be contracted, i.e., substituted by a subgraph of smaller size (but possibly larger length).
\end{itemize}

In the following, we always consider the problem with the corresponding additional requirements.
% depending on whether we measure size by number or length of edges.

\subsection{Computational Complexity}\label{sec:complexity} %

By a reduction from the known NP-complete \textsc{Dominating-Set} problem for a grid graph \cite{unit-disk}, we can show that the decision problem for GD is NP-complete for all variants we consider:

\begin{theorem}\label{theorem:NP_fr}
Given an integer $l$, deciding whether there exists a GD of size $l$ is NP-complete for both $l$ denoting the edge length and $l$ denoting the edge number, and for both \fr\ and equal-time distance as similarity criteria.
\end{theorem}

The proof of this theorem is deferred to the full version of this paper due to space constraints. 
Due to the NP-hardness we turn to approximation algorithms in the following sections. 

%\begin{figure}[b]
%\includegraphics[width=1\columnwidth]{Figures/reductionSketch}
%\caption{A grid graph and its corresponding placement of segments. The solution of the \textsc{Dominating Set} and the \textsc{Group Diagram} instances are highlighted in red.} %\maike{add some labels}
%\label{fig:reductionSketch}
%\end{figure}


%In the following sections we give approximation algorithms and their experimental evaluation on a real data set.
