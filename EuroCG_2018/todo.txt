Def 2.1 "same cluster curves" sollte noch eindeutiger formuliert werden, gemeint ist ja, dass der  representative tau nicht zu tau' extended werden kann, so dass das cluster von tau' genau aus "extended curves" aus dem cluster von tau besteht. 

Lemma 2.2 -- ist jetzt als Observation im Text (wie bei SEA) und stattdessen Lemma 4 aus SEA an seiner Stelle. 
in der SEA version: vielleicht figures of relevant and irrelevant clusters hinzuf�gen? 
Ausserdem: so wie ich es verstehe, gilt Lem 2.2/4 bei der L�ngenminimierung nur wieder bis auf den additiven Faktor 2d. Das sollte unbedingt noch in die Formulierung aufgenommen werden. 

Lemma 2.3 -- Formulierung "we can report all" ersetzen durch z.B. "all relevant cluster ... start and end at ... "

Lemma 2.5 -- habe ich jetzt als Observation/Remark formuliert (da kein Beweis)

Thm 2.8 -- "a sufficient number" -- besser genauer angeben und damit auch die (k^5 + k^4 logn)n erkl�ren. Vielleicht ebenfalls im Text nochmal wiederholen wie die einzelnen SET Cover instances das Gesamte l�sen. 

Def 2.9/SEA 12 and 13  -- die Definition ist f�r subtrajectory zu segment, und sollte noch f�r trajectory zu trajectory verallgemeinert werden (evtl. nur ersteres formal in der EuroCG version und zweiteres im Text)

Nur in der SEA version:  

Lemma 5 -- der erste Satz ist vom Englisch her nicht sch�n (viele Verben aneinander gereiht). Vielleicht brechen oder anders umformulieren? 

Im Main text (erste 12 Seiten) fehlt mE ein Hinweis auf Lemma 21