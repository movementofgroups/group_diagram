\documentclass{beamer}
%\usetheme{Rub}
\usepackage{tikz}
\usepackage[english]{babel}
\usepackage{listings,proba,amsmath,clrscode,multimedia,wrapfig,graphicx,subfigure,caption,xspace}
\usepackage{color}
\usepackage{transparent}
\definecolor{light-gray}{gray}{0.7}
\graphicspath{{figures/}}
\usetikzlibrary{shapes,backgrounds,positioning}
\newcommand{\fr}{Fr\'echet distance}
\newcommand{\Fr}{Fr\'echet Distance}

\mode<presentation>
{
	\usetheme[norubfonts,inversecolor]{RUB}
	%\usefonttheme[onlymath]{serif}
	%\setbeamercovered{transparent}
}

\renewcommand{\S}{\ensuremath{\mathcal{S}}\xspace}

\newtheorem{obs}[theorem]{Observation}
\newtheorem{defi}[theorem]{Definition}
%\newtheorem{problem}[theorem]{Problem}

\title{Group Diagrams for Representing Trajectories\\}
\author[Bernhard Kilgus]{{Maike Buchin, \textbf{Bernhard Kilgus}}}
%\institute{Ruhr-Universit\"at Bochum}

\date{\today}
%\date{March 9, 2015}


\begin{document}

% Titlepage
\begin{frame}
\titlepage
\end{frame}

% Outline
%\begin{frame}<beamer>
%  \frametitle{Outline}
%%  \setcounter{tocdepth}{1}
%  \tableofcontents
%\end{frame}


\section{Introduction}

\begin{frame}
\frametitle{Motivation}
\begin{itemize}
\item<1-> Increasing amount of movement data collected\\
$\rightarrow$ Demand for compact, information preserving representation 
\item<2-> Group Diagram as a representation for moving groups 
\end{itemize}
\centering
\vfill
\includegraphics[scale=0.3]{figures/movement}

\end{frame}

\begin{frame}
\frametitle{Definition}
\begin{itemize}
\item<1-> A group consists of $k$ entities. The movement of one entity is given by a trajectory.
\item<2-> A trajectory is a sequence of time-stamped locations with linear interpolation between two consecutive time stamps.
\end{itemize}
\vspace{0.5cm}
\onslide<2->
\includegraphics[scale=0.8]{figures/traj} 

\end{frame}

\begin{frame}
\frametitle{Definition}
\begin{defi}
A \textbf{minimal} group diagram (GD) is a minimal geometric \textbf{graph} with subtrajecories as edges that represents all input trajectories $\mathcal{T}$.
The graph represents a trajectory $\tau \in \mathcal{T}$ if there is a \textbf{similar} path $P$ in the graph under a given similarity measure.
\end{defi}
\centering
\vspace{0.6cm}
\only<1>{\hspace{0.1cm}\includegraphics[scale=1.3]{figures/groupDiagramInput}}
\only<2>{\includegraphics[scale=1.3]{figures/groupDiagram}}
\only<3>{\includegraphics[scale=1.3]{figures/groupDiagramWithVertices}}
\vspace{0.35cm}
\only<4->{

\begin{itemize}
 %\color{light-gray} or free choice of edges 
%begin{itemize}
%\item \color{light-gray}Free choice of edges
%\item 
%\end{itemize}
\item<4-> \textbf{Minimality criteria:} Minimal number of edges and minimal total length of edges
%\begin{itemize}
%\item minimal number of edges
%\item minimal total length of edges
%\end{itemize}
\item<5-> \textbf{Distance measures:} \fr, equal-time distance and similar-time distance
%\begin{itemize}
%\item \Fr
%\item Equal-Time Distance
%\item 
%\end{itemize}
\item<6-> \textbf{NP-hard} for all variants
\vspace{\fill}
\end{itemize}
}
\end{frame}

%\begin{frame}
%\frametitle{Variants}
%\begin{itemize}
%\item<2-> Edges of the GD
%\begin{itemize}
%\item Free choice of edges
%\item Using subtrajectories from the input
%\end{itemize}
%\item<3-> Minimality criteria
%\begin{itemize}
%\item mnimal number of edges
%\item minimal total length of edges
%\end{itemize}
%\item<4-> Distance Measure
%\begin{itemize}
%\item \Fr
%\item Equal-Time Distance
%\item Similar-Time Distance
%\end{itemize}
%
%\end{itemize}
%
%\end{frame}


%\section{Complexity}
%\begin{frame}
%\frametitle{Computing a minimal GD is NP-hard}
%\begin{theorem}\label{theorem:NP_fr}
%Given an integer $\ell$, deciding whether there exists a GD of size $\ell$ is  \textbf{NP-complete} for both $\ell$ denoting the edge length and $\ell$ denoting the edge number, and for both \textbf{\fr} and \textbf{equal-time distance} as similarity criteria.
%\end{theorem}
%\onslide<2->
%Reduction from the \textsc{Dominating-Set} Problem for grid graphs:
%\vspace{0.5cm}
%\includegraphics[scale=0.7]{figures/reductionSketch}
%\end{frame}
%
\section{Approximation Algorithms}

\begin{frame}
\frametitle{\Fr\ - Algorithmic Idea}
\begin{itemize}
\item<2-> Each subtrajectory $\tau$ is a representative of a cluster of subtrajectories.\\
\centering
\includegraphics[scale=1]{figures/cluster}
\item<3-> Compute the clusters of all \emph{relevant} cluster representatives.
\item<4-> Select a subset of relevant cluster representatives as GD edges. 
\end{itemize}
\onslide<5->
\begin{center}
\includegraphics[scale=0.7]{figures/groupDiagramCluster}
\end{center}
\onslide<6>
$\rightarrow$ Construct and solve a \textsc{Set-Cover} instance  
\end{frame}

\begin{frame}
\frametitle{\Fr\ - Relevant Cluster}
\begin{defi}
A cluster representative $\tau$ which represents the cluster $c(\tau)$ is \textbf{irrelevant} if it can be extended to $\tau'$ such that $c(\tau')$ is an extension of $c(\tau)$ and if there is no merge event within distance $2d$ of the endpoint of $\tau'$. Otherwise the cluster representative is \textbf{relevant}.
\end{defi}
\vspace{0.5cm}
\only<1>{\includegraphics[scale=1]{figures/relevantCluster}}
%\only<3>{\includegraphics[scale=1]{figures/relevantCluster2}}
%\only<4>{\includegraphics[scale=1]{figures/relevantCluster3}}
\onslide<2->
\vspace{0.5cm}
\begin{lemma}
There always exists a \textbf{minimal GD solution} where edges correspond to \textbf{relevant cluster representatives}.
\end{lemma}
\vspace{1.5cm}
\end{frame}

\begin{frame}
\frametitle{\Fr\ - Segmentation}

\begin{itemize} 
\item<1-> A sufficiently fine segmentation is needed. All relevant cluster representatives must start and end at vertices.
\item<2-> Two triggers for inserting a vertex:
\begin{itemize}
\item<2-> vertex-segment comparison
\item<2-> segment-segment comparison
\end{itemize}
\end{itemize}
\vspace{\fill}
\centering
\onslide<3->
\includegraphics[scale=0.7]{figures/addingVertices}
\onslide<4->
\begin{lemma}
After two steps of inserting new vertices all relevant clusters start and end at vertices.
\end{lemma}
\end{frame}

\begin{frame}
\frametitle{\Fr\ - Constructing the \textsc{Set-Cover} Instance }
\begin{itemize}
\item<2-> Sweep two points $a$ and $b$ along each trajectory. 
\item<3-> Move $b$ to the right until the representative is relevant. Report cluster representative between $a$ and $b$ and add the corresponding cluster $C$ to $\mathcal{S}$ of the \textsc{Set-Cover} instance.
\item<4->  Set $a$ to the position of $b$. 
\vspace{1.cm}
\end{itemize}

\only<2>{\includegraphics[scale=1.2]{figures/algorithmVisualization1}}
\only<3,4>{\includegraphics[scale=1.2]{figures/algorithmVisualization2}}
\only<5>{\includegraphics[scale=1.2]{figures/algorithmVisualization3}}
\only<6>{\includegraphics[scale=1.2]{figures/algorithmVisualization4}}
\only<7>{\includegraphics[scale=1.2]{figures/algorithmVisualization5}}
\only<8>{\includegraphics[scale=1.2]{figures/algorithmVisualization6}}
\only<9>{\includegraphics[scale=1.2]{figures/algorithmVisualization7}}
\only<10>{\includegraphics[scale=1.2]{figures/algorithmVisualization8}}
\only<11>{\includegraphics[scale=1.2]{figures/algorithmVisualization9}}

\onslide<12>
\begin{theorem}\label{theorem:alg_fr}
Let $N$ be the complexity of a trajectory after two steps of vertex insertion. Given a GD instance using \fr\ we can compute in $\mathcal{O}(k^2N^3)$ time a \textsc{Set-Cover} instance, the solution of which solves the GD instance.
% of size $|\mathcal{U}| = \mathcal{O}(kN)$ and $|\mathcal{S}| = \mathcal{O}(kN)$,
\end{theorem}
\vspace{0.7cm}
\end{frame}

\begin{frame}
\frametitle{Equal-Time Distance}
\begin{itemize}
\item<2-> A path $P$ is similar to an input trajectory $\tau$ if for any time stamp $t$ in the domain of $\tau$ $dist(P(t), \tau(t)) \leq d$ (Euclidean Distance).
\vspace{0.2cm}
\only<2>{\includegraphics[scale=0.485]{figures/blank}}
\item<5->  %If $dist(\tau_1(t_i), \tau_2(t_i)) \leq d$ and $dist(\tau_1(t_{i+1}), \tau_2(t_{i+1})) \leq d$ we have $dist(\tau_1(t), \tau_2(t)) \leq d$ for all $t \in (t_i,t_{i+1})$.
Using the linearity between two consecutive time stamps to compute a GD
\end{itemize}
\vspace{0.3cm}
\only<3>{\includegraphics[scale=0.5]{figures/equalTimeGD}}
\only<4,5>{\includegraphics[scale=0.5]{figures/equalTime}}
\only<6>{\includegraphics[scale=0.5]{figures/equalTimeLinearity}}
\end{frame}


\begin{frame}
\begin{itemize}
\frametitle{Equal-Time Distance -  Segmentation and \textsc{Set-Cover} construction}
\item Compute all split and merge events.
\item<2-> Insert a vertex to every trajectory for each event time. 
\item<3-> Compute and solve a $\textsc{Set-Cover}$ instance between each two consecutive event times. 
\end{itemize}

\onslide<4->
\begin{theorem}\label{theorem:alg_fr}
For equal-time distance as similarity criterion we can compute in $\mathcal{O}((k^5 + k^4 \log n)n)$ time $\mathcal{O}(k^3n)$ \textsc{Set-Cover} instances each of size $|\mathcal{U}| = k$ and $|\mathcal{S}| = k$ the solution of which solves the GD instance.
\end{theorem}
\end{frame}

\section{Experiments}

\begin{frame}
\frametitle{Experiments}
GPS data of a family (2 adults, 2 juveniles) of migrating greater white-fronted geese
\begin{figure}
  \begin{minipage}[b]{0.35 \textwidth}
    \includegraphics[width=\textwidth]{Figures/Experiments/WFamily/Greater-White-fronted-Goose}
  \end{minipage}
  \begin{minipage}[b]{0.7\textwidth}
    \vspace{0.5cm}
    \includegraphics[width=\textwidth]{Figures/Experiments/WFamily/640_0_total}
  \end{minipage}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Experiments}
\begin{itemize}
\item<2-> How many representatives are needed?
\item<3-> Impact of allowing a time shift and/or increasing distance threshold
\item<4-> Comparing these values for movement over water and movement over solid ground
\end{itemize}
\onslide<5->
%\includegraphics[width=\textwidth]{Figures/Experiments/WFamily/representation_distance}
\includegraphics[scale=0.6]{Figures/Experiments/WFamily/160_0_over_lake}
\end{frame}

%\begin{frame}
%\frametitle{Experiments}
%Comparing these values for movement over water and movement over solid ground.
%\vfill
%\includegraphics[width=\textwidth]{Figures/Experiments/WFamily/160_0_over_lake}
%
%\end{frame}


\begin{frame}
\centering \large \bf Thank you.

\end{frame}




\end{document}
