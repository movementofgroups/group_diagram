\section{Complexity Analysis}
\label{subsec:ca}
In the first section of the appendix we give detailed proofs of the complexity results.

\begin{definition}\label{def:ds}
Given a graph $G=(V,E)$. A dominating set $D$ is a subset of $V$ such that every vertex from $V$ not in $D$ is adjacent to at least one vertex in $D$. Given an integer~$s$ the \textsc{Dominating-Set} problem is to decide whether there is a dominating set of size~$s$.
\end{definition}
\textsc{Dominating-Set} is known to be NP-complete. For the proof of Theorem \ref{theorem:NP_fr} we use a special case of \textsc{Dominating-Set} which is still NP-complete but has a stronger geometrical structure which we will use for our reduction.
\begin{definition}\label{def:grid}
Let $\mathcal{D}=\{(x,y)\in \mathbb{R}^2 | \text{$x$ and $y$ are multiples of d}\}$, where $d\in \mathbb{R}$. A grid graph is a Graph $G=(V,E)$ where $V\subset \mathcal{D}$ and there is an edge between two vertices if and only if they are within distance $d$.
\end{definition}
The following result from \cite{unit-disk} is the last building block we need.

\begin{lemma}\label{lemma:ds_on_grid}
\textsc{Dominating-Set} for grid graphs is NP-complete.
\end{lemma}

With these preliminary remarks we can prove that \textsc{Group-Diagram} is NP-complete by a reduction from \textsc{Dominating-Set} for grid graphs.

\subparagraph{Proof of Theorem \ref{theorem:NP_fr} for \fr}

%Given a geometric graph $G$ embedded in the plane with $m$ non-crossing straight-line edges and a trajectory $\tau$ with $n$ segments. Then we can decide if there exists a path in $G$ with \fr\ at most $d$ to $\tau$ in $\mathcal{O}(mn\log n)$ time. Given a group diagram $G$ consisting of $l$ segments from the input trajectory. For each intersection of two segments and for each breakpoint of a non-straight edge we add a vertex in the graph $G$ inducing a Graph $\tilde{G}$ with at most $l^2$ non-crossing straight-line edges. Therefore we can decide whether there is a similar path in $G$ for every input trajectory in $\mathcal{O}(kl^2n\log n)$ time. Thus \textsc{Group Diagram} is in NP.
First, we show that \textsc{Group Diagram} is in NP.
Let $G = (V, E)$ be a plane embedded graph with vertices embedded as points in the plane that are connected by straight-line edges and a trajectory $\tau$ with $n$ segments. Then we can decide if there is a path in $G$ with \fr\ at most $d$ to $\tau$ in polynomial time with respect to $n$ and the complexity of $G$ by performing a graph exploration for each segment of $\tau$ consecutively as described in \cite{map}. Given a group diagram consisting of $l$ segments. In addition to the endpoints of the at most $l$ edges we add a vertex to every breakpoint (endpoint of a segment). Then we still have a graph of complexity $\mathcal{O}(l)$. Therefore we can decide if there exists for every input trajectory a path in $G$ with \fr\ at most $d$ in polynomial time. Thus \textsc{Group Diagram} is in NP.

Next we show that \textsc{Group Diagram} is NP-hard by a reduction from \textsc{Do\-mi\-na\-ting-Set} for grid graphs.
Given an arbitrary grid graph $G=(V,E)$ with $V=\{v_1,v_2,...,v_k\}$. We construct an instance of \textsc{Group-Diagram} as follows. For each $v \in V$ place a horizontal segment with length less than $d$ in the plane where the coordinates of the starting point equals the coordinates of $v$. 
See Figure~\ref{fig:reductionSketch} for an example. 
If two vertices in $G$ are adjacent then the corresponding segments are at \fr\ at most $d$ as the segments have equal length and are parallel. On the other hand if two (sub)segments are within \fr\ at most $d$ then the corresponding vertices in $G$ are adjacent. This follows from the construction of the placement. If two vertices $v_1$, $v_2$ are not adjacent in $G$ any point along the corresponding segment to $v_1$ has distance greater than $d$ to any point along the segment corresponding to the segment $v_2$.

Now suppose there is a dominating set $I=\{v_{i_1},...v_{i_l}\}$ of size $l$ for $G$. Let $\hat{I} = \{s_{i_1},...s_{i_l}\}$ denote the set of the corresponding segments. For each $s \notin \hat{I}$ there is a vertex $v\in I$ adjacent to the corresponding vertex $v_s$ and it follows that $s$ and $s_v$ are within distance $d$. Therefore any input trajectory is represented by at least one ``path'' consisting of only one input segment. Reversely, given a set $J$ of (sub-)segments of size $l$ representing the whole data. Particularly for each starting point $p$ of the constructed segments there is a (sub-)segment in $J$ with starting point $q$, where $dist(p,q) \leq d$. As the starting points of the segments equal the vertices in the graph a dominating set of size $l$ for $G$ consist of vertices corresponding to the (sub-)segments of $J$.

Note that a minimal solution in this setting also fulfills the local minimality criterion. This follows from the observation that for each subtrajectory $\tau_s$ of a given trajectory $\tau$, the cluster $c(\tau_s)$ contains the same (shrunken) cluster curves as $c(\tau)$. Therefore for any representative $r$ of the group diagram the part $G^*$ relative to any subtrajectory of $r$ is represented in a minimal way by the corresponding subsegments of the group diagram.

For proving NP-hardness of \textsc{Group-Diagram} using total edge length as size we fix the distance $d=1$ and the length of the segments induced by the vertices of $G$ to $l = \frac{1}{s}$. To fulfill the additional condition for group diagrams of minimal total edge length, observe that a solution consists of whole segments as the segments are parallel and starting points are within distance $d = 1 > l$. Hence a representation of two segments within distance $d$ with possibly smaller length than $\frac{1}{s}$ must consist of more than one representative and can always be replaced by a smaller subgraph in the GD by choosing one segment as representative.  Thus a solution of \textsc{Group-Diagram} with total edge length $s$ consists of exactly $l$ segments where the corresponding vertices are a solution of \textsc{Dominating-Set} of size $s$ in $G$. Obviously a solution of \textsc{Dominating-Set} of size $s$ implies a solution of \textsc{Group-Diagram} of total edge length $s$.

\begin{figure}
\includegraphics[scale=0.7]{Figures/reductionSketch}
\caption{A grid graph and its corresponding placement of segments. The solution of the \textsc{Dominating Set} and the \textsc{Group Diagram} instances are highlighted in red.} %\maike{add some labels}
\label{fig:reductionSketch}
\end{figure}

This proves NP-hardness of the problem of finding a minimal group diagram for a given set of input trajectories. Note that NP-hardness is shown for the special case of parallel, equal size trajectories with only one segment (and in case of minimal edge length with distance $1$). 


\subparagraph{Proof of Theorem \ref{theorem:NP_fr} for equal-time distance}
Given a group diagram constructed by $l$ segments from the input data. For each of the at most $l+1$ time stamps corresponding to the start- and endpoints of the segments we insert a vertex to each input trajectory at the corresponding location. To decide if for each input trajectory there is a similar path in the group diagram we consecutively compare each segment of the new partition of an input trajectory with the segments within the group diagram which start and end at the same time stamp as the segment of the trajectory. Two segments are within equal-time distance $d$ if and only if their start- and endpoints are within equal-time distance $d$. Inserting new vertices and all comparisons can be done in polynomial time, thus \textsc{Group Diagram} with respect to equal-time similarity is in NP.

To prove the NP-completeness note that a group diagram with a limited number of segments in particular provides us with a  representation of the objects for every time stamp with limited size. Thus it suffices to prove the NP-completeness of the problem of deciding if there is representation of a given size for a single time stamp. This again can be easily proven by a reduction from \textsc{Dominating Set} (see Definition~\ref{def:ds}) by identifying the location of the objects for a single time stamp with the vertices of a graph $G$ where we insert an edge between two vertices if and only if the corresponding entities are within distance at most $d$. Evidently, a representation of the objects of size $l$ equals a dominating set in $G$ of size $l$.

\section{\Fr}
\subparagraph{Proof of Lemma \ref{lemma:rel_cluster_enough}}
For an irrelevant cluster we can always extend the representative such that the cluster is extended as well and none of the cluster curves is represented by another trajectory not in the cluster in a possibly better way w.r.t. the total size of the group diagram. In terms of minimal length it is possible to shrink a relevant cluster representative $\tau$ such that it still represents the same cluster $c(\tau)$, but obviously the amount of shrinking is bounded by $2d$ as otherwise the distance to the cluster curves is greater than $d$.

%\subparagraph{Proof of Lemma \ref{lemma:requirements_are_enough}}
%The lemma follows by combining the two requirements: If requirement 1 is fulfilled we can find the same relevant clusters by allowing only restricted representatives. With these representatives we still cover the whole data: Consider two (restricted) representatives with overlapping cluster curves $c_1$ and $c_2$, both subtrajectories of a trajectory $\tau$. Then there is a vertex $v$ in the overlap where the part of $c_1$ up to $v$ is represented by one representative and the part of $c_2$ from $v$ represented by the other representative.

%As vertices are propagated to other trajectories at a minimal distance a representative starting and ending at such newly inserted vertices can be $2d$ longer than an arbitrary representative representing the same cluster. On the other hand, the difference cannot be greater than $2d$ as a result of the two steps of vertex insertion where each change of a cluster represented by an arbitrary representative results in the existence of vertices along the underlying trajectory of the representative within distance at most $d$ to the start- and endpoint of the arbitrary representative (see Figure \ref{fig:freeRepresentative}).

\subparagraph{A Sufficiently Fine Segmentation of the Trajectories}
We insert new vertices recursively for two steps. First, we observe that only one step of inserting new vertices does not suffice.
A counterexample is shown in Figure \ref{fig:secondRecursion}$b)$, where the subtrajectories forming a minimal group diagram are highlighted in green. To realize such an optimal choice a second step of inserting new vertices (vertices are shown as crosses) is obviously necessary, since having only the disk- and square vertices an overlap occurs. Hence minimality, at least in terms of minimal length, cannot be obtained (see Figure \ref{fig:secondRecursion} $a)$).
%
Next we show that two steps of inserting new vertices are sufficient.
%we are able to find all relevant clusters where the representative and all cluster curves start and end at some vertex.

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.6]{Figures/secondRecursion}
\caption{Illustration why we need a second step of inserting vertices.}
\label{fig:secondRecursion}
\end{figure}

\subparagraph{Proof of Lemma~\ref{lemma:two_steps_are_enough}}
Consider a relevant cluster representative $\tau$ starting and ending at arbitrary points along a trajectory and consider what makes $\tau$ relevant:
\begin{itemize}
\item \textbf{Case 1:} When extending $\tau$ the distance to one of the cluster curves exceeds $d$ or the distance to another trajectory not in the cluster yet falls below $d$.
\item \textbf{Case 2:} When extending $\tau$ the distance of one of the cluster curves, $\sigma$ to another trajectory not in the cluster falls down $d$.
\end{itemize}
The event described in case 1 implies that a vertex $v$ is inserted to $\tau$ in the first propagation step. The event in case 2 implies the insertion of a vertex $w$ to $\sigma$ and, as the distance of $\sigma$ and $\tau$ is smaller than $d$, a vertex $v$ is inserted to $\tau$ in the second propagation step.
Thus we can restrict $\tau$ to start at $v$.

\begin{figure}[b]
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.7]{Figures/freeRepresentative}
\caption{The red part of the segment is a representative for all three trajectories with minimal length, but it is not relevant as it can be extended until the vertices of the segment are reached.}
\label{fig:freeRepresentative}
\end{figure}


%To proof that \emph{Requirement 2 is met} consider two relevant restricted representatives $r_1$ and $r_2$ with two overlapping cluster curves $c_1$ and $c_2$, both subtrajectories of trajectory $\tau$.
%As one part of $\tau$ is represented by another relevant cluster representative than the other part, there must be an event causing the change of the representative.
%This event triggers the insertion of a vertex $v$ to $c$ for which the distance to $r_1$ and to $r_2$ are both below or equal to $d$.


\subparagraph{Avoiding clusters with open cluster curves}
Unfortunately the insertion of new vertices may cause unintended artifacts, where clusters may be relevant by definition, but we still do not need to report them. Moreover reporting this cluster with representative $\tau_1$ and proceeding with the endpoint of the representative as starting point for the new cluster representative $\tau_2$ might be a bad choice, if there are cluster curves which are only represented by the concatenation of $\tau_1$ and $\tau_2$.
In the following we argue that we can omit clusters when there are cluster curves with no suitable restricted correspondence.
For this we introduce the notions of \emph{open} and \emph{closed} cluster curves.

\begin{definition}
Given a restricted representative $\tau$ with endpoints $a$ and $b$. A curve $s$ in $c(\tau)$ is \emph{closed} if there is a vertex $v$ with distance at most $d$ to $a$ and a vertex $w$ with distance at most $d$ to $b$. Otherwise the cluster curve is \emph{open}.
\label{def:open_closed}
\end{definition}

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[scale=0.65]{Figures/needToCheckAllSubtrajectories}
\caption{Illustration why we can omit clusters with open cluster curves}
\label{fig:needToCheckAllSubtrajectories}
\end{figure}

\begin{lemma}
\label{lemma:closed_cluster_curves}
Using a sweep algorithm along each trajectory it is sufficient to report only relevant clusters where all cluster curves are closed. Furthermore, reporting clusters with open curves possibly compromises the minimality of the resulting GD.
\end{lemma}

\begin{proof}
First, we give an example proving the second statement of the lemma. The subtrajectories of the input showed in Figure \ref{fig:needToCheckAllSubtrajectories} marked in green form a minimal GD. The representative from $v$ to $w$ is a relevant representative, because the lowest curve enters the $d-$tube of one of the cluster curves if we extend the representative. But this cluster contains open cluster curves so we continue, move to the right and do not report the cluster. If we split the representative we need an additional representative for $A$ and $B$, hence we cannot have a minimal representation of the trajectories.

Now, we prove that it is sufficient to report only clusters where all curves are closed. A cluster curve can only be \emph{open} if one endpoint of the representative is a vertex from the second step of vertex insertion. Otherwise, a cluster curve inherits a suitable vertex in the first or second step. Consider a representative $\tau$ with an open cluster curve $s$ in $c(\tau)$.
Then we can conclude that $s$ is represented only by $\tau$ or by another representative ending on a vertex of the second insertion step, otherwise there would be vertices within distance $d$ to %in the $\epsilon-$surrounding of 
the endpoint and starting point of $\tau$.

If $\tau$ is a part of a minimal solution we can extend $\tau$ until all cluster curves are closed which is the case if we reach an input vertex or a vertex from the first insertion step without having a greater total number or representatives. Let $\tau_e$ be the extension of $\tau$ where all cluster curves are closed and let $E$ denote the event which makes $\tau$ a relevant representative. We now have to ensure that we do not compromise the demanded local minimality creteria by extending the cluster. Therefore we compare the input parts $G^*$  relative to $\tau$ and relative $\tau_e$.

As there is no event within distance $d$ to the corresponding trajectory of $s$ before the closure of $s$ when extending $\tau$ we can conclude that the part of $G^*$ which is not effected by $\tau$ does not change. Recall that $s$ does not have distance below $d$ to any of the closed cluster curves in $c(\tau)$. Thus if we find a better representation for the part of $G^*$ which is effected by $E$ and delete $\tau$ as representative, we have to add at least on representative to represent the part of $G^*$ which is not affected by the event. Thus in total we do not get a smaller representation for $G^*$ relative to $\tau_e$ as by keeping $\tau_e$ as representative and represent the curve causing $E$ either by itself or by its former representative.
\end{proof}


\subparagraph{Complexity of the Trajectories} Here we want to analyze the complexity of the trajectories after two steps of inserting vertices and state some reasonable restrictions to reduce the complexity.

\begin{lemma}
\label{lemma:worst_case_complexity}
After two steps of inserting vertices the total complexity is $\mathcal{O}(k^4n^4)$ for an input $\mathcal{T}$ of size $kn$, i.e. consisting of $k$ trajectories of complexity at most $n$ each.
\end{lemma}
\begin{proof}
%With no further restriction
Every segment of a trajectory can possibly intersect all other segments from the input. Each intersection will (depending on $d$) lead to the introduction of up to four new vertices. Similarly one can construct an example where each vertex from the input is propagated to all other segments of the input data.
% (see Figure \ref{fig:tooManyIntersections}).
% Recall that the size of the input data is $kn$, where $k$ is the number of trajectories of complexity $n$.

After one step of adding new vertices the complexity of one trajectory is $\mathcal{O}(kn^2)$ since each segment inherits $\mathcal{O}(kn)$ new vertices. Thus the total number of vertices after one step is $\mathcal{O}(k^2n^2)$. After another step of inserting vertices the total complexity is $\mathcal{O}(k^4n^4)$.

Alternatively we can argue with the Free-Space Diagram (FS-Diagram). Recall that the FS-Diagram consists of $kn \times kn$ cells and each cell contains a constant number of critical points. Inserting new vertices is equivalent to propagating these critical points vertically. So each of the $kn$ lowest cell boundaries of the FS-Diagram inherits up to $kn$ critical points.
\end{proof}


%\begin{figure}
%\includegraphics[scale=0.7]{Figures/tooManyIntersections}
%\caption{Example where every segment intersects with every segment of the other trajectory}
%\label{fig:tooManyIntersections}
%\end{figure}

To reduce the complexity we state some conditions on the input data which seem to be reasonable for many applications, especially if the data does not contain many zig-zags. %is predominantly zig-zag free.
\begin{description}
  \item[C1] A disk of radius $d$ around any vertex intersects at most $c_1$ segments of another trajectory.
  \item[C2] Each segment intersects at most $c_2$ segments of another trajectory.
  \item[C3] A d-tube around one segment of length $l$ intersects at most $c_3$ segments of another trajectory.
\end{description}

\begin{lemma}
\label{lemma:complexity_trajectories}
With restrictions \textbf{C1}- \textbf{C3} and $C:= c_1+c_2+c_3$ each trajectory has complexity $\mathcal{O}(k^2C^2n)$ and computing this segmentation takes $\mathcal{O}(k^4C^2n^2)$ time.
\end{lemma}

\begin{proof}
After one step of inserting new vertices we have complexity $\mathcal{O}(k^2Cn)$, since each of the $kn$ segments/vertices causes the introduction of at most $kC$ new vertices. In the second step only new vertices of the type shown in Figure \ref{fig:insertVertices} $a)$ occur. Also in the second step we want condition \textbf{C1} to be valid. Therefore the complexity after two steps of inserting new vertices is $\mathcal{O}(k^3C^2n)$. As $k \ll n$ this is linear with respect to the dominating input size $n$.

Given $k$ trajectories of complexity $n$ the first step of inserting new vertices takes $\mathcal{O}(k^2n^2)$ time performing $k^2n^2$ vertex-segment comparisons and $k^2n^2$ segment-segment comparisons. The insertion of a vertex, triggered by a vertex-segment or segment-segment comparison takes constant time. In the the second step we have $k$ trajectories of complexity at most $kCn$. Thus inserting new vertices (type 2 vertices only) takes $k^4C^2n^2$ time.
%There might be more efficient ways of inserting the vertices as described here, but as the time complexity $\mathcal{O}(k^4C^2n^2)$ of this naive approach is not the bottleneck of the overall time complexity for setting up a \textsc{Set-Cover} instance we do not further investigate better approaches here.
\end{proof} 