\section{Introduction}\label{sec:intro}

\subsection{Definition and Terminology}\label{sec:def}
%\todo{add more motivation to the problem}
The amount of available movement data such as GPS data collected by mobile devices has increased massively during the last years. Movement ecology, for instance, is a growing field at the edge of biology and GIScience which benefits from new possibilities of tagging animals with smart devices collecting location data and several other parameters. This poses new algorithmic challenges~\cite{cma}. One of the challenges associated with the large amount of data is to represent it, such that the representation is as compact as possible but still preserves the information needed for further analysis and processing. In particular, this challenge arises for movement trajectories of several interacting entities.

In this paper we face this challenge by presenting a compact representation for data of groups of moving objects.
A moving object, called \emph{entity}, is described by its location at $n$ time stamps and a linear interpolation inbetween each two consecutive time stamps. The corresponding trajectory therefore is a polygonal line.
Given $k$ trajectories, each of complexity (number of vertices) $n$ forming one or several (overlapping, i.e., splitting and merging) groups. We introduce the \emph{group diagram} as a means of compactly representing these groups.

%\begin{figure}
%\includegraphics[scale = 1]{Figures/groupDiagram}
%	\centering
%	\caption{Illustration of merging and splitting groups and their representation as GD.}
%	\label{fig:illustration}
%\end{figure}

First let us introduce the terminology we use.
%\paragraph*{Terminology}
%We will use the following terms:
A \emph{trajectory} is an ordered sequence of positions in time and space.
A \emph{subtrajectory} refers to an arbitrary but connected part of the trajectory starting and ending at arbitrary points along the trajectory. A \emph{segment} refers to a subtrajectory consisting only of two consecutive vertices of the trajectory and the linear interpolation between them. A \emph{cluster} is a set of trajectories that are similar (under some similarity measure) to one \emph{representative} of the cluster.
%
A cluster representative is thus a subtrajectory within a cluster to which all other subtrajectories in the cluster have distance at most $d$. A \emph{cluster curve} is a subtrajectory within some cluster. % (not necessarily starting and ending at vertices).
Given a cluster representative $\tau$ we denote by $c(\tau)$ the set of all cluster curves of the corresponding cluster. For a set of representatives $A$, $c(A)$ is the union of all cluster curves represented by a representative in $A$.
We call a subtrajectory starting and ending at vertices a \emph{restricted} subtrajectory. A subtrajectory that is not necessarily restricted is an \emph{arbitrary} subtrajectory.

We propose the following general definition for a group diagram.
\begin{definition}\label{def:gd}
 A group diagram (GD) is a geometric graph with vertices augmented by a temporal component, that represents all input trajectories $\mathcal{T}$.
 We say the graph represents a trajectory $T \in \mathcal{T}$ if there is a similar path $P$ in the graph,
 that is $T$ and (the geometric representation of) $P$ are similar under a given similarity measure.
 We say a GD is minimal if it is minimal in size, either with respect to its number
 of edges or the total length of edges.
\end{definition}

The definition allows to make several choices:
how to represent vertices and edges of the GD, the similarity measure, and whether to minimize number or length of edges.
%
We consider GD that are built from the input trajectories, i.e., edges of the GD are represented by subtrajectories of the input and two edges share a vertex if the endpoints of the corresponding subtrajectories are within distance $d$ from each other. Endpoints of edges with no $d$-distance neighbor have degree one.
Vertices in the graph are thus embedded as the set of endpoints of incident edges.
We will use such graphs in the following.
Note that we could transform these into planar embedded graphs, for instance by connecting all points of a point set $S$ of a vertex to the midpoint of $S$.
% or its convex hull.
Note also that we require representatives to consist of parts of the input as these give realistic representatives. % (not requiring this would allow representatives to go through impassable regions).

As similarity measure, we consider three popular measures on trajectories: the \fr, equal-, and similar-time distance.
%
Figure~\ref{fig:groupDiagram} illustrates several trajectories, where the subtrajectories forming a minimal GD for the given trajectories are highlighted in red.
The generality of definition allows to apply it in different settings, e.g., to entities moving at the same or at different times.

Minimizing the number of edges or their total length seems intuitively reasonable.
However both can lead to strange effects and we need to make further requirements to prevent these.
The effects are the following. For an illustration, see Figure~\ref{fig:strangeEffects}:
\begin{itemize}
\label{list:additional_conditions}
  \item When minimizing number of edges, if two trajectories are close to each other only partially, a smallest possible group diagram consist of the two trajectories rather than representing their similar parts with a joint edge.
  \item When minimizing length, even for a single trajectory the shortest representative would be a series of points at distance $2d$.
\end{itemize}

%Figure \ref{fig:strangeEffects} illustrates these effects. A minimal group diagram for the given trajectories is highlighted in red.

%\begin{figure}
%\includegraphics[scale=0.6]{Figures/strange_effects}
%\centering
%	\caption{Unintuitive group diagrams (highlighted in red) fulfilling the criteria of minimal edge length (left) and minimal number of edges (right).}
%	\label{fig:strangeEffects}
%\end{figure}

\begin{figure}
\begin{minipage}[b]{0.33\textwidth}
\includegraphics[width=\textwidth]{Figures/groupDiagram}
	\caption{Illustration of GD.}
	\label{fig:groupDiagram}
\end{minipage}
\hfill
\begin{minipage}[b]{0.6\textwidth}
\includegraphics[width=\textwidth]{Figures/strange_effects}
	\caption{unintuitive GD with minimal edge length (left) and minimal number of edges (right).}
	\label{fig:strangeEffects}
\end{minipage}
\end{figure}

To prevent these effects, we introduce the following conditions for the two different minimization goals:
\begin{itemize}
  \item For minimizing number of edges we require that the minimality criterion is fulfilled also for every local part of the input: \\
Given a GD $\mathcal{G}$, an arbitrary subtrajectory $\tau$ of one of the representatives in $\mathcal{G}$ and the corresponding cluster $c(\tau)$. We use $G_\tau^* := c(c(\tau))$ to denote the union of all clusters represented by a curve in $c(\tau)$.
We intersect $G_\tau^*$ with the GD $\mathcal{G}$ to obtain all representatives $A_\tau := \mathcal{G} \cap G_\tau^*$.
The \emph{local minimality criterion} can now be formalized by demanding that for each $\tau$ as described above the resulting set $A_\tau$ is a minimal representation for $c(A_\tau)$.

\item For minimizing length we require that no clusters are artificially split up to reduce the length.
   Formally, we require that no subgraph of the GD can be contracted, i.e., substituted by a subgraph of smaller size (but possibly larger length).
\end{itemize}

In the following, we always consider the problem with the corresponding requirement depending on whether we measure size by number or length of edges.


\subsection{Related work}\label{sec:rw}
Two related notions to the GD are the grouping structure and flow diagrams.
The grouping structure is the unique graph representing all density-connected groups traveling at equal-time~\cite{grouping}.
It can be seen as a specialization of the GD, which uses the equal-time distance, and density connectedness as inner group distance.
As such the grouping structure is well suited for grouping entities moving together, but not for entities travelling, e.g. commuting or migrating, at different times. Also, in some settings, such as commuting, pairwise distance is more suitable than density connectedness.
Buchin et al. gave efficient algorithms for computing the grouping structure, %analyzed its complexity 
and demonstrated its usefulness on a real-world data set (of deer, elk, and cattle).
The grouping structure was later generalized by Kostitsyna et al. for the geodesic distance~\cite{grouping2}
and by von Goethem et al. for varying parameters~\cite{grouping3}.

A flow diagram is a minimal (in the number of vertices) diagram representing segmentations of all input trajectories. In a flow diagram nodes represent criteria and edges transitions between criteria~\cite{flow}.
The flow diagram can be seen as generalization of the GD (after switching between vertices and edges) where criteria are more general than small distance of the trajectories.
Buchin et al. showed that deciding if a flow diagram of a certain size exists is NP-hard (even W[1]-hard in the number of trajectories). Hence they give efficient heuristic for computing flow diagrams and evaluate these on a real-world data set (of football players).

Two further related approaches are presented in~\cite{herds} and~\cite{olap}. Huang et. al.~\cite{herds} propose the concept of a herd, which allows splitting and merging, and in contrast to our approach are based on density-connected groups. Baltzer et. al.~\cite{olap} present OLAP for trajectories, in particular for detecting groups based on geographic overlap and intersection.

Computing a GD using the \fr\ is also highly related to map construction algorithms, where the goal is to determine the underlying network of a set of trajectories~\cite{map-construction}, as we discuss in Section~\ref{sec:fr}.
%
Similar modeling choices (i.e. choice of what constitutes edges, which similarity measure, and which minimality condition) occur in the problem of finding a representative (e.g. median, middle, ...) trajectory of a set of similar trajectories.

\subsection{Computational Complexity}\label{sec:complexity} %

By a reduction from the known NP-complete \textsc{Dominating-Set} problem for a grid graph \cite{unit-disk} we can show that the decision problem for GD is NP-complete for all variants we consider.
Given a grid graph $G$ we can easily build an instance of our problem for the special case of parallel trajectories with complexity one, all of equal length, and show that a selection of trajectories in this setting forms a GD of size $l$ if and only if the vertices in $G$ corresponding to these trajectories form a dominating set (of size $l$). See Section \ref{subsec:ca} in the appendix for more details.

\begin{theorem}\label{theorem:NP_fr}
Given an integer $l$, deciding whether there exists a GD of size $l$ is NP-complete for both $l$ denoting the edge length and $l$ denoting the edge number, and for both \fr\ and equal-time distance as similarity criteria.
\end{theorem}

%Overview:
In the following sections we give approximation algorithms and their experimental evaluation on a real data set. 