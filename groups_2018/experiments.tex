\section{Experiments}\label{sec:ex}
%AK: rewritten data section, no need to mention MPIO if I am coauthor:
In order to investigate the usability of our definition of a group diagram and the described algorithms for real world data we performed experiments on tracking data of groups of migrating birds. We used the Python programming language and its library \textit{gmplot} to directly draw the edges of the group diagram on \textit{google maps}. The \textsc{Set-Cover} instances were solved by an implementation of a greedy algorithm.

We present our results using spring migration GPS tracks of one family of greater white-fronted geese (Anser a. albifrons) with parents and two juveniles. The tracks were collected between March and June 2017 on their route from North Western Europe to the Russian Arctic, and positions were collected in half-hourly bursts of 20 GPS positions in 1 Hz resolution. Each position provides a horizontal error value, so for our investigations we could exclude points with high error, i.e. > 15 m.
%AK: I have detailed here as family with 2 young, because you have left out one. Moved details up here
Because the goose family split at the second part of its travel, we here investigated the tracks between Denmark and Lithuania, which contain around 2000 GPS positions for each animal. %AK: please count

The distance between two entities is computed based on their positions on the earth's surface, i.e. the great circle distance. %AK: is "great circle distance" correct? Please change else.
A group diagram shows when a subgroup (or one single entity) separates from the rest of the group (or from a subgroup) and when a subgroup joins another subgroup. In the data of the migrating goose family these split and merge events only happened on a relatively small scale compared to the total geographic extent of the data. If a family member completely splits from the rest of the family on a larger scale it is unlikely that a reunion of this member with the rest of the family or parts of the family occurs.  However, detecting and visualizing split and merge events on a smaller scale is an interesting application of the group diagram. When and where was the family flying close together, so that it can be represented by only one member and when do we need more representatives? For which distance did the family stay ``together'' the whole time or a given percentages of the whole observation period?
% AK: whenever you talk about the goose family, try to use past tense, because this has happened already. If you use present tense it implies that you know that the phenomenon is generalisable, which we dont know

\subparagraph{Bounded Equal-time and Similar-time Distance}
We computed group diagrams based on bounded equal-time and $\alpha$-similar-time distance as similarity measure. For the goose family tracks we determined the group diagram for distances $d = 3, 5, 10,20,40,80,160,320,640, 1280$ meters, and, for each distance, we set the allowed time shift to $\alpha = 0,10$ seconds.

\begin{wrapfigure}{L}{0.66\textwidth}
\includegraphics[width=0.66\textwidth]{Figures/Experiments/WFamily/plot_distance_number_representatives_time_together_equal_similar}
\caption{Representability of the family for different distance thresholds and equal and similar-time distance. The average number of representatives needed to represent the whole family (left) and the percentage of time stamps when one representative was enough to represent the whole family (right) is plotted against the distance thresholds.}
\label{fig:distance_vs_number_representatives}
\end{wrapfigure}

% AK: I dont understand to which figures those descriptions (following 3 paragraphs) belong! please check.
In Figure \ref{fig:distance_vs_number_representatives} we compare the decrease of the average number of representatives needed for growing distance thresholds and the increase of time when one representative is enough to represent the whole family for equal and similar-time distance.

We observe that for small distances the impact of allowing a time shift of 10 seconds was greater than the impact of doubling the distance. As an example we look at the distance threshold values of 10 and 20 meters. For equal-time distance the percentage of time the whole family is represented by one representative increases from 38 \% to 44 \% when doubling the distance threshold from 10 to 20 meters. On the other hand when allowing a time shift of 10 seconds for a distance threshold of 10 meters already for 51 \% of the time stamps one representative is sufficient. Analogue observation count for the average number of representatives and for all other distance thresholds below 40 meters.

Figure \ref{fig:10m_no_timeshift_vs_timeshift} gives an example where allowing a time shift while maintaining the distance threshold lead to a smaller diagram in terms of number of representatives needed. In this part of group diagram only two representatives are enough when a time shift is allowed whereas for bounded equal-time distance four representatives are needed.

As distance is growing it becomes the dominating parameter for the size of the group diagram and the impact of the time shift decreases what can also be seen in the Figure as the difference of the values of the red and blue curves decrease for a growing distance threshold.

The reason for these observations most likely is the formation of the flock while flying. If the entities of the flock are flying in a V-formation or a line, two entities are represented by only one representative even if their distance is greater than the given threshold when allowing a small time shift. The impact of a time shift would be less if the birds were flying next to each other rather than behind each other like in a line or a V-formation.

Another indication that the birds are following each other rather than flying parallel can be derived from Figure \ref{fig:growth}. For equal-time distance the highest relative growth is reached for five meters whereas for 10-seconds- similar-time distance we have the highest relative growth for a distance threshold of three meters. Due to the wing-spread of the geese it is not surprising that for a distance of three meters the family was almost never flying together as it is barley possible that all members are within distance of three meters to one representative at a given time stamp. On the other hand for similar-time distance already for ten percent of the time stamps one representative is enough to represent the family.
%
For greater distance thresholds the impact of a time shift is less significant because for such values the formation of the flock has a much smaller impact to the GD as the actual split and merge events.

\begin{figure}
  \begin{minipage}[t]{0.41\textwidth}
\includegraphics[width=\textwidth]{Figures/Experiments/WFamily/distance10_20_difference_timeshift}
\caption{No time shift allowed (left) and a time shift of 10s (right) for a distance threshold $d=10m$.}
\label{fig:10m_no_timeshift_vs_timeshift}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.56\textwidth}
  \includegraphics[width=\textwidth]{Figures/Experiments/WFamily/plot_distance_growth_time_together}
\caption{Relative growth of the percentage of time when all family members can be represented by one representative. The relative growth is the  absolute value of the increase of the percentage between two considered distance thresholds $d_1$ and $d_2$ divided by the difference of $d_2$ and $d_1$.}
\label{fig:growth}
  \end{minipage}

\end{figure}

\subparagraph{Migration over Water and Over Solid Ground}
During the migration one can observe that when the family was flying over surfaces of water they tended to separate more from each other than while flying over solid ground. One example of this phenomenon is shown in Figure~\ref{fig:split_merge_over_water} for a bounded equal-time distance of 160 meters.
%
\begin{figure}
  \begin{minipage}[t]{0.41\textwidth}
    \includegraphics[width=\textwidth]{Figures/Experiments/WFamily/160_0_over_lake}
\caption{Family members split when flying over a lake and merge after passing the lake.}
\label{fig:split_merge_over_water}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.56\textwidth}
    \includegraphics[width=\textwidth]{Figures/Experiments/WFamily/water_land}
\caption{Difference of representability of the family while
flying over land and over water.}
\label{fig:water_land}
  \end{minipage}
\end{figure}
%
Figure \ref{fig:water_land} shows the difference in the number of representatives needed and in the percentage of time the family is traveling together for flying over solid ground and flying over water. One interesting observation is that the values differ most between 10 and 100 m.

%\begin{wrapfigure}{L}{0.66\textwidth}
%%\captionsetup{justification=centering}
%\includegraphics[width=0.66\textwidth]{Figures/Experiments/WFamily/computation_time}
%\caption{measured run time for computing a GD.}
%\vspace{-10pt}
%\label{fig:runtime}
%\end{wrapfigure}

\subparagraph{Computation Time and Scalability}
The equal-time algorithm  consists of three parts. The computation of the event times and the vertex insertion, the construction of the \textsc{Set-Cover} instance and the process of solving the instance. The runtime of all three parts highly depend on the number of split- and merging events in the given data. 
To evaluate the scalability of our algorithms in terms of a growing group size we generate artificial trajectories based on the given trajectories of the family members. We use two different approaches here. Firstly, we generate two random numbers $r_1$ and $r_2$ for each trajectory $\tau$ and add $r_1$ and $r_2$ to the coordinates of each vertex. Secondly we generate two random numbers at each vertex separately and add the numbers to the corresponding coordinates. Note, that using the first approach the whole trajectory is shifted parallel to the original whereas the second approach produces zig-zag trajecories around the original one which leads to a higher number of events in comparison with the first approach. Using these two ways of generating data we want to emphasize that the scalability of the algorithm in terms of group size highly depends on the given data. To take randomness of the generated data into account we run each computation twice and take the average value of the measured runtime and the number of events, noting that these values barely differ between two runs of the algorithm for the same input values. 
The correlation of the type of data generation, the number of events and the runtime of the algorithms is illustrated in Figure \ref{fig:computation_time_different_data_generation} and \ref{fig:number_events_generated_data}.

\begin{figure}
\includegraphics[width=1\textwidth]{Figures/Experiments/WFamily/computation_time_different_data_generation}
\caption{In the left figure the runtime is plotted for the first data generation type and in the right figure for the second type.}
\label{fig:computation_time_different_data_generation}
\end{figure}

\begin{figure}
\includegraphics[width=1\textwidth]{Figures/Experiments/WFamily/number_events}
\caption{In the left figure the number of events is plotted for the first data generation type and in the right figure for the second type.}
\label{fig:number_events_generated_data}
\end{figure}

For a comparison of the runtime for computing the events and for the construction and solution of the \textsc{Set-Cover} instances see Figure \ref{fig:computation_split}. Note, that the construction of the \textsc{Set-Cover} instances dominates the runtime in the lower figure as for the construction we need to compare all pairs of entities to obtain a relatively small instance with only $k$ subsets. The greedy algorithm therefore finds an approximation in a very short time. For an quantitative evaluation, see Figure \ref{fig:construction_solution_time}


\begin{figure}
\includegraphics[width=1\textwidth]{Figures/Experiments/WFamily/time_events_set_cover_compare}
\caption{Runtime for computing the events (including vertex propagation) and for constructing and solving the \textsc{Set-Cover} instances.}
\label{fig:computation_split}
\end{figure}

\begin{figure}
\includegraphics[width=1\textwidth]{Figures/Experiments/WFamily/construction_solution_time}
\caption{Comparison of the runtime for constructing the \textsc{Set-Cover} instances and for solving these for a group size of eight entities.}
\label{fig:construction_solution_time}
\end{figure}


Another parameter affecting the runtime is the allowed time shift when using similar time as similarity measure. Clearly, the computation time increases for a greater time shift as the algorithm has to explore a longer subtrajectory at each comparison to examine whether two entities are within similar time distance at most $d$. See Figure \ref{fig:time_shift} for an illustration. 

\begin{figure}
%\captionsetup{justification=centering}
\includegraphics[width=1\textwidth]{Figures/Experiments/WFamily/time_compare_similar_time}
\caption{Measured run time for computing a GD for different distance thresholds and different values of allowed time shift.}
\label{fig:time_shift}
\end{figure}

\subparagraph{Conclusion and Remarks} %of caution
We find that our results of computing the group diagram give an accurate picture of the movement of family groups in terms of splitting and merging for different distance thresholds. The example of different group diagram patterns between bursts of flight over land vs. water reveals the usefulness of the group diagram method and that it can even contribute to form novel hypotheses for ecological research.

Although relative GPS accuracy is crucial in this context, we use the given absolute accuracy as a criterion for selecting the locations we use for computing a GD. However, it has often been stated that relative accuracy is much better than absolute accuracy, so we assume our data are sufficiently accurate. Another possible source of error that more likely affects our results is the necessary interpolation between two given timestamps. If the distance between two timestamps is big the applied linear interpolation is only a rough estimation of the position of the entity between the timestamps. The provided data contains relatively large gaps between the burst. For the purpose of computing a GD a more uniform sampling would lead to better results.

Developing the general framework of group diagrams has been motivated by the data described and evaluated here for which we wanted to compute a compact representation. Data of groups moving together, where the tracks of all individuals is given is rarely available, yet. But as the results for the specific problem with the geese data are promising, we expect that the algorithms can be applied to any similar data sets, which will be available to a greater extent in future. 