\section{Introduction}\label{sec:intro}

%\todo{add more motivation to the problem}
The amount of available movement data such as GPS data collected by mobile devices has increased massively over the last years. Movement ecology, for instance, is a growing field at the edge of biology and GIScience which benefits from new possibilities of tagging animals with smart devices collecting location data and several other parameters. Analysing this data poses new algorithmic challenges~\citep{cma}. One of the challenges is to (visually) represent the large amount of data, such that the representation is as concise as possible, but still preserves the information needed for further analysis and processing. In particular, this challenge arises for movement trajectories of several interacting entities.

In this paper, we take on this challenge and present a concise representation for data of groups of moving objects.
Our approach is the following: We assume we are given the trajectories of several moving objects, called \emph{entities}, each of which consists of a series of sampled locations. By linearly interpolating in between each two consecutive timestamps, the corresponding trajectory becomes a polygonal line.
We assume we are given $k$ trajectories, each of complexity $n$ (number of vertices) forming one or several (overlapping, i.e., splitting and merging) groups. We introduce a new framework named the \emph{group diagram} (GD) as a means of a concise representation of these groups. In particular, the GD allows for a concise visual representation. 

The group diagram identifies groups moving together that can be represented by one of the individuals. Its definition is quite general and in particular allows to consider different ways of treating time. 
More specifically, we define the GD as a geometric graph (see the next section for precise definitions) that represents all of the input trajectories in the sense that for each input trajectory there is a similar path in the GD. Furthermore, the GD should have minimal size with respect to the number/length of the edges in the geometric graph.


%\begin{figure}
%\includegraphics[scale = 1]{Figures/groupDiagram}
%	\centering
%	\caption{Illustration of merging and splitting groups and their representation as GD.}
%	\label{fig:illustration}
%\end{figure}



\subsection{Related work}\label{sec:rw}
Two related notions to the GD are the grouping structure and flow diagrams.
The grouping structure is defined as the unique graph which represents all density-con\-nected groups traveling at equal-time~\citep{grouping}.
It can be seen as a specialization of the GD that uses density connectedness at equal timestamps as inner group distance.
As such the grouping structure is well suited for grouping entities moving together, but not for entities travelling, e.g. commuting or migrating, at different times. Also, in some settings, such as commuting, pairwise distance is more suitable than density connectedness. For instance, commuters moving in slow traffic along a major road would form a group in the grouping structure, even though individual commuters in this group may be far away from each other. Also note that density connectedness and equal-time alignment of the time-stamped locations are inherent to the grouping structure (which is based on the Reeb graph). Hence, it would not generalize to other distance measures, as the GD does. Moreover, the Reeb graph that forms the grouping structure does not consist of representatives that capture the movement of each entity because density connectedness does not allow to represent a group by one individual that is close to all in the group. In contrast, for each entity, the group diagram contains a path which is similar to the corresponding trajectory of the entity. 
 
\cite{grouping} gave efficient algorithms for computing the grouping structure, analyzed its complexity,
and demonstrated its usefulness on data sets of deer, elk, and cattle.
The grouping structure was later generalized by \cite{grouping2} for the geodesic distance
and by \cite{grouping3} for varying parameters. Furthermore, \cite{refined_grouping2016} refined the grouping structure by restricting the density connectedness to within a group. They claim that this is a more intuitive definition since \cite{grouping} allow two entities of one group to be density connected via other entities not in the group. However, a grouping structure based on the refined definition is less efficient to compute. \cite{grouping_exp2018} experimentally compared the original and the refined version and showed that the magnitude of change between the original and refined grouping structure increases if the density in the crowd increases.
\cite{multi_granular_trend_detection} built on the work of \cite{grouping} and \cite{refined_grouping2016} in order to detect trends in one-dimensional time-varying data. 

A flow diagram is a minimal diagram (in the number of vertices) representing segmentations of all input trajectories. In a flow diagram, vertices represent segmentation criteria and edges transitions between criteria~\citep{flow}. As a simple example, a trajectory of a person in one day could be roughly segmented to 
\emph{home - work - home}. 
The flow diagram can be seen as a generalization of the GD (after switching between vertices and edges) where criteria are more general than the distance between trajectories. However for this, one would need to consider an infinite number of criteria of the form ``travelled this route at this time'', which is not computationally feasible. 
\cite{flow} showed that deciding if a flow diagram of a certain size exists is NP-hard (even W[1]-hard in the number of trajectories). Hence, they give efficient heuristics for computing flow diagrams and evaluate these on a data set of football players.

Further related approaches for detecting and representing groups of moving objects are flocks \citep{reporting_flocks, longest_flocks, flocks_detecting_patterns, flocks_online_discovering}, convoys \citep{convoys}, swarms \citep{swarms} and herds \citep{herds, olap}. They differently incorporate time and space in their definition of a group. For instance, some concepts require that the entities stay in a disc whereas others use density connectedness \citep{density-based} as grouping criterion.

The only concept that captures splitting and merging of (sub)groups is the herd concept~\citep{herds, olap}. In contrast to our approach, the authors use density-connected groups and define splitting and merging of groups based on a definition of recall and precision measure of a herd. Furthermore, this concept only exhibits the evolution of a group for discrete timestamps whereas the GD graphs show the evolution of one or several groups continuoulsy from the beginning to the end of the movement of each entity. Moreover, in contrast to the GD, no representatives which express the movement of each entity are computed. \cite{olap} present OLAP (online analytical processing) for trajectories to detect groups based on geographic overlap and intersection. Their approach is based on mapping the trajectories on a two-dimensional spatial grid. In contrast, our approach works with precise geometric distance and allows for different ways of handling the temporal component. 

Computing a GD using the \fr\ is also related to map construction algorithms wherein the goal is to determine the underlying network of a set of trajectories~\citep{map-construction}.
%
Similar modeling choices, i.e., the choice of the similarity measure, the minimality condition, and what constitutes edges also occur in the problem of finding a representative of a set of similar trajectories. Approaches to this problem are, for instance, the median trajectory \citep{median_trajectories}, the middle \citep{middle_trajectories} trajectory, or the central \citep{central_trajectories} trajectory.

\subsection{Contribution}\label{sec:contribution}
We present a new framework called group diagram (GD) to concisely represent the movement of one or several moving groups. As outlined above, the framework inherently differs from all previous methods and approaches of representing the movement of groups. In contrast to the grouping structure, it can be applied on data of entities moving at different times and it captures the pairwise distance between the entities, whereas the grouping structure is based on density connectedness. Formally, the flow diagram allows for similar computations, but it cannot be applied in practice to compute all occurrences of splits and merges within a group as one needs to consider an infinite number of criteria. Furthermore, in contrast to all approaches discussed above, our framework computes geometric representatives such that each individual movement is expressed by at least one of these representatives. 

Our work is motivated by the problem of finding such a representation for the movement of groups of migrating greater white-fronted geese \emph{(anser a. albifrons)}. For this specific research question, we present experimental results for one goose family and for groups of independent individual geese. The input data consists of one trajectory for each goose, given by a number of time-stamped GPS-locations. In order to obtain a concise representation, we compute a minimal GD for the input trajectories and for a visual representation, we  plot the representatives of the GD on a map. This representation, for instance, shows typical migration routes for this species of geese or highlights where the goose family was flying closely together and where they were moving with larger pairwise distance, i.e. more separated. We vary the distance threshold of the GD to investigate for which distance the family stayed ``together'' during the whole migration route or for a given percentage of the whole observation period. Furthermore, we generated synthetic data similar to the input to examine the scalability of the methods and their implementation, as real data sets with more entities are not available.  

Our framework allows for several problem sensitive specifications such as the criterion specifying the conciseness of the group diagram and the similarity measure between the moving objects. Therefore, we believe the framework can be applied to a wide range of research questions in the field of movement analysis and beyond.

In this paper, we present the general framework and algorithmic idea to compute the GD and a comprehensive theoretical elaboration which is a presentation of the computational complexity and of specific algorithms for three similarity measures. These measures are the equal-time distance, the similar-time distance and the \fr. The motivation behind focusing on these measures is that they allow to compute a GD for different type of groups. These types are groups where all objects move simultaneously (equal-time distance), within a given time-shift (similar-time distance) or time-independent, that is at different periods of time (\fr). As minimality criteria, we consider the minimal total edge length of the GD and the minimal number of edges.

The definition of the framework and the general algorithmic approach is described in Section~\ref{sec:prelim}. In Section~\ref{sec:complexity}, we show that computing a minimal GD is NP-hard for all the variants considered in detail. Hence, we develop approximation algorithms in Section~\ref{sec:fr} and Section~\ref{sec:et}. The results of applying the GD on data of migrating geese are presented in Section~\ref{sec:ex}.

This paper extends the results of \citep{DBLP:conf/gis/BuchinKK18} presented at the International Workshop on Computational Transportation Science in 2018. Here, we give a more detailed presentation of the general algorithmic approach and a comprehensive analysis of the computational complexity of all variants of the GD we consider.
% experimental evaluations with one similarity measure, namely the equal-time similarity - a measure which is, for instance, suitable to compute a GD of a goose-family migrating within the same flock. In this paper, we claim and proof that all variants of the GD we consider are NP-hard and
Furthermore, we explore the use of the \fr\ as a similarity measure for GD. This measure allows to compute GD for trajectories recorded within different periods of time as this distance only considers the ordering of the timestamps (vertices) along the trajectory and not the specific time at each location. For instance, this is the case if the input consists of migration routes of one individual recorded during several different migration periods. Considering one migration period, we can summarize the migration routes of several independent individuals which do not necessarily migrate simultaneously.


%Figure~\ref{fig:groupDiagram} gives an example, where the subtrajectories forming a minimal GD % for the given trajectories
%are marked in red.
%
%\begin{figure}
%\centering
%\includegraphics[width=0.55\columnwidth]{groupDiagram}
%	\caption{Example of a group diagram.}
%	\label{fig:groupDiagram}
%\end{figure}

% Overview
%In the next section we precisely define and study the computational complexity of this problem.
%As it turns out, the problem of computing a minimal group diagram is NP-hard for both the equal-time and the \fr.
%Hence in the following sections, we develop approximation algorithms, which we then experimentally evaluate on real and synthetic data sets.
