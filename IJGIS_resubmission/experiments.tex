\section{Experiments}\label{sec:ex}
%AK: rewritten data section, no need to mention MPIO if I am coauthor:
In this section we return to the motivating problem of finding a concise representation for one or more groups of migrating geese and present experimental results applying the algorithms on GPS tracking data of a family and independent individuals of greater white-fronted geese (Anser a. albifrons). For the migration of goose families, we use equal- and similar-time distance as similarity criteria and for the migration of independent individuals, we use the \fr\ as similarity criterion. 
We are interested in a concise (visual) representation and obtain such a representation by computing minimal GD for the input trajectories and by plotting the edges of the GD on a map. This representation highlights typical migration routes. Furthermore, we use the solution of the constructed \textsc{Set-Cover} instance to summarize the migration of a goose family answering questions like how much time of the whole migration period was the family flying close together, with respect to a varying distance threshold. Beyond representation, the obtained migration characterizations can help to form novel research hypothesis. Additionally, we investigate the runtime and scalability by introducing additional artificial group members.

We used the Python programming language and the library \textit{simplekml} to create group diagrams which can be displayed on \textit{google maps} or \textit{OpenStreetMaps}. To solve the (weighted) \textsc{Set-Cover} instance we used the library \textit{SetCoverPy}. All computations were performed on an Intel Pentium i5 2.4 GHz processor and 12 GB RAM.

\subsection{Equal-Time and Similar-Time Distance}
In this section, we consider one tracked goose family, which consists of both parents and two juveniles. The tracks were collected between March and June 2017 on their spring migration route from North Western Europe to the Russian Arctic. Positions were collected in half-hourly bursts of 10 GPS positions (one position every two seconds). Each position provides a horizontal error value, so we preprocessed the data by deleting all points with high error, i.e. $>$ 15 m.
%AK: I have detailed here as family with 2 young, because you have left out one. Moved details up here
Because the goose family splits at the second part of its travel, we investigated the tracks between Denmark and Lithuania here, which contain around 2000 GPS positions for each animal. %AK: please count

\begin{figure}
\includegraphics[width=1\textwidth]{wolde_track}
\caption{The trajectory of one goose family member between Denmark and Lithuania. The lower picture shows one burst of sampling.}
\label{fig:track}
\end{figure}

The distance between two entities is computed based on their positions on the earth's surface, i.e. the great circle distance. %AK: is "great circle distance" correct? Please change else.
A group diagram shows when a subgroup (or one single entity) separates from the rest of the group (or from a subgroup) and when a subgroup joins another subgroup. In the data of the migrating goose family, these split and merge events only happened on a relatively small scale compared to the total geographic extent of the data. Generally, if a family member completely splits from the rest of the family on a larger scale, it is unlikely that a reunion of this member with the rest of the family or parts of the family occurs.  However, detecting and visualizing split and merge events on a small scale is an interesting application of the group diagram. We use the GD to investigate when and where the family was flying close together, so that it can be represented by only one member and when more than one representative is needed. Varying the distance threshold, we examine for which distance the family stayed ``together'' the whole time or a given percentage of the whole observation period.

% AK: whenever you talk about the goose family, try to use past tense, because this has happened already. If you use present tense

We computed group diagrams based on equal-time and $\alpha$-similar-time distance as similarity measure. For the goose family tracks, we determined the group diagram for distances $d = 3, 5, 10, 20, 40, 80, 160, 320, 640, 1280$ meters. Due to the wingspread of the geese, a distance lower than 3 m is unlikely to observe. Doubling the distance in each step is motivated by the observation that the GD changes the most for small distances. For each distance, we set the allowed time shift to $\alpha = 10$ seconds, when computing the GD with $\alpha$-similar-time distance as similarity measure. This parameter was experimentally chosen. For larger time shifts only the computation time increases without a significant difference of the GD compared with the 10-second time shift.  

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{plt_dist_number_representatives_time_together_equal_similar}
\caption{Representativity of the goose family for different distance thresholds and equal- and similar-time distance. The average number of representative needed is the average size of the \textsc{Set-Cover} solution between two timestamps over the whole observation period. A value of four indicates that during the whole migration the pairwise distance between the family members exceeds the distance threshold, whereas a value of one implies that in between each two consecutive timestamps there exists one entity $e$, such that the distance to $e$ is lower than the given threshold for each family member.}
\label{fig:distance_vs_number_representatives}
\end{figure}

% AK: I dont understand to which figures those descriptions (following 3 paragraphs) belong! please check.
In Figure \ref{fig:distance_vs_number_representatives}, we compare the decrease of the average number of representatives needed for increasing distance thresholds and the increase of time when one representative is sufficient to represent the whole family for equal ($\alpha = 0s$) and similar-time ($\alpha = 10s$) distance.
We observe that for small distances the impact on the average number of representatives needed of allowing a time shift of 10 seconds is greater than the impact of doubling the distance. For instance, we look at the distance threshold values of 10 and 20 meters. For equal-time distance the percentage of time the whole family is represented by one representative increases from 38 \% to 44 \% when doubling the distance threshold from 10 to 20 meters. When allowing a time shift of 10 seconds for a distance threshold of 10 meters already for 51 \% of the timestamps one representative is sufficient. Analogue observations can be made for the average number of representatives and for all other distance thresholds below 40 meters.

Figure \ref{fig:10m_no_timeshift_vs_timeshift} gives an example where allowing a time shift while maintaining the distance threshold leads to a smaller diagram in terms of the number of representatives needed. In this part of the group diagram, only two representatives are enough when a time shift is allowed whereas for equal-time distance four representatives are needed.
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{distance10_20_difference_timeshift}
\caption{No time shift allowed (left) and a time shift of 10s (right) for a distance threshold $d$=10m. The trajectory of each family member is shown in a different color. That is in the left Figure each entity needs its own trajectory as a representative whereas, in the right Figure, the blue and the green trajectories represent the movement of all four family members. The thicker the subtrajectory $\tau$ of an entity $e$, the more entities are represented, that is within distance of 10 m to $e$ during the lifespan of $\tau$.}
\label{fig:10m_no_timeshift_vs_timeshift}
\end{figure}
As distance increases, it becomes the dominating parameter for the size of the group diagram, and the impact of the time shift decreases. This can also be seen in Figure \ref{fig:10m_no_timeshift_vs_timeshift} as the difference of the values of the red and blue curves decrease for an increasing distance threshold.

The reason for these observations most likely is the formation of the flock while flying. If the entities of the flock are flying in a V-formation or a single file, two entities are represented by only one representative even if their distance is greater than the given threshold when allowing a small time shift. The impact on the size of the GD of a time shift would be less if the birds were flying next to each other rather than behind each other like in a single file or a V-formation.

%Another indication that the birds are following each other rather than flying parallel can be derived from Figure \ref{fig:growth}. For equal-time distance the highest relative growth is reached for five meters whereas for 10-seconds- similar-time distance we have the highest relative growth for a distance threshold of three meters. Due to the wing-spread of the geese it is not surprising that for a distance of three meters the family was almost never flying together as it is barley possible that all members are within distance of three meters to one representative at a given time stamp. On the other hand for similar-time distance already for ten percent of the time stamps one representative is enough to represent the family.
%%
%For greater distance thresholds the impact of a time shift is less significant because for such values the formation of the flock has a much smaller impact to the GD as the actual split and merge events.

\begin{figure}
\centering
    \includegraphics[width=0.9\textwidth]{160_0_over_lake}
\caption{Family members split when flying over a lake and merge again after passing the lake.}
\label{fig:split_merge_over_water}
\end{figure}


%\begin{figure}
%  \includegraphics[width=0.5\textwidth]{Figures/Experiments/WFamily/plot_distance_growth_time_together}
%\caption{Relative growth of the percentage of time when all family members can be represented by one representative. The relative growth is the  absolute value of the increase of the percentage between two considered distance thresholds $d_1$ and $d_2$ divided by the difference of $d_2$ and $d_1$.}
%\label{fig:growth}
%\end{figure}

\subsubsection{Migration Over Water and Solid Ground}
During the migration, one can observe that when the family was flying over surfaces of water, they tended to separate more from each other than while flying over solid ground. One example of this phenomenon is shown in Figure~\ref{fig:split_merge_over_water} for an equal-time distance of 160 meters.
%
\begin{figure}
\centering
\includegraphics[width=.75\textwidth]{water_land}
\caption{Difference of representativity of the family while
flying over land and over water.}
\label{fig:water_land}
\end{figure}
%
Figure \ref{fig:water_land} shows the difference in the number of representatives needed for flying over solid ground and flying over water. One interesting observation is that the values differ most between 10 and 100 m.

%\begin{wrapfigure}{L}{0.66\textwidth}
%%\captionsetup{justification=centering}
%\includegraphics[width=0.66\textwidth]{Figures/Experiments/WFamily/computation_time}
%\caption{measured run time for computing a GD.}
%\vspace{-10pt}
%\label{fig:runtime}
%\end{wrapfigure}

\subsubsection{Runtime and Scalability}
The equal-time algorithm  consists of three parts. The computation of the event times and the vertex insertion, the construction of the \textsc{Set-Cover} instance, and the process of solving the \textsc{Set-Cover} instance. The runtime of all three parts highly depends on the number of split- and merge events in the given data.

To evaluate the sensitivity of our algorithms for growing group size, we generate artificial trajectories based on the given trajectories of the family members. We use two different approaches here. Firstly, we generate two random numbers $r_1$ and $r_2$ for each trajectory $\tau$ and add $r_1$ and $r_2$ to the coordinates of each vertex. Secondly, we generate two random numbers at each vertex separately and add the numbers to the corresponding coordinates. The range of $r_1$ and $r_2$ is between $-0.0001^{\circ}$ and $+0.0001^{\circ}$ which results in shifting each vertex within a circle of approximately 10 meter radius. Note that using the first approach, the whole trajectory is shifted parallel to the original whereas the second approach produces zig-zag trajectories around the original one which leads to a higher number of events in comparison with the first approach, see Figure~\ref{fig:data_generation} for an example. 
\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{data_generation}
\caption{Generating a new trajectory based on a given trajectory (thick line) by shifting each vertex the same 
amount in north-south and east-west direction (a) or by shifting each vertex independently (b).}
\label{fig:data_generation}
\end{figure}
Using these two ways of generating data, we want to emphasize that the scalability of the algorithm in terms of group size highly depends on the given data. To take randomness of the generated data into account, we run each computation twice and take the average value of the measured runtime and the number of events, noting that these values barely differ between two runs of the algorithm for the same input values.
The correlation of the type of data generation, number of events and runtime of the algorithms is illustrated in Figure \ref{fig:computation_time_different_data_generation} and \ref{fig:number_events_generated_data}.


\begin{figure}
\centering
\includegraphics[width=.9\textwidth]{computation_time_different_data_generation}
\caption{The runtime of the algorithm using the first data generation type (left), that is shifting each vertex independently and the second type (right), that is shifting the whole trajectory randomly in north-south and east-west direction.}
\label{fig:computation_time_different_data_generation}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.9\textwidth]{number_events}
\caption{The number of events using the first data generation type (left) and the second type (right).}
\label{fig:number_events_generated_data}
\end{figure}
For a comparison of the runtime for computing the events and for the construction and solution of the \textsc{Set-Cover} instances, see Figure \ref{fig:computation_split} (first type of data generation was used here). Note that the construction of the \textsc{Set-Cover} instances dominates the runtime in the lower plot as for the construction we need to compare all pairs of entities. However, the \textsc{Set-Cover} instance computed between each two consecutive timestamps is relatively small; it consists of exactly $k$ subsets. Therefore, the runtime of the greedy algorithm which computes an approximate solution of the \textsc{Set-Cover} instance is significantly smaller than the construction time. For a quantitative evaluation, see Table \ref{fig:construction_solution_time}. 
The table shows the runtimes for a dataset of 8 entities obtained by the first type of data generation from the input data. 
%Note, that in this table the construction time includes the time for computing events and for inserting the new vertices. 

\begin{figure}
\centering
\includegraphics[width=.9\textwidth]{time_events_set_cover_compare}
\caption{Runtime for computing the events including vertex propagation (upper plot) and for constructing and solving the \textsc{Set-Cover} instances.}
\label{fig:computation_split}
\end{figure}
\medskip
\begin{table}
\centering
  \begin{tabular}{cccc}
    \toprule
    Distance (m) & Construction Time (s) & Solving Time (s) & Total Runtime (s)\\
    \midrule
    3 & 35.9 & 0.9 & 36.8 \\
    5 & 52.0 & 1.0 & 53.0\\
    40 & 4.9 & 0.1 & 5.0\\
    80 & 3.1 & 0.0 & 3.1\\
    460 & 0.5 & 0.0 & 0.5\\
  \bottomrule
\end{tabular}
\caption{Runtime of the two steps of the algorithm.}
  \label{fig:construction_solution_time}
\end{table}

%\begin{figure}
%\includegraphics[width=0.5\textwidth]{Figures/Experiments/WFamily/construction_solution_time}
%\caption{Comparison of the runtime for constructing the \textsc{Set-Cover} instances and for solving these for a group size of eight entities.}
%\label{fig:construction_solution_time}
%\end{figure}


Another parameter affecting the runtime is the allowed time shift when using similar-time as similarity measure. Clearly, the computation time increases for a greater time shift as the algorithm has to explore a longer subtrajectory at each comparison to examine whether two entities are within similar-time distance at most $d$. See Figure \ref{fig:time_shift} for an illustration.

\begin{figure}
\centering
%\captionsetup{justification=centering}
\includegraphics[width=0.9\textwidth]{time_compare_similar_time}
\caption{Measured run time for computing a GD for different distance thresholds and values of allowed time shift.}
\label{fig:time_shift}
\end{figure}

\subsection{\Fr}

In this section, we show how group diagrams can be used to extract representative, i.e. typical migration routes from a data set of migration trajectories. To this end, we computed GD with the \fr\ as similarity criterion. Recall, that for the \fr\ only the order of the time-stamped locations is taken into account and not the specific timestamp. Therefore, we can compare entities migrating during different periods of time. 
We consider two different settings. Firstly, we compute GD for one individual and its migration trajectories during several migration periods. Secondly, we consider several independent individuals for one specific migration period. 

\subsubsection{Implementation and Data Description}
We use the Python libraries \textit{Shapely} and \textit{LatLon} to compute the segmentation of the input data as described in Section \ref{sec:segmentation}. \textit{Shapely} is used to perform geometric operations such as projections of points onto line segments and the computation of the intersections of polygonal lines. With \textit{LatLon} we are able to perform parallel shifts of a line segment on the earth surface for a given distance $d$. In our implementation of the \fr, we use the Vincenty's formula provided by the library \textit{geopy} for all point to point distance calculations.


We want to compare and to summarize the migration routes on a large scale, that is with a distance threshold between 50 and 300 km. Therefore, we are not interested in the movement during stopovers or during a burst of GPS positions. Moreover, to satisfy the memory constraints of the implementation (see the runtime analysis in Section \ref{sec:group_diagrams_frechet_runtime}) it is necessary to upper bound the resolution of the migration routes, i.e. the complexity of the trajectories. We have manually chosen a minimum distance  of 90 km between two consecutive stored locations. By that, we exclude the stopovers and upper bound the positions for each burst but maintain a sufficiently fine resolution of the migration routes for the distance thresholds we consider in the evaluations below.


Each relevant cluster (subset of the \textsc{Set-Cover} instance) is weighted by the relative length of its representative to the total length of all migration routes of the input and we solve the resulting  \textsc{Weighted-Set-Cover} instance to obtain the GD with minimal total edge length. The length of each representative is approximately computed by projecting the longitudes and latitudes to the Mercator web with the library \textit{PyProj} and by using the built-in length function of the resulting \textit{Shapely-LineString} class instance of the projected subtrajectory.

In the following figures, the GPS-tracks of the geese are plotted in black and the edges of the computed GD are shown in red.

\subsubsection{Group Diagrams}
\label{sec:group_diagrams_frechet}
\paragraph*{One Individual - Multiple Years}
Three GD representing the spring migration between 2014 and 2016 of one individual (id:711) of the study \textit{LifeTrack Geese IG-RAS MPIO ICARUS} (data set maintained by movebank.org) are shown in Figure \ref{fig:one_individual}. The GD provide compact representations of the three migration routes and outline similarities and differences in the migration periods for different distance thresholds. Note, that to the best of our knowledge there are no data sets with an observation period of more than three years for migrating geese available.

In order to make the plots more readable and understandable, we connected the computed edges of the group diagram with additional straight-line, dark red colored edges if the endpoints of two edges are within the given distance threshold.

The distance thresholds have been chosen experimentally such that the three GD significantly differ from each other. For a distance threshold of 50 km, two representatives are needed to capture all data, except for the part in northern Germany, the Finish Bay, and the Barents Sea, where one representative is sufficient to represent the migration routes for all three years. For a distance threshold of 100 km, one representative is sufficient except for the migration over Lithuania and Latvia. Finally, for a distance threshold of 250 km, the group diagram consists of only one representative, that is all the spring migration routes between 2014 and 2016 are inside a 250 km-tube around this representative. This example shows how a concise visual representation depending on the specific demands can be obtained. If one is only interested in extracting (and plotting) representative migration routes which show spatial differences between the year only on a large scale one can choose a large distance threshold. With a decreasing distance threshold, the size of the GD increases, revealing spatial differences between the migration years on a smaller scale.  



\begin{figure}
\centering
\includegraphics[width=1\textwidth]{711_spring_migration}
\caption{GD (red) for spring migration between 2014 and 2016 (black) for a distance threshold of 50 km (upper left), 100 km (upper right) and 250 km (lower Figure).}
\label{fig:one_individual}
\end{figure}

\paragraph*{Multiple Individuals - One Year}
We can also use the group diagram framework to summarize the different migration routes for multiple individuals during one migration period. In Figure \ref{fig:muliple_individuals} we present two GD of three individuals from the study \textit{LifeTrack Geese IG-RAS MPIO ICARUS} (data set maintained by movebank.org) for the observation period from February to June in 2014. Again, the size of the GD and the placements of the edges of the GD reflects the similarity of the migration routes of the individuals for different distance thresholds.
For the lower distance threshold (100 km) the migration routes can be summarized by a first part where all individuals are migrating inside a 100-km-tube of the migration route of one representative individual, by two splits, one in eastern Poland and one in Lithuania and by two merges at the coastline of the White Sea and at the coastline of the Barents Sea. 
A distance threshold of 300 km results in a GD which consists of only two different representative migration routes, one northern route along the coastline of the Baltic Sea and the White Sea and one southern route passing Ukraine and the midland of western Russia. Again, these distance thresholds have been chosen experimentally to show two significantly different group diagrams. 

\begin{figure}
%\captionsetup{justification=centering}
\centering
\includegraphics[width=1\textwidth]{icarus_3}
\caption{GD (red) for spring migration in 2014 of three individuals (black) for a distance threshold of 100 km (left Figure) an 300 km (right Figure).}
\label{fig:muliple_individuals}
\end{figure}

The given examples illustrate the idea of finding a concise visual representation for multiple tracks of moving objects, i.e. to extract typical routes for different distance threshold and to avoid overplotting especially for a small \fr\ between the input trajectories. However, computing such a representation is more interesting for groups of larger size. Figure~\ref{fig:8_entities} shows a group diagram of the spring migration in 2014 for a group which consist of eight individuals from the study \textit{LifeTrack Geese IG-RAS MPIO ICARUS} for a distance threshold of 300 km. This example also shows how the GD can be used to identify outliers, as the representative along the coastline captures the vast majority (seven out of eight) individuals, whereas the representative along the midland of Europe captures the migration of only one individual. Due to memory constraints, we skipped the segmentation steps here and performed the clustering and the computation of the relevant clusters based on the input vertices solely. The possible errors of this approach, which are discussed in Section \ref{sec:segmentation}, do not seem to play an important role here, because the distance threshold is around three times higher than the resolution of the tracks, hence, for the splits and merges on this large scale the input data is already sufficiently finely segmented. 

Note that the algorithm additionally provides us with the \textit{representation strength} of each edge of the GD (see Remarks after Definition \ref{def:gd}) at no extra cost. Thus, when adjusting the thickness of each edge accordingly, the GD reflects that most individuals were migrating along the coastline and only one was following a route more in the midland of Eastern Europe and Russia.

\begin{figure}
%\captionsetup{justification=centering}
\centering
\includegraphics[width=0.55\textwidth]{icarus_8}
\caption{GD (red) for spring migration in 2014 of eight individuals (black) for a distance threshold of 300 km. The extracted typical migration routes for this distance threshold are shown in red. The upper part of the GD along the coastline represents the migration of seven individuals. The lower part represents the migration of one individual.}
\label{fig:8_entities}
\end{figure}

\subsubsection{Runtime}
\label{sec:group_diagrams_frechet_runtime}
We briefly summarize the runtime of the computations in this section. All experiments were performed twice and we show the average runtimes in the tables.

Table~\ref{table:one_ind_multiple_years} shows the number of vertices (Size) after two segmentation steps and the runtime of the different steps of the algorithm for all three considered distance thresholds. The runtime is broken down into the time needed to run the segmentation (Segm.), the time to compute all the data structures needed for the clustering (Setup), the computation of all relevant clusters and cluster representatives which form the \textsc{Set-Cover} instance (Constr.) and the runtime of solving the instance (Solve). The setup consists of the computation of the labeled augmented Free-Space-Diagram. This data structure allows searching for cluster curves with \fr\ at most $d$ to a given representative. For more details, see \citep{clustering}.

\begin{table}
\centering
\begin{tabular}{@{}cc|ccccccc@{}}\toprule
\multicolumn{2}{c|}{Data} & \phantom{ab} & \multicolumn{5}{c}{Comp. Time (s)} &
\phantom{abcde} \\
\cmidrule{1-2} \cmidrule{3-9}
Dist(km) & Size && Segm. & Setup & Constr. & Solving & Total\\ \midrule
50 & 591 && 8.9 & 69.0 & 2.6 & 2.2 & 82.7 \\
100 & 766 && 9.9 & 122 & 5.9 & 1.0& 138.8 \\
250 & 859 && 11.3 & 243.9 & 14.3 & 1.1& 270.6 \\
\bottomrule
\end{tabular}
\caption{Runtime to compute the GD: one individual - multiple years.}
\label{table:one_ind_multiple_years}
\end{table}

Table~\ref{table:one_year_multiple_ind} summarizes the runtimes to compute the GD of three individuals for one migration period. The first two columns show the number of the vertices before (Input) and after the segmentation steps (Augmen.) for three different resolutions (Res.), that is the minimal distance between two input points.

Further experiments showed that the limit of the implementation in terms of the data size is roughly 1200 vertices after the segmentation steps. Therefore, the computation of the GD fails for the distance threshold of 100 km and a resolution of 30 km, see Table~\ref{table:one_year_multiple_ind}. For a data size of 1000 vertices,  we need to store up to 1e6 float number in the augmented Free-Space-Diagram which requires 12 GB storage on our system. In order to perform experiments with a larger input size, the implementation would need to be more memory efficient as described in \citep{clustering}. The runtime is dominated by the computation of the data structures needed for the clustering and the computation of the relevant clusters.

\begin{table}
\centering
\begin{tabular}{@{}ccc|ccccccc@{}}\toprule
\multicolumn{3}{c|}{Data} & \phantom{abc} & \multicolumn{5}{c}{Comp. Time (s)} &
\phantom{abcde} \\
\cmidrule{1-3} \cmidrule{4-10}
Res. (km) & Input & Augmen. && Segm. & Setup & Constr. & Solving & Total\\ \midrule
100 & 115 & 356 && 5.4 & 23.5 & 0.9 & 0.7 & 30.5 \\
60 & 179 & 512 && 10.3 & 51.4 & 1.7 & 1.1 & 64.5\\
40 & 252 & 798 && 20.8 & 139.0 & 4.8 & 1.7 & 166.3 \\
30 & 350 & 1589 && 62.6 & MemErr& - & - & -\\
\bottomrule
\end{tabular}\\
\medskip
\begin{tabular}{@{}ccc|ccccccc@{}}\toprule
\multicolumn{3}{c|}{Data} & \phantom{abc} & \multicolumn{5}{c}{Comp. Time (s)} &
\phantom{abcde} \\
\cmidrule{1-3} \cmidrule{4-10}
Res. (km) & Input & Augmen. && Segm. & Setup & Constr. & Solving & Total\\ \midrule
100 & 115 & 484 && 7.5 & 60.3 & 2.7 & 0.9 & 71.4\\
60 &  179 & 731 && 14.9 & 161.7 & 6.9 & 2.4 & 185.9\\
40 &  252 & 1122 && 32.6 & 466.1 & 15.0 & 5.8 & 519.5 \\
\bottomrule
\end{tabular}
%\caption{Runtime to compute the GD: one year - multiple individual, 300 km distance threshold}
%\label{table:one_year_multiple_ind_300}
\caption{Runtime to compute the GD: one year - multiple individuals for a distance threshold of 100 km (upper Table) and 300 km (lower table).}
\label{table:one_year_multiple_ind}
\end{table}


\subsection{Discussion of Possible Errors} %of caution
GPS accuracy is crucial for the computation of GD, in particular, relative GPS accuracy is crucial for small distance thresholds. 
We use the given absolute accuracy as a criterion for selecting the locations for computing a GD. 
It has often been stated that relative accuracy is lower than absolute accuracy, so we assume that our data is sufficiently accurate.
For the \fr\ experiments GPS accuracy can be neglected for the large distance thresholds we considered. Another possible source of error that more likely affects our results is the necessary interpolation between two given timestamps, which only gives a rough estimation of the position in between two timestamps. For the purpose of computing a GD, a more uniform sampling, rather than dense bursts with large time gaps, would lead to better results.


%Developing the general framework of group diagrams has been motivated by the data described and evaluated here for which we wanted to compute a compact representation. Data of groups moving together, where the tracks of all individuals is given is rarely available, yet. But a
%As the results for the specific problem with the geese data are promising, we expect that the algorithms can be applied to any similar data sets, which will be available to a greater extent in future. 