\section{Group Diagrams Based on the \Fr\ }\label{sec:fr}

Following the idea presented in Section~\ref{sec:algorithmic_appraoch}, we construct and then approximately solve a \textsc{Set-Cover} instance. Therefore, we obtain  approximation algorithms in both cases, minimizing the total size of the GD and minimizing the length. When minimizing length for the \fr, we additionally make a small additive error in the construction for each edge of the GD, see Lemma~\ref{lemma:rel_cluster_enough}.

Recall that the \emph{\fr} between two polygonal curves $T$ and $S$ parameterized over $[0,1]$ is defined as the infimum over all reparameterizations $\alpha$ and $\beta$ of $[0,1]$ of the maximum over all $t \in [0,1]$ of $\|T(\alpha(t)) - T(\beta(t)) \|$ \citep{frechet}.

Typically, the \fr\ is illustrated by a man walking his dog. The \fr\ equals the shortest length of a leash that allows the man and the dog to walk on their curves from beginning to end. Neither the man nor the dog is allowed to walk back along their curves. 
The \fr\ between two polygonal curves of complexity $n$ can be computed in $O(n^2 \log n)$ time~\citep{frechet}.

To compute a minimal GD with \fr\ as similarity criteria, we use a sweep algorithm with moving points $a_T$ and $b_T$ along each trajectory $T$ and report all \emph{relevant} clusters represented by the subtrajectory between the current positions of $a_T$ and $b_T$ as described in~\citep{clustering}.

\subsection{Segmentation}
\label{sec:segmentation}
%Now we consider how to segment the input trajectories and choose relevant representatives.
%A first approach may be to use the segmentation of the trajectories given by the vertices of the input. However, we observe that this segmentation is not fine grained enough to obtain a minimal representation. See Figure~ for an illustrating example.  % of the data.

\begin{figure}
\centering
 \includegraphics[width=0.9\columnwidth]{inputNotEnoughBoth}
 %\hspace{10mm}
 %\includegraphics[scale=0.5]{Figures/inputNotEnoughRepresentative}
\caption{Input vertices are not sufficient for a minimal representation. There does not exist a representative starting and ending at the input vertices representing all three subtrajectories in the middle part as the endpoints of the segments are within distance greater than $d$. }
\label{fig:inputNotEnough}
\end{figure}

As proposed in Section~\ref{sec:algorithmic_appraoch}, we need to insert new vertices along the trajectories to obtain a minimal representation where all representatives start and end at a vertex. See Figure~\ref{fig:inputNotEnough} for an illustrating example of an insufficiently fine grained input.
%Recall, that we denote the obtained data as \emph{augmented input data}.
%\bernhard{introduce the definition of a relevant cluster $\rightarrow$ formulate requirements of the segmentation: for all not-restricted relevant cluster representative there is a restricted cluster representative of similar length representing the same cluster.}

Before we develop a strategy of inserting vertices, we precisely define a relevant cluster representative.
We will show (Lemma~\ref{lemma:rel_cluster_enough}) that it suffices to consider only these representatives.
Recall that a cluster representative is a subtrajectory of the input and cluster curves are the subtrajectories within a fixed distance $d$ to the representative.
We use $d$-tube, $\mathcal{T}_d(\tau)$, to refer to the $d$-neighborhood of a subtrajectory $\tau$, that is the set of all points with distance $\leq d$ to a point on $\tau$. 

\begin{definition}
\label{def:relevantCluster}
A cluster representative $\tau$ which represents the cluster $c(\tau)$ is \emph{irrelevant} if it can be extended to $\tau'$ such that $c(\tau')$ contains only extended curves of $c(\tau)$ and $|c(\tau)|=|c(\tau')|$ and such that no other trajectory $T$ with $T \cup \mathcal{T}_d(\tau) = \emptyset$ contains a subtrajectory inside one of the $d$-tubes around the cluster curves of $c(\tau')$. If a cluster representative cannot be extended in such a way, the representative and the corresponding cluster are \emph{relevant}.
\end{definition}

Figure \ref{fig:relevant_cluster} shows some examples of relevant and irrelevant clusters.
\begin{figure}[b]
\centering
\includegraphics[width=0.9\columnwidth]{relevantCluster}
	\caption{Examples of relevant and irrelevant clusters.}
	\label{fig:relevant_cluster}
\end{figure}

\begin{lemma}
\label{lemma:rel_cluster_enough}
When minimizing size, there always exists a minimal GD solution where edges correspond to relevant cluster representatives. When minimizing length, this solution adds at most an additive error of $2de$, where $d$ is the distance threshold and $e$ is the number of edges of an optimal solution.
\end{lemma}

\begin{proof}
For an irrelevant cluster we can always extend the representative such that the cluster is extended as well and none of the cluster curves is represented by another trajectory not in the cluster in a possibly better way with respect to the total size of the group diagram. In terms of minimal length, it is possible to shrink a relevant cluster representative $\tau$ such that it still represents the same cluster $c(\tau)$, but the amount of shrinking is bounded by $2d$ as otherwise, the distance to the cluster curves is greater than $d$.
\end{proof}

%Note that this additive error is tight: Consider an input setting of trajectories of complexity one and length greater than $2d$, where always two trajectories are congruent and the pairs are within distance greater than $d$. Here, only the whole trajectories are relevant and therefore each representative (whole trajectory) is $2d$ longer than a minimal representative (middle part with distance $d$ to endpoints). When each segment has length greater than $4d$, which is the case in our experiments, we have a multiplicative error of at most 2 when using only relevant representatives.

We now describe how to obtain a sufficiently fine segmentation such that all relevant clusters start and end at vertices.

\paragraph*{Inserting New Vertices}
%\bernhard{A short discussion what intuitively is meant here is needed as one can also argue that the movement represented by the upper segment is different to the one represented by the lower one, so they should not be represented by just one of them.}
We consider two triggers for inserting a new vertex. Firstly, for every vertex $v$ of the input data, we add a vertex to every segment which has distance to $v$ less than or equal to $d$ (see Figure~\ref{fig:insertVertices}$a$) at the point along the segment where the distance to $v$ is minimal (type 1). Secondly, we add a new vertex if the distance between two segments is less than $d$ for the first time and if the distance exceeds $d$ again (type 2) (see Figure~\ref{fig:insertVertices}$b$). Type 1 introduces 0 or 1 new vertex for each vertex-segment pair and type 2 introduces 0, 2 or 4 vertices for each pair of segments.

\begin{figure}
\centering
%\captionsetup{width=1\textwidth}
\includegraphics[width=0.8\columnwidth]{addingVertices}
\centering
\caption{Inserting new vertices. The vertices from the input are shown as disks whereas the newly added ones are marked as squares.}
\label{fig:insertVertices}
\end{figure}

First, we observe that only one step of inserting new vertices does not suffice for finding relevant cluster representatives. A counterexample is shown in Figure \ref{fig:secondRecursion}$b$. 
%where the subtrajectories forming a minimal group diagram are highlighted in blue. Both representatives start or end at a vertex which is not part of the input and which has not been added in one step of vertex insertion. Thus, a second step of inserting new vertices (vertices of the second insertion step are shown as crosses in the Figure) is necessary. Otherwise, when only input vertices and vertices of one insertion step are allowed as endpoints of a representative, the representatives overlap, see Figure \ref{fig:secondRecursion}$a$. Hence, minimality cannot be obtained  for edge length as minimality criterion, but also to obtain a GD with minimal edge number, we need the second insertion step to not violate the local minimality condition.
%

Next, we show that two steps of inserting new vertices are sufficient.
%we are able to find all relevant clusters where the representative and all cluster curves start and end at some vertex.

\begin{figure}
\centering
%\captionsetup{justification=centering}
%\captionsetup{width=0.8\textwidth}
\includegraphics[width=0.8\columnwidth]{secondRecursion}
\caption{The GD consisting of the representatives marked in red does not have minimal edge length or violates the local minimality criterion. This cannot be avoided in this example if only input vertices (dots) and vertices of the first vertex insertion step (squares) are allowed to bound the representatives. To obtain a minimal GD, a second step of vertex insertion is needed (crosses). The GD consisting of representatives marked in blue has minimal length and fulfils the local minimality criterion.}
\label{fig:secondRecursion}
\end{figure}

\begin{lemma}
\label{lemma:two_steps_are_enough}
After two steps of inserting new vertices, all relevant cluster representatives start and end at vertices of the augmented data.
\end{lemma}

\begin{proof}
Consider a relevant cluster representative $\tau$ starting and ending at arbitrary points along a trajectory and consider what makes $\tau$ relevant:
\begin{itemize}
\item \textbf{Case 1:} When extending $\tau$, the distance to one of the cluster curves exceeds $d$ or the distance to another trajectory, not in the cluster yet, falls below $d$.
\item \textbf{Case 2:} When extending $\tau$, the distance of one of the cluster curves $\sigma$ to another trajectory, not in the cluster, falls below $d$.
\end{itemize}
The event described in case 1 implies that a vertex $v$ is inserted to $\tau$ in the first propagation step. The event in case 2 implies the insertion of a vertex $w$ to $\sigma$ and, as the distance of $\sigma$ and $\tau$ is smaller than $d$, a vertex $v$ is inserted to $\tau$ in the second propagation step.
Thus, we can restrict $\tau$ to start at $v$.
\end{proof}
%
Lemmas~\ref{lemma:rel_cluster_enough} and \ref{lemma:two_steps_are_enough} imply:
\begin{theorem}
\label{thm:restrictedclusters}
After two steps of inserting new vertices there always exists a minimal GD solution wherein edges correspond to restricted relevant cluster representatives when minimizing size. When minimizing length, this solution adds at most an additive error of $2de$.% as in Lemma~\ref{lemma:rel_cluster_enough}.
\end{theorem}


\subsection{Construction of the Subsets}
For the \textsc{Set-Cover} construction, we use a sweep algorithm to determine relevant cluster representatives for each of the given trajectories. The idea is to extend each representative until it is relevant, report the corresponding cluster, and proceed with the endpoint of this representative as the starting point of the next representative.

With this approach, we ensure that we only obtain representatives of which a minimal selection covering the whole input data also fulfills the additional criteria stated in the definition. As we cut the representative every time when the part $G^*$ as described in the problem definition changes, we obtain both: On the one hand, by giving each representative (corresponding subset) a unit weight, an optimal solution for the whole data can only be achieved when the solution for each part $G^*$ is minimal. On the other hand, if we limit the selection to relevant representatives we avoid artificial splits. Note that during the sweep algorithm, we only need to report clusters where all cluster curves (no only the representative) are restricted, i.e. start and end at a vertex. Details on this remark can be found in \citep{DBLP:conf/gis/BuchinKK18}.

%Furthermore, we ignore a relevant cluster if %and only if
%there are cluster curves which do not start and end at vertices of the augmented data.
%The argument is given in the following paragraph.
%
%\paragraph*{Avoiding Clusters with Open Cluster Curves}
%%The insertion of new vertices may cause unintended artifacts, where clusters may be relevant, but we still do not need to report them. Moreover reporting this cluster with representative $\tau_1$ and proceeding with the endpoint of the representative as starting point for the new cluster representative $\tau_2$ might be a bad choice, if there are cluster curves which are only represented by the concatenation of $\tau_1$ and $\tau_2$.
%%In the following we argue that we can omit clusters when there are cluster curves with no suitable restricted correspondence.
%%For this we introduce the notions of \emph{open} and \emph{closed} cluster curves.
%
%The insertion of new vertices may cause unwanted artifacts. These artifacts are clusters, which are relevant by definition, but where the corresponding representatives $\tau_1$ and $\tau_2$ cannot be part of the optimal solution, because there might be subtrajectories which are only represented by the concatenation $\tau$ of $\tau_1$ and $\tau_2$. Using a sweep algorithm wherein we update the starting point each time we detect a relevant cluster, we would never report the cluster represented by $\tau$, which might be part of the optimal solution. Let us illustrate this observation with an example: The subtrajectories marked in green, of the input shown in Figure \ref{fig:needToCheckAllSubtrajectories}, form a minimal GD. The representative from $v$ to $w$ is a relevant representative, because the lowest curve enters the $d-$tube of one of the cluster curves if we extend the representative. If we split the representative, we need an additional representative for $A$ and $B$. Hence, we cannot have a minimal representation of the trajectories.
%
%In the following, we argue that we can omit clusters when there are cluster curves with no suitable restricted correspondence. For this, we introduce the notions of \emph{open} and \emph{closed} cluster curves.
%
%\begin{definition}
%Given a restricted representative $\tau$ with endpoints $a$ and $b$. A curve $s$ in $c(\tau)$ is \emph{closed} if there is a vertex $v$ with distance at most $d$ to $a$ and a vertex $w$ with distance at most $d$ to $b$. Otherwise the cluster curve is \emph{open}.
%\label{def:open_closed}
%\end{definition}
%
%\begin{figure}
%\centering
%%\captionsetup{justification=centering}
%\captionsetup{width=.5\textwidth}
%\includegraphics[width=0.4\textwidth]{needToCheckAllSubtrajectories}
%\caption{Illustration of why we can omit clusters with open cluster curves.}
%\label{fig:needToCheckAllSubtrajectories}
%\end{figure}
%
%\begin{lemma}
%\label{lemma:closed_cluster_curves}
%Using a sweep algorithm along each trajectory it is sufficient to report only relevant clusters where all cluster curves are closed.
%%Furthermore, reporting clusters with open curves possibly compromises the minimality of the resulting GD.
%\end{lemma}
%
%\begin{proof}
%A cluster curve can only be \emph{open} if one endpoint of the representative is a vertex from the second step of vertex insertion. Otherwise, a cluster curve inherits a suitable vertex in the first or second step. Consider a representative $\tau$ with an open cluster curve $s$ in $c(\tau)$.
%Then, we can conclude that $s$ is represented only by $\tau$ or by another representative ending on a vertex of the second insertion step. Otherwise, there would be vertices within distance $d$ to %in the $\epsilon-$surrounding of
%the endpoint and starting point of $\tau$.
%
%If $\tau$ is a part of a minimal solution, we can extend $\tau$ until all cluster curves are closed, which is the case if we reach an input vertex or a vertex from the first insertion step, without having a greater total number or representatives. Let $\tau_e$ be the extension of $\tau$ where all cluster curves are closed and let $\tau_d = \tau_e \backslash \tau$ be the difference of $\tau_e$ and $\tau$. Furthermore, let $E$ denote the event which makes $\tau$ a relevant representative. We now have to ensure that we do not compromise the local minimality criteria by extending the cluster:
%There is no event within distance $d$ to the corresponding trajectory of $s$ until $\tau$ is extended to $\tau'$    such that $s$ is a closed curve in $c(\tau')$. Thus, we can conclude that the part of $G^*_{\tau_d}$ which is not effected by $E$ does not change. Thus, if we find a better representation for the part of $G^*_{\tau_d}$ which is effected by $E$ and delete $\tau_d$ as representative, we have to add at least one representative to represent the part of $G^*_{\tau_d}$ which is not affected by the event. Thus, in total we do not get a smaller representation for $G^*_{\tau_d}$ as by keeping $\tau_d$ as representative and represent the curve causing $E$ either by itself or by its former representative.
%\end{proof}
%
The algorithm described above is summarized in the following pseudocode. The correctness of Algorithm \ref{alg:sweep} follows from Lemma \ref{lemma:rel_cluster_enough} 
%and Lemma \ref{lemma:closed_cluster_curves}.
For a given trajectory $T$ of complexity $N$, $T_{a,b}$ for $a,b \in \{1,2,...,N\}$, $a < b$, denotes the subtrajectory starting at vertex number $a$ and ending at vertex number $b$.

\begin{algorithm}
 \textbf{Input} $k$ trajectories $\{T_1, T_2,...,T_k\}$ of complexity $N_j$, $j = 1,...,k$\\
 boolean variable \textit{minimizingLength} \# \textsc{True} for minimal total edge length, \textsc{False} for minimal number of edges \\ 
 empty list $\mathcal{S}$ \% \textit{all computed subsets are added to} $\mathcal{S}$ \\
 empty list $R$ \% \textit{all representatives (subtrajectories) of the clusters (subsets) are added to} $R$\\

 \textbf{Output} list $R$, list $\mathcal{S}$

 \For{$j=1,2,..,k$}{
 	$T \leftarrow T_j$\\
 	$a \leftarrow 0$\\
 	$b \leftarrow 1$\\
 	% $control\_relevance \leftarrow True$\\
	\Repeat{$b > N_j$}{
	 \Do{$is\_irrelevant$ and $b <= N_j$ }{
	  compute $c(T_{a,b})$ \\
	 $is\_irrelevant \leftarrow checkRelevance(T_{a,b}, T_{a,b+1},c(T_{a,b}))$\\	
	 	 	 	$b  \leftarrow b+1$\\

	 }
	 add all segments of curves in $c(T_{a,b})$ as subset to $\mathcal{S}$.\\
	  Set the weight according to the minimality criteria:\\
	\uIf{minimizingLength}
	{ $w(c(T_{a,b})) \leftarrow \text{length}(T_{a,b})$}
	\Else {
	$w(c(T_{a,b})) \leftarrow 1 $}
	  Add $T_{a,b}$ to $R$ with the same index as the corresponding subset in $\mathcal{S}$.

	 $a \leftarrow b - 1$\\
	}
	}
 \caption{Compute subsets for \textsc{Set-Cover} instance}
 \label{alg:sweep}
\end{algorithm}

\begin{algorithm}
 \textbf{Input} $T_{a,b}$, $T_{a, b+1}$, $c(T_{a,b})$ \\
\uIf{all cluster curves start and end at vertex of the augmented input}{%are closed}{
compute $c(T_{a, b+1})$ \\
 \uIf{$\textsc{L}_{T_{a,b}}$ equal to  $\textsc{L}_{T_{a,b+1}}$}{
     \uIf{$G^*_{T_{a,b}}$ smaller than $G^*_{T_{b,b+1}}$ }  {
    return  \textsc{False}%$\tau$, $c(\tau_l)$ \;
  }
  \uElse{
    return  \textsc{True} \;
  }
  }
  \Else{
    return \textsc{False}% $\tau$, $c(\tau_l)$ \;
  }
  }
    \Else{
    return \textsc{True} \;
  }
	
 \caption{checkRelevance}
\end{algorithm}

When solving the constructed \textsc{Set-Cover} instance, we only need to keep track of the indices of the chosen sets in $\mathcal{S}$ to select the corresponding representatives from $R$ to build the GD.
For each trajectory, we maintain a list $\textsc{L}_{\tau}$ of all trajectories which maintain subtrajectories within distance $d$ to the current representative $\tau$.
Furthermore, we store the part $G^*$ based on the last segment of $T_{a,b}$.
If all cluster curves start and end at a vertex and if $T_{a,b}$ is a relevant cluster representative, that is
%are closed
if $|L|$ increases or decreases when we extend $T_{a,b}$ by one segment or if new cluster curves are added to $G^*$  when extending $T_{a,b}$, we report $T_{a,b}$ and the cluster $c(T_{a,b}$) as one subset in $\mathcal{S}$ of the \textsc{Set-Cover} instance.
%To verify if all cluster curves are closed,
%are closed,
%we perform two queries, one where we allow arbitrary
%%open
%cluster curves and one where we allow only restricted curves. After computing the clusters, we only have to compare the corresponding list of the trajectories which have parts in the cluster.

\paragraph*{Computational Complexity}
For reporting the clusters, we use the same approach as in \citep{clustering}. First, we compute the Free-Space Diagram (FS-Diagram) for the input trajectories which can be done in $\mathcal{O}(k^2n^2)$ time. Note that the time complexity for computing the FS-Diagram only depends on the complexity of the input trajectories, which is at most $n$ for each of the $k$ trajectories.

Let $N = \max_{j=1,...,k}N_j$. For each trajectory, we have to perform at least $N$ queries in case that only the whole trajectory is a relevant cluster representative and  at most $2N$ queries as we have at most $2N$ different positions of $a$ and $b$.
Each query takes $\mathcal{O}(kNl)$ time \citep{clustering}, where $l$ is the number of segments of the cluster representative. The average of $l$ ($l_{avg}$) per query is maximal if the whole trajectory is a relevant cluster representative. In this case, we have:
$l_{avg} = \frac{1}{N}\sum_{j=1}^N{j} = \frac{N+1}{2} \in \mathcal{O}(N).$
Thus, the total query time sums up to $\mathcal{O}(kN^3)$.
Now we determine the complexity of the additional steps of the algorithm.
To obtain $G^*$, we have to compute the cluster represented by the last segment of the current representative, $\tau_{last\_segment}$. In total, we have to perform $\mathcal{O}(kN)$ queries of length 1. Thus, the complexity is  $\mathcal{O}(k^2N^2)$.
%
Each cluster $c(T_{b-1,b})$ contains at most $k$ cluster curves of complexity at most $N$. Thus, the complexity to compute $G^* = c(c(T_{b-1,b}))$ is $\mathcal{O}(kN^2)$ and for $kN$ segments we have $\mathcal{O}(k^2N^3)$ in total.
As the lists described in the algorithm have size at most $k$, we can compare them in $\mathcal{O}(k)$ time. Having $\mathcal{O}(kN)$ comparisons, the time complexity is $\mathcal{O}(k^2N)$ for these computations.

\begin{theorem}\label{theorem:alg_fr}
Given a GD instance, we can compute in $\mathcal{O}(k^2N^3)$ time a \textsc{Set-Cover} instance of size $|\mathcal{U}| = \mathcal{O}(kN)$ and $|\mathcal{S}| = \mathcal{O}(kN)$, the solution of which solves the GD instance.
\end{theorem}

The theorem follows directly from the analysis above.
A discussion about the complexity of a trajectory after two steps of vertex insertion and the computation time of the insertion can be found in \citep{DBLP:conf/gis/BuchinKK18}.
Note that the time for computing the subsets described in the algorithms above dominates the time complexity of the preprocessing.

%\subsection{Complexity of the Trajectories} Here, we want to analyze the complexity of the trajectories after two steps of inserting vertices and state some restrictions to reduce the complexity. These restrictions are reasonable in the sense that in general the trajectories of moving entities of one group do not intersect arbitrarily often.
%
%\begin{lemma}
%\label{lemma:worst_case_complexity}
%After two steps of inserting vertices, the total complexity is $\mathcal{O}(k^4n^4)$ for an input $\mathcal{T}$ of size $kn$, i.e. consisting of $k$ trajectories of complexity at most $n$ each.
%\end{lemma}
%\begin{proof}
%%With no further restriction
%Every segment of a trajectory can possibly intersect all other segments from the input. Each intersection will (depending on $d$) lead to the introduction of up to four new vertices. Similarly, one can construct an example where each vertex from the input is propagated to all other segments of the input data.
%% (see Figure \ref{fig:tooManyIntersections}).
%% Recall that the size of the input data is $kn$, where $k$ is the number of trajectories of complexity $n$.
%
%After one step of adding new vertices, the complexity of one trajectory is $\mathcal{O}(kn^2)$ since each segment inherits $\mathcal{O}(kn)$ new vertices. Thus, the total number of vertices after one step is $\mathcal{O}(k^2n^2)$. After another step of inserting vertices the total complexity is $\mathcal{O}(k^4n^4)$.
%
%Alternatively, we can argue with the FS-Diagram. Recall that the FS-Diagram consists of $kn \times kn$ cells and each cell contains a constant number of critical points. Inserting new vertices is equivalent to propagating these critical points vertically. So each of the $kn$ lowest cell boundaries of the FS-Diagram inherits up to $kn$ critical points.
%\end{proof}
%
%
%%\begin{figure}
%%\includegraphics[scale=0.7]{Figures/tooManyIntersections}
%%\caption{Example where every segment intersects with every segment of the other trajectory}
%%\label{fig:tooManyIntersections}
%%\end{figure}
%
%To reduce the complexity, we state some conditions on the input data which seem to be reasonable for many applications, especially if the data does not contain many zig-zags. %is predominantly zig-zag free.
%\begin{description}
%  \item[C1] A disk of radius $d$ around any vertex intersects at most $c_1$ segments of another trajectory.
%  \item[C2] Each segment intersects at most $c_2$ segments of another trajectory.
%  \item[C3] A d-tube around one segment of length $l$ intersects at most $c_3$ segments of another trajectory.
%\end{description}
%
%Figure \ref{fig:zigzag} shows an example of a data set where \textbf{C1} is violated for $c_1 < 3$ (the $d$-disk around $v$ intersects 3 segments of $\tau_2$), \textbf{C2} for $c_2 < 12$ (the segment $s$ intersects 12 segments of $\tau_1$) and \textbf{C3} is violated for $c_3 < 14$ (the $d$-tube around $t$ intersects 14 segments of $\tau_1$).
%
%\begin{figure}
%\centering
%%\captionsetup{justification=centering}
%\includegraphics[width=1\columnwidth]{tooManyIntersections}
%\caption{Example of a data set with high complexity.}
%\label{fig:zigzag}
%\end{figure}
%
%
%\begin{lemma}
%\label{lemma:complexity_trajectories}
%If the restrictions \textbf{C1, C2, C3} hold for each trajectory in the data set, then each trajectory has complexity $\mathcal{O}(k^2C^2n)$, where $C:= c_1+c_2+c_3$. Computing this segmentation takes $\mathcal{O}(k^4C^2n^2)$ time.
%\end{lemma}
%
%\begin{proof}
%After one step of inserting new vertices, we have complexity $\mathcal{O}(k^2Cn)$, since each of the $\mathcal{O}kn$ seg\-ments and ver\-ti\-ces causes the introduction of at most $kC$ new vertices. In the second step only new vertices of the type shown in Figure \ref{fig:insertVertices}$a$ occur. Also in the second step, we want condition \textbf{C1} to be valid. Therefore, the complexity after two steps of inserting new vertices is $\mathcal{O}(k^3C^2n)$. As usually $k \ll n$ this is linear with respect to the dominating input size $n$.
%
%Given $k$ trajectories with each at most $n$ vertices. The first step of inserting new vertices takes $\mathcal{O}(k^2n^2)$ time, because we need to perform $k^2n^2$ vertex-segment comparisons and $k^2n^2$ segment-segment comparisons. The insertion of a vertex triggered by a vertex-segment or segment-segment comparison takes constant time. In the the second step we have $k$ trajectories of complexity at most $kCn$. Thus, inserting new vertices (type 2 vertices only) takes $k^4C^2n^2$ time.
%%There might be more efficient ways of inserting the vertices as described here, but as the time complexity $\mathcal{O}(k^4C^2n^2)$ of this naive approach is not the bottleneck of the overall time complexity for setting up a \textsc{Set-Cover} instance we do not further investigate better approaches here.
%\end{proof}


\section{Group Diagrams Based on the Equal- and Similar-Time Distance}\label{sec:et}
Next, we want to compute a GD based on equal-time distance as similarity measure. A path $P$ within a group diagram is similar to an input trajectory $T$ if for any $t$ in the domain of $T$ the Euclidean distance $dist(P(t), T(t))$ is at most $d$.

The following observation is a direct consequence of the linear interpolation between two vertices of a trajectory:
\label{obs:linearity}
Given two piecewise-linear trajectories $T_1$, $T_2$ with vertices at the locations corresponding to the timestamps $t_0,...,t_m$. Then, if $dist(T_1(t_i), T_2(t_i)) \leq d$ and $dist(T_1(t_{i+1}), T_2(t_{i+1})) \leq d$, we have $dist(T_1(t), T_2(t)) \leq d$ for all $t \in (t_i,t_{i+1})$.


\subsection{Segmentation} Exploiting this observation, %remark~\ref{obs:linearity}
we insert a sufficient number of timestamps and corresponding vertices additional to the input vertices to ensure that in between consecutive timestamps the pairwise equal-time distance of the trajectories does not change with respect to the threshold $d$. We do this by simulating equal-time distance first, i.e. inserting (by interpolation) a vertex to each trajectory for the at most $kn$ different timestamps. Subsequently, we consider only the common time interval of all trajectories. Then we compare all segments between two consecutive timestamps in a second step. Obviously, for computing a minimal GD it is sufficient to use restricted representatives with vertices in $V$, where $V$ is the set of vertices after the preprocessing.

Let $\overline{AB}$ and $\overline{CD}$ be two segments of different trajectories between two consecutive timestamps $i$ and $i+1$.
If $dist(\overline{AB}_t, \overline{CD}_t) \leq d$ holds for $t=t_i$ and $t=t_{i+1}$, the segments are at equal-time distance at most $d$ for all $t \in (t_i, t_{i+1})$ and we do not need to insert any new vertices. If $dist(\overline{AB}_t, \overline{CD}_t) \leq d$ holds for $t_i$ but not for $t_{i+1}$, the equation $dist(\overline{AB}_t, \overline{CD}_t) = d$ has exactly one solution $t_s$ in $(t_i,t_{i+1})$, and we insert a new vertex to all trajectories (if possible) at the corresponding locations at $t_s$ \emph{(split event)}. Analogously, we calculate $t_s$ and insert new vertices if the inequality holds for $t_{i+1}$ but not for $t_i$ \emph{(merge event)}. Lastly, if the inequality does not hold for $t_i$ nor for $t_{i+1}$, the equation $dist(\overline{AB}_t, \overline{CD}_t) = d$ has either no solution or exactly two solutions $t_{\min}$ and $t_{\max}$ in $I$. In the first case, we can conclude that the segments do not share a part where the equal-time distance is less than or equal to $d$. In the latter case, we obtain one merge and one split event between $t_i$ and $t_{i+1}$. Again, we insert vertices to every trajectory at time $t_{\min}$ and $t_{\max}$.

Figure \ref{fig:equalTimeSegmentation} shows the trajectories $T_1$ and $T_2$ of two entities moving along a similar route, but with different speed. The sampled positions at $t_0$ and $t_1$ for $T_1$ and at $t_0$ and $t_2$ for $T_2$ are shown as discs. A vertex at $t_1$ is added to $T_2$ to simulate equal time sampling and vertices at the timestamp where the distance of the entities is exactly $d$ (at $t_{new}$) is added to both trajectories. The part where the two entities were travelling within equal-time distance is highlighted with thick, red edges.

\begin{figure}
\centering
%\captionsetup{justification=centering}
\includegraphics[width=0.8\columnwidth]{maxSimilarPartEqualTime}
\caption{Equal-time segmentation of two trajectories.}
\label{fig:equalTimeSegmentation}
\end{figure}


\begin{lemma}
\label{lem:segmentation_time}
The segmentation takes $\mathcal{O}(k^4 n \log n)$ time. After this process each of the $k$ trajectories has at most $k^3 n$ vertices.
\end{lemma}
\begin{proof}
Each comparison takes constant time. The stated complexity results from the number of comparisons which is $\mathcal{O}(k^3n)$ and the time needed for one vertex propagation which is $\mathcal{O}(k\log n)$.
\end{proof}

\subsection{Computing the GD}
For computing the GD, we proceed in the following way: Between each two consecutive timestamps in $V,$ we compute one subset for each segment which contains the indices of all other segments within equal-time distance at most $d$. The distance between two segments is the maximum of the Euclidean distance between the two starting points of the segments and their two ending points.

Then we solve the \textsc{Set-Cover} instance and report the segments that correspond to the selected subsets. When minimizing the total edge number of the GD, we have to ensure that the representation does not change when not necessary in terms of minimality. Otherwise, the GD consists of edges that could be concatenated. This can happen because the solution of the \textsc{Set-Cover}, in general, is not unique. To maintain one representation as long as possible, we check if the representation $R_{old}$ between the previous two timestamps still represents all segments between the current two timestamps and if the size of $R_{old}$ equals the size of the current solution. In this case, we maintain $R_{old}$ and proceed with the next timestamp. This additional step is not necessary when minimizing the total edge length as the sum of the length of a minimal length representation between a series of consecutive timestamps within a time frame from start $t_s$ to end time $t_e$ is at most the minimal length of a representation looking at the whole interval $[t_s, t_e]$ at once.

\begin{lemma}
\label{lem:computation_time}
For each timestamp, we can compute the $k$ sets of the \textsc{Set-Cover} instance in $\mathcal{O}(k^2)$ time.
\end{lemma}

Lemma~\ref{lem:segmentation_time} and Lemma~\ref{lem:computation_time} imply the following theorem:

\begin{theorem}\label{theorem:alg_fr}
Given a GD instance using equal-time distance, we can compute in $\mathcal{O}((k^5 + k^4 \log n)n)$ time $\mathcal{O}(k^3n)$ \textsc{Set-Cover} instances each of size $|\mathcal{U}| = k$ and $|\mathcal{S}| = k$ the solution of which solves the GD instance.
\end{theorem}

\subsection{Similar-Time Distance}
Equal-time distance may be too restrictive for some applications, for example for entities which travel the exact same route, but such that each entity reaches each position with a small delay.  We introduce the similar-time distance as an additional similarity measure for such settings.

\begin{definition}\label{def:similar-time-distance}
Given two trajectories $T_1$ and $T_2$, where $T_1$ has size $m$ and $T_2$ has size $n$. We say that $T_1$ has $\alpha$-similar-time distance at most $d$ to $T_2$ if for all timestamps $t_i$, $i = 0,1,...,n-1$ there exists $\hat{t}_i \in [t_i - \alpha, t_i+\alpha]$, $\hat{t}_{i+1} \in [t_{i+1}-\alpha, t_{i+1} + \alpha]$, $\hat{t}_{i} \leq \hat{t}_{i+1}$, with $dist(q_i,\hat{p}_i) \leq d$, $dist(q_{i+1},\hat{p}_{i+1}) \leq d$, where $\hat{p}_i$ is the location of $T_1$ at time $\hat{t}_i$ and $q_i$ is the location of $T_2$ at $t_i$, and for all $t \in [\hat{t}_i,\hat{t}_{i+1}]$ $T_1$ stays within a $d$-tube around $q_iq_{i+1}$.
\end{definition}

Figure \ref{fig:similarTime} shows the same two trajectories as Figure \ref{fig:equalTimeSegmentation}. The trajectory $T_1$ has $\alpha-$similar-time distance to $T_2$ for any $\alpha \geq 20$ minutes. Between 00:00 and 00:50 the entities are moving within similar-time distance.

\begin{figure}
\centering
%\captionsetup{justification=centering}
\includegraphics[width=0.8\columnwidth]{similarTime}
\caption{Illustration of $\alpha-$similar-time distance.}
\label{fig:similarTime}
\end{figure}

For computing a GD with similar-time distance as similarity criteria, we use the same approach as for equal-time distance and only adapt the construction of the sets for the \textsc{Set-Cover} instance for each timestamp by comparing each segment with the segments of the other trajectory in the interval given by the allowed time shift.

In addition to the conditions of Definition~\ref{def:similar-time-distance}, one can demand that the length of the time interval between $\hat{t}_i$ and $\hat{t}_{i+1}$ does not fall below a given minimal value, for instance a fraction of the length of the interval $[t_i, t_{i+1}]$. Thus, we still allow a time shift when two entities travel along a similar route, but additionally, demand that the duration of traveling a similar route does not differ too much:

\begin{definition}\label{def:a_b_similar-time-distance}
Given two trajectories $T_1$ and $T_2$, where $T_1$ has size $m$ and $T_2$ has size $n$. We say that $T_1$ has $\alpha$-$\beta$-similar-time distance at most $d$ to $T_2$ if for all timestamps $t_i$, $i = 0,1,...,n-1$ there exists $\hat{t}_i \in [t_i - \alpha, t_i+\alpha]$, $\hat{t}_{i+1} \in [t_{i+1}-\alpha, t_{i+1} + \alpha]$, $\hat{t}_{i} \leq \hat{t}_{i+1}$, with $dist(q_i,\hat{p}_i) \leq d$, $dist(q_{i+1},\hat{p}_{i+1}) \leq d$, where $\hat{p}_i$ is the location of $T_1$ at time $\hat{t}_i$ and $q_i$ is the location of $T_2$ at $t_i$, and for all $t \in [\hat{t}_i,\hat{t}_{i+1}]$ $T_1$ stays within a $d$-tube around $q_iq_{i+1}$ and $\hat{t}_{i+1} - \hat{t}_i \geq \beta(t_{i+1} - t_i)$.
\end{definition} 