\documentclass[]{interact}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{subcaption}
\captionsetup{compatibility=false}
\usepackage{natbib}
\bibpunct[, ]{(}{)}{;}{a}{}{,}
\renewcommand\bibfont{\fontsize{10}{12}\selectfont}

\theoremstyle{plain}% Theorem-like structures
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}[theorem]{Proposition}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}

\theoremstyle{remark}
\newtheorem{remark}{Remark}
\newtheorem{notation}{Notation}

\usepackage[ruled,lined]{algorithm2e}
\SetKwRepeat{Do}{do}{while}
%\setlength{\belowcaptionskip}{-10pt}
%\graphicspath{{./graphics/}}%helpful if your graphic files are in another directory

%\bibliographystyle{plainurl}% the recommended bibstyle

% Author macros::begin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\h}{Hausdorff distance}
\newcommand{\fr}{Fr\'echet distance}
\newcommand{\Fr}{Fr\'echet Distance}
\newcommand{\dfr}{discrete Fr\'echet distance}
\newcommand{\Dfr}{Discrete Fr\'echet Distance}
\newcommand{\wfr}{weak Fr\'echet distance}
\newcommand{\Wfr}{Weak Fr\'echet Distance}
\newcommand{\cfr}{continuous Fr\'echet distance}
\newcommand{\fsd}{free space diagram}
\newcommand{\eps}{\varepsilon}
\newcommand{\IR}{\mathbb{R}}
\newcommand{\IQ}{\mathbb{Q}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\etal}{\ \textit {et al.}}
\newcommand\polylog{{\rm polylog}}
\newcommand{\maike}[1]{{\bf Maike: }{\it #1}}
\newcommand{\bernhard}[1]{{\bf Bernhard: }{\it #1}}
\newcommand{\todo}[1]{{\bf TODO: } {\it #1}}

\begin{document}

\articletype{Special Issue on “Computational Movement Analysis”}

\title{Group Diagrams for Representing Trajectories}

%\author{
%\name{Maike Buchin \textsuperscript{a}\thanks{maike.buchin@tu-dortmund.de} and Bernhard Kilgus \textsuperscript{b}\thanks{bernhard.kilgus@rub.de} and Andrea K\"olzsch\textsuperscript{c,d}\thanks{akoelzsch@orn.mpg.de}}
%\affil{\textsuperscript{a}Faculty of Computer Science, TU Dortmund, Germany; \textsuperscript{b} Department of Mathematics, Ruhr University Bochum, Germany; \textsuperscript{c} Max Planck Institute for Ornithology, Germany; \textsuperscript{d}Department of Biology, University of Konstanz, Germany
%}}

\maketitle

\begin{abstract}
Given the trajectories of one or several moving groups, we propose a 
new framework, the \emph{group diagram (GD)} for representing these. 
Specifically, we seek a minimal
GD as a concise representation of the groups maintaining the
spatio-temporal structure of the groups' movement. A GD is specified by
three input values, namely a distance threshold, a similarity measure and
a minimality criterion. For several variants of the GD, we give a
comprehensive analysis of their computational complexity and present
efficient approximation algorithms for their computation.

Furthermore, we experimentally evaluate our algorithms on GPS data of
migrating geese. Applying the proposed methods on these data reveals 
how the GD provides a condensed picture of the movement of 
the groups which can be used for further analysis and comparisons and 
how the GD can help to formulate new hypotheses for
further ecological research. We use different similarity measures to summarize the
migration routes of (i) a goose family for one migration period and to
summarize (ii) the migration routes of one individual for several migration
periods or (iii) the migration routes of several independent individuals for
one migration period.
\end{abstract}

%\begin{CCSXML}
%<ccs2012>
%<concept>
%<concept_id>10002951.10003227.10003236.10003237</concept_id>
%<concept_desc>Information systems~Geographic information systems</concept_desc>
%<concept_significance>500</concept_significance>
%</concept>
%</ccs2012>
%\end{CCSXML}
%
%\ccsdesc[500]{Information systems~Geographic information systems}
%
%
\begin{keywords}
Movement Analysis; Trajectory Analysis; Computational Geometry; \Fr; Equal-Time Distance
\end{keywords}


%\paragraph*{Acknowledgments.} This work was supported by the Deutsche Forschungsgemeinschaft (DFG), project BU 2419/3-1. We are grateful to Gerard M\"uskens and the Dutch Association of Goose Catcher for help with catching and tagging goose families. AK acknowledges funding from the DLR (ICARUS directive).

\input{introduction}


\input{preliminaries}

\input{algorithms}

\input{experiments}



%\appendix

%\input{appendix}

\section{Conclusion}
The group diagram framework presented in this paper provides a method for a concise representation of the data of moving entities forming one or several moving groups. In particular, a concise visual representation is obtained by plotting the GD representatives onto a map. The GD can be adapted to several different types of movement and research areas by specifying the similarity measure and the minimality criterion accordingly. We presented the general algorithmic approach to compute a GD and provided a detailed theory on how to compute the GD for the equal-time, the similar-time, and  the  \fr\ as similarity measure. With these distances, one can compute GD for groups where all individuals are moving simultaneously or at different periods of time. 

We find that our experimental results on data of migrating geese give an accurate picture of the movement for different types of groups of geese (family, one individual for multiple periods, multiple independent individuals). The GD give an indication of the similarity between individual migration routes for different distance thresholds. It extracts typical migration routes from a given set of migration trajectories which can be plotted on a map to obtain a representation of the group's movement. The example of different GD patterns between bursts of flight over land vs. water reveals the usefulness of the group diagram method beyond representation. Here, it can even contribute to form novel hypotheses for ecological research, like the different inner-group behavior when flying over land or water.

In the future, it would be interesting to apply group diagrams to more data sets. Data sets of groups moving together, where several individuals are tracked simultaneously or data sets of one individual traveling a similar route during  multiple periods of time have been quite rare up to now, but are becoming more common. Hence, we hope to perform more experiments with data from different fields, such as vehicle tracking or hiking data in the future.

\bibliographystyle{tfv}
\bibliography{groups_bib}
\end{document}
