\section{Introduction}\label{sec:intro}

%\todo{add more motivation to the problem}
The amount of available movement data, such as GPS data collected by mobile devices has increased massively during the last years. Movement ecology, for instance, is a growing field at the edge of biology and GIScience which benefits from new possibilities of tagging animals with smart devices collecting location data and several other parameters. Analysing this data poses new algorithmic challenges~\citep{cma}. One of the challenges is to (visually) represent the large amount of data, such that the representation is as concise as possible, but still preserves the information needed for further analysis and processing. In particular, this challenge arises for movement trajectories of several interacting entities.

In this paper, we take on this challenge and present a concise representation for data of groups of moving objects.
Our approach is the following. We assume we are given the trajectories of several moving objects, called \emph{entities}, each of which is given by a series of sampled locations. By linearly interpolating in between each two consecutive timestamps, the corresponding trajectory becomes a polygonal line.
We assume we are given $k$ trajectories, each of complexity $n$ (number of vertices) forming one or several (overlapping, i.e., splitting and merging) groups. We introduce a new framework, named the \emph{group diagram} (GD) as a means of a concise representation of these groups. In particular, the GD allows a concise visual representation. 

The group diagram identifies groups moving together, that can be represented by one of the individuals. Its definition is quite general and in particular, allows for different ways of treating time. 
More specifically, we define the GD as a geometric graph (see the next section for precise definitions) that represents all of the input trajectories, in the sense that for each input trajectory there is a similar path in the group diagram. Furthermore, the group diagram should have minimal size with respect to the number/length of the edges in the geometric graph.


%\begin{figure}
%\includegraphics[scale = 1]{Figures/groupDiagram}
%	\centering
%	\caption{Illustration of merging and splitting groups and their representation as GD.}
%	\label{fig:illustration}
%\end{figure}



\subsection{Related work}\label{sec:rw}
Two related notions to the GD are the grouping structure and flow diagrams.
The grouping structure is defined as the unique graph which represents all density-con\-nected groups traveling at equal-time~\citep{grouping}.
It can be seen as a specialization of the GD that uses density connectedness at equal timestamps as inner group distance.
As such the grouping structure is well suited for grouping entities moving together, but not for entities travelling, e.g. commuting or migrating, at different times. Also, in some settings, such as commuting, pairwise distance is more suitable than density connectedness. For instance, commuters moving in slow traffic along a major road would form a group in the grouping structure, even though individual commuters in this group may be far away from each other. Also note, that density connectedness and equal-time alignment of the time-stamped locations are inherent to the grouping structure (which is based on the Reeb graph), hence it would not generalize to other distance measures, as do the group diagrams. Moreover, the Reeb graph that forms the grouping structure does not consist of representatives that capture the movement of each entity because density connectedness does not allow to represent a group by one individual that is close to all in the group. In contrast, for each entity, the group diagram contains a path which is similar to the corresponding trajectory of the entity. 
 
\cite{grouping} gave efficient algorithms for computing the grouping structure, analyzed its complexity,
and demonstrated its usefulness on data sets of deer, elk, and cattle.
The grouping structure was later generalized by \cite{grouping2} for the geodesic distance
and by \cite{grouping3} for varying parameters. Furthermore, \cite{refined_grouping2016} refined the grouping structure by restricting the density connectedness to within a group, claiming that this is a more intuitive definition since \cite{grouping} allow two entities of one group to be density connected via other entities, not in the group. However, a grouping structure based on the refined definition is less efficient to compute. \cite{grouping_exp2018} experimentally compared the refined and original grouping structure and showed that the difference between the two definitions increases if the density in the crowd increases.
\cite{multi_granular_trend_detection} build on the work of \cite{grouping} and \cite{refined_grouping2016} to detect trends in one-dimensional time-varying data. 

A flow diagram is a minimal (in the number of vertices) diagram representing segmentations of all input trajectories. In a flow diagram vertices represent segmentation criteria and edges transitions between criteria~\citep{flow}. As a simple example, a trajectory of a person in one day could be roughly segmented to 
\emph{home - work - home}. 
The flow diagram can be seen as a generalization of the GD (after switching between vertices and edges) where criteria are more general than small distance of the trajectories. However, for this one would need to consider infinitely many criteria of the form ``travelled this route at this time'', which is not computationally feasible. 
Buchin et al. showed that deciding if a flow diagram of a certain size exists is NP-hard (even W[1]-hard in the number of trajectories). Hence, they give efficient heuristics for computing flow diagrams and evaluate these on a data set of football players.

Further related approaches for detecting and representing groups of moving objects are flocks \citep{reporting_flocks, longest_flocks, flocks_detecting_patterns, flocks_online_discovering}, convoys \citep{convoys}, swarms \citep{swarms} and herds \citep{herds, olap}. They differently incorporate time and space in their definition of a group, for instance, some concepts require that the entities stay in a disc whereas other use density connectedness \citep{density-based} as grouping criterion.

The only concept that captures splitting and merging of (sub)groups is the herd concept~\citep{herds, olap}. In contrast to our approach, they use density-connected groups and define splitting and merging of groups based on a definition of recall and precision measure of a herd. Furthermore, this concept only exhibits the evolution of a group for discrete timestamps whereas the group diagram graphs show the evolution of one or several groups continuoulsy from the beginning to the end of the movement of each entity. Moreover, in contrast to the group diagrams, no natural representatives, with respect to time and space, capturing the movement of each entity are computed. \cite{olap} present OLAP (online analytical processing) for trajectories for detecting groups based on geographic overlap and intersection. Their approach is based on mapping the trajectories to a two-dimensional spatial grid. In contrast, our approach works with precise geometric distance and allows different ways of handling the temporal component. 

Computing a GD using the \fr\ is also related to map construction algorithms, wherein the goal is to determine the underlying network of a set of trajectories~\citep{map-construction}.
%
Similar modeling choices (i.e. choice of what constitutes edges, which similarity measure, and which minimality condition) occur in the problem of finding a representative (e.g. median \citep{median_trajectories}, middle \citep{middle_trajectories}, central \citep{central_trajectories},...) trajectory of a set of similar trajectories.

\subsection{Contribution}\label{sec:contribution}
We present a new framework called group diagram to concisely represent the movement of one or several moving groups. The framework inherently differs from all previous methods and approaches of representing the movement of groups. Our work is motivated by the problem of finding such a representation for the movement of groups of migrating greater white-fronted geese (anser a. albifrons). For this specific research question, we present experimental results for one goose family and for groups of independent individual geese. The input data consist of one trajectory for each entity, given by a number of time-stamped GPS-locations. To obtain a concise representation, we compute a minimal GD for the input trajectories and for a visual representation, we  plot the representatives of the GD on a map. This representation, for instance, shows typical migration routes for this species of goose or highlights where the goose family was flying close together and where they were moving with larger pairwise distance. Varying the distance threshold used to compute the GD is used to investigate for which distance the family stayed ``together'' during the whole migration route or for a given percentage of the whole observation period. Furthermore, we generated synthetic data similar to the input to examine the scalability of the methods and the implementation.

Our framework allows several problem sensitive specification, like the criterion specifying the conciseness of the group diagram and the similarity measure between the moving objects. Therefore, we believe the framework can be applied for a wide range of research questions in the field of movement analysis and beyond.

In this paper we present the general framework and algorithmic idea to compute the GD and a comprehensive theoretical elaboration, that is a presentation of the computational complexity and of specific algorithms for three similarity measures. These measures are the equal-time, the similar-time and the \fr. The motivation behind focusing on these measures is that they cover the range of computing GD for groups where all objects move simultaneously (equal-time distance), within a given time-shift (similar-time distance) or time-independent, that is at different periods of time (\fr). As minimality criterion, we consider the minimal total edge length of the GD and the minimal number of edges.

The definition of the framework and the general algorithmic approach can be found in Section~\ref{sec:prelim}. In Section~\ref{sec:complexity} we show that computing a minimal GD is NP-hard for all the variants we consider in detail. Hence, we develop approximation algorithms in Section~\ref{sec:fr} and Section~\ref{sec:et}. The results of applying the GD on data of migrating geese is presented in Section~\ref{sec:ex}.

This paper extends the results of \citep{DBLP:conf/gis/BuchinKK18} presented at the International Workshop on Computational Transportation Science, 2018. Here, we give a more detailed presentation of the general algorithmic approach, and a comprehensive analysis of the computational complexity of all variants of the GD we consider.
% experimental evaluations with one similarity measure, namely the equal-time similarity - a measure which is, for instance, suitable to compute a GD of a goose-family migrating within the same flock. In this paper, we claim and proof that all variants of the GD we consider are NP-hard and
Furthermore, we explore the use of the \fr\ as a similarity measure for GD. This measure allows computing GD for trajectories recorded within different periods of time as this distance only considers the ordering of the timestamps (vertices) along the trajectory and not the specific time at each location. For instance, using the \fr\ as similarity measure, we can summarize the migration routes of one individual for several migration periods. Considering one migration period, we can summarize the migration routes of several independent individuals where the individuals do not necessarily migrate simultaneously.


%Figure~\ref{fig:groupDiagram} gives an example, where the subtrajectories forming a minimal GD % for the given trajectories
%are marked in red.
%
%\begin{figure}
%\centering
%\includegraphics[width=0.55\columnwidth]{groupDiagram}
%	\caption{Example of a group diagram.}
%	\label{fig:groupDiagram}
%\end{figure}

% Overview
%In the next section we precisely define and study the computational complexity of this problem.
%As it turns out, the problem of computing a minimal group diagram is NP-hard for both the equal-time and the \fr.
%Hence in the following sections, we develop approximation algorithms, which we then experimentally evaluate on real and synthetic data sets.
