\section{Definition and Algorithmic Approach}\label{sec:prelim}

\subsection{Preliminaries}
First, we introduce \DIFdelbegin \DIFdel{the terminology we use and then formally define group diagrams}\DIFdelend \DIFaddbegin \DIFadd{our terminology and then we formally define GD}\DIFaddend .
%\paragraph*{Terminology}
%We will use the following terms:
A \emph{trajectory} describes the movement of an entity over time. \DIFdelbegin \DIFdel{Therefore, it has }\DIFdelend \DIFaddbegin \DIFadd{This means, it entails }\DIFaddend a temporal and a spatial component. The movement of an entity is assumed to be continuous, but it is measured at a discrete set of times. 
Formally, a \emph{trajectory} $T = ((p_1, t_1), (p_2, t_2),\dots,(p_n, t_n))$ of complexity $n$ is an ordered sequence of positions in time $t_i$ and space $p_i$, \DIFdelbegin \DIFdel{where }\DIFdelend \DIFaddbegin \DIFadd{whereby }\DIFaddend the space is two- or three-dimensional. In this paper, space is always two-dimensional. We assume that an entity moves with constant velocity on a straight line between two time-stamped locations. That is, we use linear interpolation between two time-stamped locations to obtain a continuous movement from $p_1$ to $p_n$. Therefore, we can interpret $T$ as a polygonal curve. In the following, we denote the time-stamped locations as vertices of $T$. 
A \emph{subtrajectory} refers to an arbitrary but connected part of the trajectory starting and ending at arbitrary points along the trajectory. We call a subtrajectory starting and ending at vertices a \emph{restricted} subtrajectory. A subtrajectory that is not necessarily restricted is an \emph{arbitrary} subtrajectory. A \emph{segment} refers to a subtrajectory consisting only of two consecutive vertices of the trajectory and the linear interpolation between them.

A \emph{cluster} is a set of trajectories that are similar (under some similarity measure) to one \emph{representative} of the cluster.
A cluster representative is thus a subtrajectory within a cluster to which all other subtrajectories in the cluster have distance at most $d$. A \emph{cluster curve} is a subtrajectory within \DIFdelbegin \DIFdel{some }\DIFdelend \DIFaddbegin \DIFadd{a }\DIFaddend cluster. % (not necessarily starting and ending at vertices).
Given a cluster representative $\tau$, we denote by $c(\tau)$ the set of all cluster curves of the corresponding cluster. For a set of representatives $A$, $c(A)$ is the union of all cluster curves represented by a representative in $A$.

\subsection{Definition of the Group Diagram Framework}

We propose the following general definition for a group diagram:
\begin{definition}\label{def:gd}
 A group diagram (GD) is a geometric graph with vertices augmented by a temporal component, which represents all input trajectories $\mathcal{T}$.
 We say the graph represents a trajectory $T \in \mathcal{T}$ if there is a similar path $P$ in the graph,
 that is $T$ and (the geometric representation of) $P$ are similar under a given similarity measure.
 We say a GD is minimal if it is minimal in size, either with respect to its number
 of edges or the total length of edges.
\end{definition}

Note that a group according to Definition~\ref{def:gd} can consist of not only several individuals, but also one individual and its movement for several different periods of time.
Furthermore, the definition allows to make several choices:
how to represent vertices and edges of the GD, the similarity measure, and whether to minimize the number or the total (geographical) length of the edges.

We consider GD that are built from the input trajectories, i.e., edges of the GD are represented by subtrajectories of the input. These give realistic representatives \DIFdelbegin \DIFdel{because they represent a real movement leaving aside errors of measurement and the impact of interpolation }\DIFdelend \DIFaddbegin \DIFadd{in the sense that they show an actual movement of an entity, despite interpolation between recorded locations and measurement errors}\DIFaddend . In contrast, newly generated representatives might not satisfy natural restrictions and might, for instance, \DIFdelbegin \DIFdel{traverse }\DIFdelend \DIFaddbegin \DIFadd{pass through }\DIFaddend a lake although this is not possible for the specific entities. Furthermore, two edges share a vertex if the endpoints of the corresponding subtrajectories are within distance $d$ from each other. Endpoints of edges \DIFdelbegin \DIFdel{with no }\DIFdelend \DIFaddbegin \DIFadd{without a }\DIFaddend $d$-distance neighbor have degree one.
Vertices in the graph are thus embedded as the set of endpoints of incident edges.
We will use such graphs in the following.
Note that we could transform these into planar embedded graphs, for instance by connecting all points of a point set $S$ of a vertex to the midpoint of $S$.
% or its convex hull. % (not requiring this would allow representatives to go through impassable regions).

As similarity measure, we consider three popular measures on trajectories: the \fr, \mbox{equal-,} and similar-time distance.
All of these, measure the similarity as the maximum distance under certain alignments of the temporal component:
equal-time aligns equal timestamps, similar-time allows a bounded shift in time, and the \fr\ allows \DIFdelbegin \DIFdel{arbitrary shifts but }\DIFdelend \DIFaddbegin \DIFadd{for arbitrary shifts, however }\DIFaddend still respecting the ordering in time (see \DIFdelbegin \DIFdel{the respective sections for precise definitions}\DIFdelend \DIFaddbegin \DIFadd{Section~\ref{sec:fr} for a formal definition}\DIFaddend ). Figure~\ref{fig:gd_examples} shows GD for the three distance measures for an input of four trajectories.
%
\begin{figure}[t]
\centering
\DIFdelbeginFL %DIFDELCMD < \includegraphics[width=0.85\textwidth]{GD_examples}
%DIFDELCMD < %%%
\DIFdelendFL \DIFaddbeginFL \includegraphics[width=0.75\textwidth]{GD_examples}
\DIFaddendFL \caption{GD for an input of four trajectories. \DIFdelbeginFL \DIFdelFL{The GD are marked }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{As depicted }\DIFaddendFL in \DIFdelbeginFL \DIFdelFL{red. In }\DIFdelendFL the upper figure, the \DIFdelbeginFL \DIFdelFL{timestamp of }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{upper two trajectories show a movement which starts at 00:00, }\DIFaddendFL the \DIFdelbeginFL \DIFdelFL{first vertex }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{third trajectory shows a movement which starts with a delay }\DIFaddendFL of \DIFdelbeginFL \DIFdelFL{each }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{10 minutes and the fourth }\DIFaddendFL trajectory \DIFdelbeginFL \DIFdelFL{is shown}\DIFdelendFL \DIFaddbeginFL \DIFaddFL{shows a movement with a delay of 100 minutes in comparison to the first two trajectories}\DIFaddendFL . \DIFdelbeginFL \DIFdelFL{The }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{Note that the period of }\DIFaddendFL time \DIFdelbeginFL \DIFdelFL{difference }\DIFdelendFL between two consecutive vertices is 10 minutes. \DIFdelbeginFL \DIFdelFL{There }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{The GD for the three different similarity measures }\DIFaddendFL are \DIFdelbeginFL \DIFdelFL{two trajectories with equal starting time}\DIFdelendFL \DIFaddbeginFL \DIFaddFL{marked in red}\DIFaddendFL .
\DIFdelbeginFL \DIFdelFL{Within }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{For }\DIFaddendFL the \DIFdelbeginFL \DIFdelFL{GD with }\DIFdelendFL equal-time distance\DIFdelbeginFL \DIFdelFL{as similarity measure these }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{, the upper two }\DIFaddendFL trajectories are \DIFdelbeginFL \DIFdelFL{represented by one representative }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{summarized }\DIFaddendFL if they are within a spatial distance of at most $d$. \DIFaddbeginFL \DIFaddFL{A more rigorous summary of the trajectories is not possible, since the timestamps of the other trajectories do not align with the timestamps of the upper two. }\DIFaddendFL When similar-time distance with an allowed time shift of 10 minutes is applied, \DIFdelbeginFL \DIFdelFL{this }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{it is possible to summarize the upper three trajectories by one }\DIFaddendFL representative \DIFdelbeginFL \DIFdelFL{also captures }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{whenever }\DIFaddendFL the \DIFdelbeginFL \DIFdelFL{movement }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{spatial distance }\DIFaddendFL of the \DIFdelbeginFL \DIFdelFL{entity which corresponds to the trajectory starting with a 10 minutes delay}\DIFdelendFL \DIFaddbeginFL \DIFaddFL{trajectories is at most $d$}\DIFaddendFL . \DIFdelbeginFL \DIFdelFL{Applying }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{Finally, when applying }\DIFaddendFL the \fr, \DIFdelbeginFL \DIFdelFL{all subtrajectories that are within }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{only the }\DIFaddendFL spatial distance \DIFdelbeginFL \DIFdelFL{at most $d$ to one representative are summarized in the GD}\DIFdelendFL \DIFaddbeginFL \DIFaddFL{is considered}\DIFaddendFL , independent of the specific time \DIFaddbeginFL \DIFaddFL{of }\DIFaddendFL the \DIFdelbeginFL \DIFdelFL{entities where moving along the trajectories}\DIFdelendFL \DIFaddbeginFL \DIFaddFL{movements}\DIFaddendFL .}
\label{fig:gd_examples}
\end{figure}


The generality of our definition allows to apply it in different settings, e.g. to entities moving at the same or at different times. Also note \DIFdelbegin \DIFdel{, }\DIFdelend that the information of the number of subtrajectories similar to one edge of the GD (\textit{representation strength} of an edge) can be \DIFaddbegin \DIFadd{used}\DIFaddend , for instance, \DIFdelbegin \DIFdel{used }\DIFdelend to indicate most commonly traveled routes by adjusting the thickness of each edge according to its representation strength when drawing the GD.

Minimizing the number of edges or their total length seems intuitively reasonable, especially if we want \DIFaddbegin \DIFadd{to obtain}\DIFaddend , for instance, \DIFdelbegin \DIFdel{to obtain }\DIFdelend a concise representation of the groups movement plotted on a map. 
However, both can lead to unwanted effects: When minimizing \DIFaddbegin \DIFadd{the }\DIFaddend number of edges, similar parts of the movement of two entities might not be captured and when minimizing the total length, two unconnected vertices within distance $d$ represent the same part of the input as one segment of length $d$. For instance\DIFaddbegin \DIFadd{, }\DIFaddend the shortest representation of a single trajectory would consist of a series of points at distance $2d$ (see Figure~\ref{fig:strangeEffects} for an illustration). Thus, we need to \DIFdelbegin \DIFdel{state }\DIFdelend \DIFaddbegin \DIFadd{formulate }\DIFaddend further requirements to prevent these effects\DIFdelbegin \DIFdel{:
The effects }\DIFdelend \DIFaddbegin \DIFadd{. 
The requirements }\DIFaddend are the following:
\begin{itemize}
\label{list:additional_conditions}
%  \item When minimizing number of edges, similar parts of the movement of two entities might not be captured: If two trajectories are close to each other only partially, a smallest possible group diagram consists of the two trajectories rather than representing their similar parts with a joint edge.
%  \item When minimizing length,
%\end{itemize}
%Figure \ref{fig:strangeEffects} illustrates these effects. A minimal group diagram for the given trajectories is highlighted in red.
%\begin{figure}
%\includegraphics[scale=0.6]{Figures/strange_effects}
%\centering
%	\caption{Unintuitive group diagrams (highlighted in red) fulfilling the criteria of minimal edge length (left) and minimal number of edges (right).}
%	\label{fig:strangeEffects}
%\end{figure}

\begin{figure}[t]
\centering
%\captionsetup{width=.9\textwidth}
\includegraphics[width=0.9\textwidth]{strange_effects}
	\caption{\DIFdelbeginFL \DIFdelFL{Unintuitive }\DIFdelendFL \DIFaddbeginFL \DIFaddFL{Counterintuitive }\DIFaddendFL GD with a minimal number of edges (left) and minimal edge length (right).}
	\label{fig:strangeEffects}
\end{figure}

  \item \DIFdelbegin \DIFdel{For }\DIFdelend \DIFaddbegin \DIFadd{When }\DIFaddend minimizing the number of edges, we require \DIFdelbegin \DIFdel{that }\DIFdelend the minimality criterion \DIFdelbegin \DIFdel{is fulfilled also }\DIFdelend \DIFaddbegin \DIFadd{to be fulfilled }\DIFaddend for every local part of the input:
Given a GD $\mathcal{G}$, an arbitrary subtrajectory $\tau$ of one of the representatives in $\mathcal{G}$, and the corresponding cluster $c(\tau)$\DIFdelbegin \DIFdel{. We }\DIFdelend \DIFaddbegin \DIFadd{, we }\DIFaddend use $G_\tau^* := c(c(\tau))$ to denote the union of all clusters represented by a curve in $c(\tau)$.
We intersect $G_\tau^*$ with the GD $\mathcal{G}$ to obtain all representatives $A_\tau := \mathcal{G} \cap G_\tau^*$.
The \emph{local minimality criterion} now requires that for each $\tau$ as described above the resulting set $A_\tau$ is a minimal representation for $c(A_\tau)$.

\item \DIFdelbegin \DIFdel{For }\DIFdelend \DIFaddbegin \DIFadd{When }\DIFaddend minimizing length, we require that no clusters are artificially split up to reduce the length.
   Formally, we require that no subgraph of the GD can be replaced by a subgraph with less connected components but possibly larger total edge length.
\end{itemize}

In the following, we \DIFdelbegin \DIFdel{always }\DIFdelend consider the problem with these
% corresponding 
additional requirements.
% depending on whether we measure size by number or length of edges.

\subsection{General Algorithmic Approach}\label{sec:algorithmic_appraoch}
First, we observe that each edge in a GD \DIFdelbegin \DIFdel{can be identified with }\DIFdelend \DIFaddbegin \DIFadd{corresponds to }\DIFaddend a representative of a cluster of subtrajectories from the input. This observation leads to the following general algorithmic idea:
We subdivide the problem of computing a minimal GD into a \emph{clustering} and a \emph{covering} problem. That is, we first detect a set of cluster representatives and then select a minimal set of these clusters \DIFdelbegin \DIFdel{where }\DIFdelend \DIFaddbegin \DIFadd{whereby }\DIFaddend the union covers the complete input. Thus, our approach is to construct and solve a \textsc{Set-Cover} instance with universe $\mathcal{U}$ consisting of all segments of the input trajectories.

To make this approach computationally feasible, we need to ensure that all representatives which are part of an optimal solution start and end at some vertex. However, in general, this does not hold for the segmentation of the trajectories given by the input vertices\DIFdelbegin \DIFdel{as }\DIFdelend \DIFaddbegin \DIFadd{. Here, }\DIFaddend a representative which is part of a minimal GD \DIFdelbegin \DIFdel{possible starts }\DIFdelend \DIFaddbegin \DIFadd{might start }\DIFaddend and end in between two input vertices along the trajectory. \DIFdelbegin \DIFdel{Therefore, we }\DIFdelend \DIFaddbegin \DIFadd{We therefore }\DIFaddend need to preprocess the input data to obtain a sufficiently fine \DIFaddbegin \DIFadd{grained }\DIFaddend segmentation. We call the obtained higher resolution data the \emph{augmented input}.   
%(Lemma~\ref{lemma:two_steps_are_enough}).

To take the two different minimality criteria into account, we can formulate a weighted \textsc{Set-Cover} problem \DIFdelbegin \DIFdel{where }\DIFdelend \DIFaddbegin \DIFadd{in which }\DIFaddend we assign a weight to each representative (subset) depending on which minimality condition we choose: unit weight for the number of edges, and length of representative for edge length.
\DIFaddbegin \DIFadd{The workflow of this general algorithmic approach is summarized in Figure~\ref{fig:workflow}. 
}\DIFaddend 

Note that the criterion for minimizing the edge number and the criterion for minimizing the total edge length are contrasting. For a minimal length, we \DIFdelbegin \DIFdel{demand that we do not cut the representatives }\DIFdelend \DIFaddbegin \DIFadd{require that the representatives are not cut }\DIFaddend too often whereas to fulfill the condition \DIFdelbegin \DIFdel{for }\DIFdelend \DIFaddbegin \DIFadd{of }\DIFaddend minimal edge number, we have to force some cuts so that we can ensure a minimal representation also for an arbitrarily small part of the input data. In Section~\ref{sec:segmentation} we formally define a \emph{relevant} cluster representative and show how \DIFdelbegin \DIFdel{the }\DIFdelend these representatives optimally balance the number of cuts, such that a solution which consists of relevant cluster representatives fulfills the additional \DIFdelbegin \DIFdel{criterion }\DIFdelend \DIFaddbegin \DIFadd{criteria }\DIFaddend for both minimization goals. 


\paragraph*{Solving \textsc{Set-Cover}}
As the \textsc{Set-Cover} problem is $NP$-complete, there is no exact polynomial time algorithm to solve the problem unless $P=NP$. We use an implementation of a greedy algorithm to approximate the constructed \textsc{Set-Cover} instance.
A greedy algorithm takes the largest subset in each step and deletes the points of the universe already covered from the other subsets which are not selected yet. Suppose $m$ is the minimum number of subsets (or the minimum total weight). Then\DIFaddbegin \DIFadd{, }\DIFaddend the greedy algorithm returns a solution of size less than $m\log(u)$, where $u$ is the cardinality of the universe. In fact, \textsc{Set-Cover} cannot be efficiently approximated better than to a factor of  $(1- o(1))\log(u)$ unless $P=NP$. The runtime of the greedy algorithm is bounded by $\mathcal{O}(su\min\{s,u\})$, where $s$ is the number of the given subsets.

\DIFdelbegin \DIFdel{The workflow of this general algorithmic approach is summarized in Figure~\ref{fig:workflow}. 
}\DIFdelend \begin{figure}[b]
\centering
%\captionsetup{width=1.\textwidth}
\includegraphics[width=1.\textwidth]{workflow.jpg}
	\caption{The workflow of the general algorithmic approach to compute a minimal GD. \DIFaddbeginFL \DIFaddFL{All processed data is marked with trapezoids and the computation steps with rectangles. The input data is processed first to obtain a sufficiently fine grained segmentation. Then the clustering step is applied on this augmented data and outputs a }\textit{\DIFaddFL{Set-Cover}} \DIFaddFL{instance which is solved in the last step to obtain a minimal GD.}\DIFaddendFL }
	\label{fig:workflow}
\end{figure}


\section{Computational Complexity}\label{sec:complexity} %
In this section, we prove that computing the minimal \DIFdelbegin \DIFdel{group diagram }\DIFdelend \DIFaddbegin \DIFadd{GD }\DIFaddend is NP-hard for all variants we consider. For this, we reduce an NP-complete variant of the \textsc{Dominating-Set} problem to the decision version of our problem. The decision version is formulated in the following definition. \DIFaddbegin \DIFadd{This section justifies developing approximate solutions in the following and can be skipped by the reader not interested in computational complexity theory. 
}\DIFaddend 

\begin{definition}\label{def:gd_dv}
Given $k$ trajectories and an integer value $l$\DIFdelbegin \DIFdel{. The }\DIFdelend \DIFaddbegin \DIFadd{, the }\DIFaddend \textsc{Group-Diagram} problem  is to decide whether there is a collection of subtrajectories $\mathcal{S}$ of total size $l$, such that every input trajectory has distance at most $d$ to at least one path in the group diagram consisting of the subtrajectories in $\mathcal{S}$. Again, if an endpoint of a subtrajectory and a start point of another subtrajectory are within distance at most $d$ they share a vertex in the group diagram.
\end{definition}
In this definition, the size of a \DIFdelbegin \DIFdel{group diagram }\DIFdelend \DIFaddbegin \DIFadd{GD }\DIFaddend is either the smallest integer greater than or equal to the total length of the edges, which we denote by \textsc{GD-Min-Length} or the number of edges in the \DIFdelbegin \DIFdel{group diagram}\DIFdelend \DIFaddbegin \DIFadd{GD}\DIFaddend , which we denote as \textsc{GD-Min-Edgenumber}. The distance of a trajectory to a path in the \DIFdelbegin \DIFdel{group diagram }\DIFdelend \DIFaddbegin \DIFadd{GD }\DIFaddend refers either to the \fr\ or \DIFaddbegin \DIFadd{to }\DIFaddend equal-time distance.

For the reduction to prove NP-hardness, we need to recall some definitions and results from the literature. We define the \textsc{Dominating-Set} problem and a variant with more geometric structure. This variant is still NP-complete and thus, we can reduce from this problem and exploit the geometric structure to show NP-hardness for \textsc{GD-Min-Length} and \textsc{GD-Min-Edgenumber} for a variant where all trajectories are parallel. \DIFdelbegin \DIFdel{From this}\DIFdelend \DIFaddbegin \DIFadd{Hence}\DIFaddend , we can conclude that both \textsc{GD-Min-Length} and \textsc{GD-Min-Edgenumber} are NP-hard to decide (and to compute) for arbitrary input trajectories. 

\begin{definition}\label{def:ds}
Given a graph $G=(V,E)$. A dominating set $D$ is a subset of $V$ such that every vertex from $V$ not in $D$ is adjacent to at least one vertex in $D$. Given an integer~$l$\DIFaddbegin \DIFadd{, }\DIFaddend the \textsc{Dominating-Set} problem is to decide whether there is a dominating set of size~$l$.
\end{definition}

\textsc{Dominating-Set} is known to be NP-complete for general graphs. However, the problem remains NP-hard for 
grid graphs, which was proven by \cite{unit-disk}:

\begin{definition}\label{def:grid}
Let $\mathcal{D}=\{(x,y)\in \mathbb{R}^2 |$  $x$ and $y$ are mul\-ti\-ples of $d\}$, where $d\in \mathbb{R}$. A \emph{grid} graph is a graph $G=(V,E)$\DIFdelbegin \DIFdel{wherein }\DIFdelend \DIFaddbegin \DIFadd{, }\DIFaddend $V\subset \mathcal{D}$ and wherein there is an edge between two vertices if and only if they are within distance $d$.
\end{definition}

\begin{lemma}\label{lemma:ds_on_grid}
\textsc{Dominating-Set} for grid graphs is NP-complete.
\end{lemma}

With these preliminary remarks, we can prove that \textsc{Group-Diagram} is NP-hard by a reduction from \textsc{Dominating-Set} for grid graphs.

\begin{theorem}\label{theorem:NP_fr}
Given an integer $l$, deciding whether there exists a GD of size~$l$ is NP-hard for both $l$ denoting the edge length and $l$ denoting the edge number, and for both \fr\ and equal-time distance as similarity criteria.
\end{theorem}

\begin{proof}
\textbf{\fr:}
Given an arbitrary grid graph $G=(V,E)$ with $V=\{v_1,v_2,...,v_k\}$\DIFdelbegin \DIFdel{. We }\DIFdelend \DIFaddbegin \DIFadd{, we }\DIFaddend construct an instance of \textsc{Group-Diagram} as follows\DIFdelbegin \DIFdel{. }\DIFdelend \DIFaddbegin \DIFadd{: }\DIFaddend For each $v \in V$ \DIFaddbegin \DIFadd{we }\DIFaddend place a horizontal segment with length less than $d$ in the plane wherein the coordinates of the starting point equal the coordinates of $v$.
See Figure~\ref{fig:reductionSketch} for an example.
\DIFdelbegin \DIFdel{If }\DIFdelend \DIFaddbegin \DIFadd{On the one hand, if }\DIFaddend two vertices in $G$ are adjacent\DIFaddbegin \DIFadd{, }\DIFaddend then the corresponding segments are at \fr\ at most $d$ as the segments \DIFdelbegin \DIFdel{have equal length and are }\DIFdelend \DIFaddbegin \DIFadd{are equally long and }\DIFaddend parallel. On the other hand, if \DIFaddbegin \DIFadd{the }\fr\DIFadd{\ between }\DIFaddend two (sub)segments\DIFdelbegin \DIFdel{are within }%DIFDELCMD < \fr%%%
\DIFdel{\ }\DIFdelend \DIFaddbegin \DIFadd{, $s_1$ and $s_2$, is }\DIFaddend at most $d$\DIFdelbegin \DIFdel{then the corresponding }\DIFdelend \DIFaddbegin \DIFadd{, then the }\DIFaddend vertices in $G$\DIFdelbegin \DIFdel{are adjacent}\DIFdelend \DIFaddbegin \DIFadd{, that correspond to $s_1$ and $s_2$, are adjacent in $G$}\DIFaddend . This follows from the construction of the segments. If two vertices $v_1$, $v_2$ are not adjacent in $G$, then any point along the corresponding segment to $v_1$ has distance greater than $d$ to any point along the segment corresponding to the vertex $v_2$.

Now suppose there is a dominating set $I=\{v_{i_1},...v_{i_l}\}$ of size~$l$ for $G$. Let $\hat{I} = \{s_{i_1},...s_{i_l}\}$ denote the set of the corresponding segments. For each $s \notin \hat{I}$ there is a vertex $v\in I$ adjacent to the corresponding vertex $v_s$ and it follows that $s$ and $s_v$ are within distance $d$. Therefore, any input trajectory is represented by at least one ``path'' consisting of only one input segment. Now consider a set $J$ of (sub-)segments of size $l$ which represents the whole data. Particularly for each starting point $p$ of the constructed segments there is a (sub-)segment in $J$ with starting point $q$, where $dist(p,q) \leq d$. As the starting points of the segments equal the vertices in the graph, a dominating set of size $l$ for $G$ consist of vertices corresponding to the (sub-)segments of $J$.

Note that a minimal solution in this setting also fulfills the local minimality criterion. This follows from the observation that for each subtrajectory $\tau_s$ of a given trajectory $T$, the cluster $c(\tau_s)$ contains the same (shrunken) cluster curves as $c(T)$. Therefore, for any representative $r$ of the \DIFdelbegin \DIFdel{group diagram }\DIFdelend \DIFaddbegin \DIFadd{GD, }\DIFaddend the part $G^*$ relative to any subtrajectory of $r$ is represented in a minimal way by the corresponding subsegments of the \DIFdelbegin \DIFdel{group diagram}\DIFdelend \DIFaddbegin \DIFadd{GD}\DIFaddend .

For proving NP-hardness of \textsc{Group-Diagram} using total edge length as size, we fix the distance $d=1$ and the length of the segments induced by the vertices of $G$ to $l = d$. To fulfill the additional condition for \DIFdelbegin \DIFdel{group diagrams }\DIFdelend \DIFaddbegin \DIFadd{GD }\DIFaddend of minimal total edge length, observe that a solution consists of whole segments as the segments are parallel and starting points are within distance $d = 1$. Thus, each representation of two segments within distance $d$ must have \DIFdelbegin \DIFdel{length }\DIFdelend \DIFaddbegin \DIFadd{a length of }\DIFaddend at least 1 and a representation of several subsegments can be replaced by a smaller subgraph in the GD by choosing one segment as representative. Hence, \DIFdelbegin \DIFdel{only representatives which consist of a whole segment result in a }\DIFdelend \DIFaddbegin \DIFadd{a }\DIFaddend GD which fulfills the demanded \DIFdelbegin \DIFdel{condition and a }\DIFdelend \DIFaddbegin \DIFadd{is composed of representatives which correspond to a whole segment only. Furthermore, a }\DIFaddend solution of \textsc{Group-Diagram} with total edge length $l$ consists of exactly $l$ segments where the corresponding vertices are a solution of \textsc{Dominating-Set} of size $l$ in $G$. Obviously, a solution of \textsc{Dominating-Set} of size $l$ implies a solution of \textsc{Group-Diagram} of total edge length $l$, by choosing the corresponding segments as GD edges.

This proves NP-hardness of the problem of finding a minimal group diagram for a given set of input trajectories. Note that NP-hardness is shown for the special case of parallel, equal size trajectories with only one segment (and in case of minimal edge length with distance $1$).

\textbf{Equal-time distance:}
%Given a group diagram constructed by $l$ segments from the input data. For each of the at most $l+1$ time stamps corresponding to the start- and endpoints of the segments, we insert a vertex to each input trajectory at the corresponding location. To decide if for each input trajectory there is a similar path in the group diagram, we consecutively compare each segment of the new partition of an input trajectory with the segments within the group diagram which start and end at the same time stamp as the segment of the trajectory. Two segments are within equal-time distance $d$ if and only if their start- and endpoints are within equal-time distance $d$. Inserting new vertices and all comparisons can be done in polynomial time, thus \textsc{Group Diagram} with respect to equal-time similarity is in NP.

To prove the NP-hardness, note that a \DIFdelbegin \DIFdel{group diagram }\DIFdelend \DIFaddbegin \DIFadd{GD }\DIFaddend with a limited number of segments, in particular, provides us with a representation of the objects for every timestamp with limited size. Thus\DIFaddbegin \DIFadd{, }\DIFaddend it suffices to prove the NP-hardness of the problem of deciding if there is a representation of a given size for a single timestamp. This again can be easily proven by a reduction from \textsc{Dominating Set} (see Definition~\ref{def:ds}) by identifying the location of the entities for a single timestamp with the vertices of a graph $G$ where we insert an edge between two vertices if and only if the corresponding entities are within distance at most $d$. Evidently, a representation of the objects of size~$l$ equals a dominating set in $G$ of size~$l$.
\end{proof}

\begin{figure}
\centering
%\captionsetup{width=.7\textwidth}
\includegraphics[scale=0.7]{reductionSketch}
\caption{A grid graph and its corresponding placement of segments \DIFaddbeginFL \DIFaddFL{(right)}\DIFaddendFL . The solution of the \textsc{Dominating Set} and the \textsc{Group Diagram} instances are highlighted in red.} %\maike{add some labels}
\label{fig:reductionSketch}
\end{figure}

\DIFdelbegin \DIFdel{Due to NP-hardness, we develop approximation algorithms in the next sections.
}\DIFdelend %DIF > Due to NP-hardness, we develop approximation algorithms in the next sections.

%\begin{figure}[b]
%\includegraphics[width=1\columnwidth]{Figures/reductionSketch}
%\caption{A grid graph and its corresponding placement of segments. The solution of the \textsc{Dominating Set} and the \textsc{Group Diagram} instances are highlighted in red.} %\maike{add some labels}
%\label{fig:reductionSketch}
%\end{figure}


%In the following sections we give approximation algorithms and their experimental evaluation on a real data set.
