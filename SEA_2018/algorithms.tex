\section{\Fr\ }\label{sec:fr}

\subsection{Formulating the Problem as Set-Cover Instance}\label{sec:sc}

To efficiently solve the problem approximately we subdivide it into a clustering and a covering problem:
Each edge in a GD can be identified with a representative of a cluster of subtrajectories from the input. We first detect a set of cluster representatives and then select a minimal set of these clusters where the union covers the complete input. Thus our approach is to construct and solve a \textsc{Set-Cover} instance with universe $\mathcal{U}$ consisting of all segments of the input trajectories. To make this approach computationally feasible we segment the trajectories such that we only need to consider subtrajectories starting and ending at vertices of the segmentation as cluster representatives.

As we construct and approximately solve a \textsc{Set-Cover} instance we obtain  approximation algorithms in both cases, minimizing the total size of the GD and minimizing the length. When minimizing length for the \fr\ we additionally make a small additive error for each edge of the GD, see Lemma \ref{lemma:rel_cluster_enough}.

%To make this approach computationally feasible we segment the trajectories such that we only need to consider the discrete subtrajectories starting and ending at vertices of the segmentation. With this restriction we make a small error in terms of minimal length in comparison with allowing arbitrary subtrajectories as representatives.
%Note that no error is introduced when minimizing the number of edges.
%However, for both variants we construct and approximately solve a \textsc{Set-Cover} instance, hence obtaining approximation algorithms in both cases.
%Hence in the following we ignore this term when referring to the GD optimal solution.
%with a term in $\mathcal{O}(d)$, where $d$ is the given distance threshold.
%i.e. trajectories segmented at vertices as representatives and cluster curves.

%The error when minimizing length is at most $2d$ per representative, hence we get a total additive error of at most $2de$, where $e$ is the number of edges of an optimal solution. When each segment has length greater than $4d$, which is the case in our experiments, we have an multiplicative error of at most 2 when using only representatives starting and ending at vertices of the segmentation.
%This additive error is tight: Consider an input setting of trajectories of complexity one and length greater than $2d$, where $d$ is the distance threshold and where always two trajectories are congruent and the pairs are within distance greater than $d$. Here we need one representative for each pair (one whole trajectory) and each representative is $2d$ longer than a minimal representative which is not restricted to start and end at vertices. However we will use a sufficiently fine segmentation such that the total additive error cannot be greater than in this example.

To take the two different minimality criteria into account we can formulate a weighted \textsc{Set-Cover} problem where we assign a weight to each representative (subset) depending on which minimality condition we choose: unit weight for number of edges, and length of representative for edge length.
%
In the following we call the  ``computation of the subsets of a \textsc{Set-Cover} instance to compute a GD'' shortly a \textit{Set-Cover} \emph{construction}.

Recall that the \emph{\fr} between two curves $\tau$ and $\sigma$ parameterized over $[0,1]$ is defined as the infimum over all reparameterizations $\alpha$ and $\beta$ on $[0,1]$ of the maximum over all $t \in [0,1]$ of $\|\tau(\alpha(t)) - \sigma(\beta(t)) \|$ \cite{frechet}.
%
To compute a minimal GD with \fr\ as similarity criteria we use a sweep algorithm with two moving points $a$ and $b$ along every trajectory and report all \emph{relevant} clusters represented by the subtrajectory between the current positions of $a$ and $b$ as described in~\cite{clustering}.

\subsection{Segmentation}
Next we consider how to segment the input trajectories and choose relevant representatives.
A first approach may be to use the segmentation of the trajectories given by the vertices of the input. However we observe that this segmentation is not fine enough to obtain a minimal representation. % of the data.
An example is shown in Figure~\ref{fig:inputNotEnough}, where we do not find a representative representing all three subtrajectories in the middle part.
%
Hence we need to insert new vertices along the trajectories. We denote the obtained data as \emph{augmented input data}.

\begin{figure}
\centering
 \includegraphics[width=0.9\textwidth]{Figures/inputNotEnoughBoth}
 %\hspace{10mm}
 %\includegraphics[scale=0.5]{Figures/inputNotEnoughRepresentative}
\caption{Input vertices are not sufficient for a minimal representation.}
\label{fig:inputNotEnough}
\end{figure}

%\bernhard{introduce the definition of a relevant cluster $\rightarrow$ formulate requirements of the segmentation: for all not-restricted relevant cluster representative there is a restricted cluster representative of similar length representing the same cluster.}
Before we develop a strategy of inserting vertices, we define what a relevant cluster representative is.
We will show (Lemma~\ref{lemma:rel_cluster_enough}) that it suffices to consider only these representatives.
Recall that a cluster representative is a subtrajectory of the input and cluster curves are the subtrajectories within a fixed distance $d$ to the representative. We use $\eps$-tube to
refer to the $\eps$-neighborhood of a subtrajectory $\tau$, i.e., the Minkowski sum of the curve $\tau$ and the ball $B_\eps$.

\begin{definition}
\label{def:relevantCluster}
A cluster representative $\tau$ which represents the cluster $c(\tau)$ is \emph{irrelevant} if it can be extended to $\tau'$ such that $c(\tau')$ contains only extended curves of $c(\tau)$ and $|c(\tau)|=|c(\tau')|$ and no other trajectory not in the cluster enters a $d$-tube around one of the cluster curves. If a cluster representative cannot be extended in such a way the representative and the corresponding cluster are \emph{relevant}.
\end{definition}

\begin{lemma}
\label{lemma:rel_cluster_enough}
When minimizing size, there always exists a minimal GD solution where edges correspond to relevant cluster representatives. When minimizing length, this solution adds at most an additive error of $2de$, where $d$ is the distance threshold and $e$ is the number of edges of an optimal solution.
\end{lemma}

Note that this additive error is tight: Consider an input setting of trajectories of complexity one and length greater than $2d$, where always two trajectories are congruent and the pairs are within distance greater than $d$. Here only the whole trajectories are relevant and therefore each representative (whole trajectory) is $2d$ longer than a minimal representative (middle part with distance $d$ to endpoints).

When each segment has length greater than $4d$, which is the case in our experiments, we have a multiplicative error of at most 2 when using only relevant representatives.

%We state two requirements the segmentation must fulfill in order to allow us to only consider restricted cluster representatives (Lemma~\ref{lemma:requirements_are_enough}).
We now describe how to obtain a sufficiently fine segmentation such that all relevant clusters are restricted subtrajectories. More details and the proofs can be found in the appendix.

%\textbf{Requirement 1:}
%Given a relevant cluster representative $\tau$. Then there is a restricted representative $\hat{\tau}$ which is at most $2d$ longer than $\tau$ and represents the same cluster as $\tau$.
%the same parts of the augmented input data.

%\textbf{Requirement 2:}
%Given a relevant cluster representative $\tau$ starting and ending at vertices and a cluster curve $c$ of trajectory $\sigma$ in the cluster of $\tau$. Then there must be a subtrajectory $s$ of $\sigma$ starting and ending at vertices, such that $c$ and $s$ do not differ too much, in the sense, that .. and $s$ is still represented by $\tau$.
%If two representatives $\tau$ and $\tau'$ represent overlapping cluster curves $c$ and $c'$ there exists a vertex in the overlap of $c$ and $c'$.

%\begin{lemma}
%\label{lemma:requirements_are_enough}
%If requirement 1 and requirement 2 are fulfilled all relevant cluster curves start and end at vertices.
%we find all relevant cluster representatives to compute a minimal group diagram by solving the corresponding \textsc{Set-Cover} instance, when we restrict the representatives and the cluster curves to start and end at vertices of the new segmentation. The local error of the length of the representative in comparison with an arbitrary representative is bounded by $2d$.
%\end{lemma}

\subparagraph{Inserting New Vertices}
%\bernhard{A short discussion what intuitively is meant here is needed as one can also argue that the movement represented by the upper segment is different to the one represented by the lower one, so they should not be represented by just one of them.}
We consider two different triggers for inserting a new vertex. Firstly, for every vertex $v$ of the input data we add a vertex to every segment which has distance to $v$ less than or equal to $d$ (see Figure~\ref{fig:insertVertices}$a$) at the point along the segment where the distance to $v$ is minimal (type 1).
Secondly, we add a new vertex if the distance between two segments is less than $d$ for the first time and if the distance exceeds $d$ again (type 2) (see Figure~\ref{fig:insertVertices}$b$).
Type 1 introduces 0 or 1 new vertex for each vertex-segment pair and type 2 introduces 0, 2 or 4 vertices for each pair of segments.

\begin{figure}
\includegraphics[scale=0.7]{Figures/addingVertices}
\centering
\caption{Inserting new vertices. The vertices from the input are shown as disks whereas the newly added ones are marked as squares.}
\label{fig:insertVertices}
\end{figure}

\begin{lemma}
\label{lemma:two_steps_are_enough}
After two steps of inserting new vertices all relevant cluster representatives start and end at vertices of the augmented data.
\end{lemma}

Lemmas~\ref{lemma:rel_cluster_enough} and \ref{lemma:two_steps_are_enough} imply:
\begin{theorem}
After two steps of inserting new vertices there always exists a minimal GD solution where edges correspond to restricted cluster representatives, when minimizing size. When minimizing length, this solution adds at most an additive error of $2de$.
\end{theorem}


\subsection{Construction of the Subsets}
For the \textsc{Set-Cover} construction we use a sweep algorithm to determine relevant cluster representatives for each of the given trajectories. The idea is to extend each representative until it is relevant, report the corresponding cluster, and proceed with the endpoint of this representative as the starting point of the next representative.

With this approach we ensure that we only obtain representatives of which a minimal selection covering the whole input data also fulfills the additional criteria stated in the definition. Note that the criterion for minimizing the edge number and the criterion for minimizing the total edge length are contrasting. For a minimal length we demand that we do not cut the representatives too often whereas to fulfill the condition for minimal edge number we have to force some cuts, so that we can ensure a minimal representation also for an arbitrary small part of the input data. As we cut the representative every time when the part $G^*$ as described in the problem definition changes, we obtain both: By giving each representative (corresponding subset) a unit weight an optimal solution for the whole data can only be achieved when the solution for each part $G^*$ is minimal, too. On the other hand, if we limit the selection to relevant representatives we avoid artificial splits. Furthermore, we ignore a relevant cluster if and only if there are cluster curves which do not start and end at vertices of the augmented data.  %(open cluster curve).
The argumentation can be found in the appendix.

The algorithm described above is summarized in the following pseudocode. The correctness of Algorithm \ref{alg:sweep} follows from Lemma \ref{lemma:rel_cluster_enough} and Lemma \ref{lemma:closed_cluster_curves}. For a given trajectory $\tau$ of complexity $N$, $\tau_{a,b}$ for $a,b \in \{1,2,...,N\}$, $a < b$, is the subtrajectory starting at vertex number $a$ and ending at vertex number $b$.

\begin{algorithm}[H]
\label{alg:sweep}
 \textbf{Input} $k$ trajectories $\{\tau_1, \tau_2,...,\tau_k\}$ of complexity $N_j$, $j = 1,...,k$\\
 empty list $\mathcal{S}$ \% \textit{all computed subsets are added to} $\mathcal{S}$ \\
 empty list $R$ \% \textit{all representatives (subtrajectories) of the clusters (subsets) are to} $R$\\

 \textbf{Output} list $R$, list $\mathcal{S}$

 \For{$j=1,2,..,k$}{
 	$\tau \leftarrow \tau_j$\\
 	$a \leftarrow 0$\\
 	$b \leftarrow 1$\\
 	% $control\_relevance \leftarrow True$\\
	\Repeat{$b > N_j$}{
	 \Do{$is\_irrelevant$ and $b <= N_j$ }{
	  compute $c(\tau_{a,b})$ \\
	 $is\_irrelevant \leftarrow checkRelevance(\tau_{a,b}, \tau_{a,b+1},c(\tau_{a,b}))$\\	
	 	 	 	$b  \leftarrow b+1$\\

	 }
	 add all segments of curves in $c(\tau_{a,b})$ as subset to $\mathcal{S}$.\\
	  Set the weight according to the 		    minimality criteria:\\
	\begin{itemize}
	\item[] 	  $w(C) \leftarrow 1 $ for minimizing the edge number of the group diagram.

	\item[]	  $w(C) \leftarrow \text{length}(\tau_{a,b})$ for minimizing the total edge length.

\end{itemize}			
	  Add $\tau_{a,b}$ to $R$ with the same index as the corresponding subset in $\mathcal{S}$.

	 $a \leftarrow b - 1$\\
	}
	}
 \caption{Compute subsets for \textsc{Set-Cover} instance}
\end{algorithm}

\begin{algorithm}[H]
 \textbf{Input} $\tau_{a,b}$, $\tau_{a, b+1}$, $c(\tau_{a,b})$ \\
\uIf{all cluster curves can be restricted}{%are closed}{
compute $\tau_{a, b+1}$ \\
 \uIf{$\textsc{L}_{\tau_{a,b}}$ equal to  $\textsc{L}_{\tau_{a,b+1}}$}{
     \uIf{$G^*_{\tau_{a,b}}$ smaller than $G^*_{\tau_{a,b+1}}$ }  {
    return  \textit{False}%$\tau$, $c(\tau_l)$ \;
  }
  \uElse{
    return  \textit{True} \;
  }
  }
  \Else{
    return \textit{False}% $\tau$, $c(\tau_l)$ \;
  }
  }
    \Else{
    return True \;
  }
	
 \caption{checkRelevance}
\end{algorithm}

When solving the constructed \textsc{Set-Cover} instance we only need to keep track of the indices of the chosen sets in $\mathcal{S}$ to select the corresponding representatives from $R$ to build the GD.
For each trajectory we maintain a list $\textsc{L}_{\tau}$ of all trajectories which contain subtrajectories within distance $d$ to the current representative $\tau$.
Furthermore we store the part $G^*$ based on the last segment of $\tau$.
If all cluster curves can be restricted 
%are closed 
and if $L$ differs when we extend $\tau$ by one segment or if $G^*$ grows when extending $\tau$, then $\tau$ is a relevant representative and we report $\tau$ and the cluster $c(\tau$) as one subset in $\mathcal{S}$ of the \textsc{Set-Cover} instance.
To verify if all cluster curves can be restricted, 
%are closed, 
we perform two queries, one where we allow arbitrary 
%open 
cluster curves and one where we allow only restricted curves. The complexity of both queries is the same \cite{clustering}. After computing the clusters we only have to compare the corresponding list of the trajectories which have parts in the cluster.

\subparagraph{Complexity}
For reporting the clusters we use the same approach as in \cite{clustering}. First, we compute the Free Space Diagram for the input trajectories which can be done in $\mathcal{O}(k^2n^2)$ time. Note that the time complexity for computing the Free Space diagram only depends on the complexity of the input trajectories, which is at most $n$ for each of the $k$ trajectories.

Let $N = \max_{j=1,...,n}N_j$. For each trajectory we have to perform at least $N$ queries in case that only the whole trajectory is a relevant cluster representative and  at most $2N$ queries as we have at most $2N$ different positions of $a$ and $b$.
Each query takes $\mathcal{O}(kNl)$ time, where $l$ is the number of segments of the cluster representative. The average of $l$ ($l_{avg}$) per query is maximal if the the whole trajectory is a relevant cluster representative. In this case we have:
$l_{avg} = \frac{1}{N}\sum_{j=1}^N{j} = \frac{N+1}{2} \in \mathcal{O}(N).$


Thus the total query time sums up to $\mathcal{O}(kN^3)$.
Now we compute the complexity of the additional computations of the algorithm.
For computing  $G^*$ we have to compute the cluster represented by the last segment of the current representative, $\tau_{last\_segment}$. In total we have to perform $\mathcal{O}(kN)$ queries of length 1, thus the complexity is  $\mathcal{O}(k^2N^2)$.
%
Each cluster $c(\tau_{last\_segment})$ contains at most $k$ cluster curves of complexity at most $N$. Thus the complexity to compute $G^* = c(c(\tau_{last\_segment}))$ is $\mathcal{O}(kN^2)$ and for $kN$ segments we have $\mathcal{O}(k^2N^3)$ in total.
As the lists described in the algorithm have size at most $k$ we can compare them in $\mathcal{O}(k)$ time. Having $\mathcal{O}(kN)$ comparisons we have complexity $\mathcal{O}(k^2N)$ for these computations.

\begin{theorem}\label{theorem:alg_fr}
Given a GD instance, we can compute in $\mathcal{O}(k^2N^3)$ time a \textsc{Set-Cover} instance of size $|\mathcal{U}| = \mathcal{O}(kN)$ and $|\mathcal{S}| = \mathcal{O}(kN)$, the solution of which solves the GD instance.
\end{theorem}
\begin{remark}
The statement follows directly from the analysis above. A discussion about the complexity of a trajectory after two steps of vertex insertion and the computation time of the insertion can be found in the appendix (Lemma \ref{lemma:worst_case_complexity} and Lemma \ref{lemma:complexity_trajectories}). Note that the time for computing the subsets described in the algorithms above dominates the time complexity of the preprocessing.
\end{remark}


\subparagraph{Solving \textsc{Set-Cover}}
As the \textsc{Set-Cover} Problem is $NP$-complete there is no efficient algorithm to solve the problem unless $P=NP$.
A greedy algorithm takes the largest subset in each step and deletes the points of the universe already covered from the other subsets which are not selected yet. Suppose $m$ is the minimum number of subsets (or the minimal total weight). Then the greedy algorithm returns a solution of size less than $m\log_e(u)$, where $u$ is the cardinality of the universe. In fact \textsc{Set-Cover} cannot be efficiently approximated better than to a factor of  $(1- o(1))\log_e(u)$ unless $P=NP$. The run time of the greedy algorithm is bounded by $\mathcal{O}(su\min\{s,u\})$, where $s$ is the number of the given subsets.

\section{Equal- and Similar-Time Distance}
Next we want to compute a GD based on equal-time distance as similarity measure \cite{equal-time}. A path $P$ within a group diagram is similar to an input trajectory $\tau$ if for any $t$ in the domain of $\tau$ the Euclidean distance $dist(P(t), \tau(t))$ is at most  $d$.
The following observation follows directly from the linear interpolation between two vertices of a trajectory.
\begin{remark}
\label{obs:linearity}
Given two piecewise-linear trajectories $\tau_1$, $\tau_2$ with vertices at the locations corresponding to the time stamps $t_1,...,t_m$. Then if $dist(\tau_1(t_i), \tau_2(t_i)) \leq d$ and \\ $dist(\tau_1(t_{i+1}), \tau_2(t_{i+1})) \leq d$ we have $dist(\tau_1(t), \tau_2(t)) \leq d$ for all $t \in (t_i,t_{i+1})$.
\end{remark}

\subparagraph{Segmentation} Using this observation %remark~\ref{obs:linearity}
we insert a sufficient number of time stamps and corresponding vertices additional to the input vertices to ensure that in between consecutive time stamps the pairwise equal-time distance of the trajectories does not change with respect to the threshold $d$. We do this by simulating equal-time distance first, i.e., inserting (by interpolation) a vertex to each trajectory for the at most $kn$ different time stamps. Subsequently we consider only the common time interval of all trajectories. Then we compare all segments between two consecutive time stamps in a second step. Obviously for computing a minimal GD it is sufficient to use restricted representatives with vertices in $V$, where $V$ is the set of vertices after the preprocessing.

Let $\overline{AB}$ and $\overline{CD}$ be two segments of different trajectories between two consecutive time stamps $i$ and $i+1$.
If $dist(\overline{AB}_t, \overline{CD}_t) \leq d$ holds for $t=t_i$ and $t=t_{i+1}$ the segments are at equal-time distance at most $d$ for all $t \in (t_i, t_{i+1})$ and we do not need to insert any new vertices. If $dist(\overline{AB}_t, \overline{CD}_t) \leq d$ holds for $t_i$ but not for $t_{i+1}$ the equation $dist(\overline{AB}_t, \overline{CD}_t) = d$ has exactly one solution $t_s$ in $(t_i,t_{i+1})$ and we insert a new vertex to all trajectories (if possible) at the corresponding locations at $t_s$ \emph{(split event)}. Analogously we calculate $t_s$ and insert new vertices if the inequality holds for $t_{i+1}$ but not for $t_i$ \emph{(merge event)}. Lastly, if the inequality does not hold for $t_i$ nor for $t_{i+1}$ the equation $dist(\overline{AB}_t, \overline{CD}_t) = d$ has either no solution or exactly two solutions $t_{\min}$ and $t_{\max}$ in $I$. In the first case we can conclude that the segments do not share a part where the equal-time distance is less than or equal to $d$. In the latter case we obtain one merge and one split event between $t_i$ and $t_{i+1}$. Again, we insert vertices to every trajectory at time $t_{\min}$ and $t_{\max}$.

\begin{lemma}
The segmentation takes $\mathcal{O}(k^4 n \log n)$ time. After this process each of the $k$ trajectories has at most $k^3 n$ vertices.
\end{lemma}
\begin{proof}
Each comparison takes constant time. The stated complexity results from the number of comparisons which is $\mathcal{O}(k^3n)$ and the time needed for one vertex propagation which is $\mathcal{O}(k\log n)$.
\end{proof}

\subparagraph{Computing the GD}
For computing the GD we proceed in the following way. Between each two consecutive time stamps in $V$ we compute one subset for each segment, which contains the indices of all other segments within equal-time distance at most $d$. The distance between two segments is the maximum of the Euclidean distance between the two starting points of the segments and their two ending points.

Then we solve the \textsc{Set-Cover} instance and report the segments that correspond to the selected subsets. When minimizing the total edge number of the GD we have to ensure that the representation does not change when not necessary in terms of minimality. Otherwise the GD consists of edges that could be concatenated. This can happen because the solution of the \textsc{Set-Cover} in general is not unique. To maintain one representation as long as possible we check if the representation $R_{old}$ between the previous two time stamps still represents all segments between the current two timestamps and if the size of $R_{old}$ equals the size of the current solution. In this case we maintain $R_{old}$ and proceed with the next time stamp. This additional step is not necessary when minimizing the total edge length as the sum of the length of a minimal length representation between a series of consecutive time stamps within a time frame from start $t_s$ to end time $t_e$ is at most the minimal length of a representation looking at the whole interval $[t_s, t_e]$ at once.

\begin{lemma}
For each time stamp we can compute the $k$ sets of the \textsc{Set-Cover} instance in $\mathcal{O}(k^2)$ time.
\end{lemma}

From the previous lemmas we can conclude the following theorem:
\begin{theorem}\label{theorem:alg_fr}
Given a GD instance using equal time distance, we can compute in $\mathcal{O}((k^5 + k^4 \log n)n)$ time $\mathcal{O}(k^3n)$ \textsc{Set-Cover} instances each of size $|\mathcal{U}| = k$ and $|\mathcal{S}| = k$ the solution of which solves the GD instance.
\end{theorem}

\subparagraph{Similar-Time Distance}
Equal-time distance may be too restrictive for some applications, for example for entities which travel the exact same route, but such that each entity reaches each position with a small delay.  We introduce the similar-time distance as an additional similarity measure for such settings.

\begin{definition}\label{def:similar-time-distance}
Given two trajectories $P$ and $Q$, where $P$ has size $m$ and $Q$ has size $n$. We say that $P$ has $\alpha$-similar-time distance at most $d$ to $Q$ if for all time stamps $t_i$, $i = 1,2,...,n-1$ there exists $\hat{t}_i \in [t_i - \alpha, t_i+\alpha]$, $\hat{t}_{i+1} \in [t_{i+1}-\alpha, t_{i+1} + \alpha]$ with $dist(q_i,\hat{p}_i) \leq d$, $dist(q_{i+1},\hat{p}_{i+1}) \leq d$ where $\hat{p}_i$ is the location of $P$ at time $\hat{t}_i$ and for all $t \in [\hat{t}_i,\hat{t}_{i+1}]$ $P$ stays within a $d$-tube around $q_iq_{i+1}$ and $\hat{t}_{i} \leq \hat{t}_{i+1}$.
\end{definition}

For computing a GD with similar-time distance as similarity criteria we use the same approach as for equal-time distance and only adapt the construction of the sets for the \textsc{Set Cover} instance for each time stamp by comparing each segment with the segments of the other trajectory in the interval given by the allowed time shift.

In addition to the conditions of Definition~\ref{def:similar-time-distance} one can demand that the length of the time interval between $\hat{t}_i$ and $\hat{t}_{i+1}$ does not fall below a given minimal value, for instance a fraction of the length of the interval $[t_i, t_{i+1}]$. Thus we still allow a time shift when two entities travel along a similar route, but additionally demand that the duration of traveling a similar route does not differ too much:

\begin{definition}\label{def:a_b_similar-time-distance}
Given two trajectories $P$ and $Q$, where $P$ has size $m$ and $Q$ has size $n$. We say that $P$ has $\alpha-\beta$-similar-time distance at most $d$ to $Q$ if for all time stamps $t_i$, $i = 1,2,...,n-1$ there exists $\hat{t}_i \in [t_i - \alpha, t_i+\alpha]$, $\hat{t}_{i+1} \in [t_{i+1}-\alpha, t_{i+1} + \alpha]$ with $dist(q_i,\hat{p}_i) \leq d$, $dist(q_{i+1},\hat{p}_{i+1}) \leq d$ where $\hat{p}_i$ is the location of $P$ at time $\hat{t}_i$ and for all $t \in [\hat{t}_i,\hat{t}_{i+1}]$ $P$ stays within a $d$-tube around $q_iq_{i+1}$ and $\hat{t}_{i+1} - \hat{t}_i \geq \beta(t_{i+1} - t_i)$.
\end{definition} 