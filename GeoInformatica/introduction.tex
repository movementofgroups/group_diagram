\section{Introduction}\label{sec:intro}

%\todo{add more motivation to the problem}
The amount of available movement data, such as GPS data collected by mobile devices, has increased massively during the last years. Movement ecology, for instance, is a growing field at the edge of biology and GIScience which benefits from new possibilities of tagging animals with smart devices collecting location data and several other parameters. Analysing these data poses new algorithmic challenges~\cite{cma}. One of the challenges is to represent the large amount of data, such that the representation is as compact as possible, but still preserves the information needed for further analysis and processing. In particular, this challenge arises for movement trajectories of several interacting entities.

In this paper we take on this challenge and present a compact representation for data of groups of moving objects.
Our approach is the following. We assume we are given the trajectories of several moving objects, called \emph{entities}, each of which is given by a series of sampled locations. By linearly interpolating in between each two consecutive time stamps, the corresponding trajectory becomes a polygonal line.
We assume we are given $k$ trajectories, each of complexity $n$ (number of vertices) forming one or several (overlapping, i.e., splitting and merging) groups. We introduce the \emph{group diagram} (GD) as a means of a compact representation of these groups.

The GD is a geometric graph (see the next section for precise definitions) that represents all of the input trajectories, in the sense that for each input trajectory there is a similar path in the group diagram. Furthermore, the group diagram should have minimal size with respect to the number/length of the edges in the geometric graph. As similarity measure we propose to use three common similarity measures for trajectories, namely equal-time, similar-time and the \fr.
Figure~\ref{fig:groupDiagram} gives an example, where the subtrajectories forming a minimal GD % for the given trajectories
are marked in red.

\begin{figure}
\centering
\includegraphics[width=0.55\columnwidth]{Figures/groupDiagram}
	\caption{Example of a group diagram.}
	\label{fig:groupDiagram}
\end{figure}

% Overview
In the next section we precisely define and study the computational complexity of this problem.
As it turns out, the problem of computing a minimal group diagram is NP-hard for both the equal-time and the \fr.
Hence in the following sections, we develop approximation algorithms, which we then experimentally evaluate on real and synthetic data sets.

Preliminary results of this paper are presented in \cite{DBLP:conf/gis/BuchinKK18}. Here we extend this presentation, in particular with a detailed discussion on the complexity of the problem and an experimental evaluation for GDs based on the \fr.

%\begin{figure}
%\includegraphics[scale = 1]{Figures/groupDiagram}
%	\centering
%	\caption{Illustration of merging and splitting groups and their representation as GD.}
%	\label{fig:illustration}
%\end{figure}



\subsection{Related work}\label{sec:rw}
Two related notions to the GD are the grouping structure and flow diagrams.
The grouping structure is defined as the unique graph representing all density-con\-nected groups traveling at equal-time~\cite{grouping}.
It can be seen as a specialization of the GD which uses equal-time distance as similarity and density connectedness as inner group distance.
As such the grouping structure is well suited for grouping entities moving together, but not for entities travelling, e.g. commuting or migrating, at different times. Also, in some settings, such as commuting, pairwise distance is more suitable than density connectedness. For instance, commuters moving in slow traffic along a major road would form a group in the grouping structure, even though individual commuters in this group may be far away from each other.
Buchin et al. gave efficient algorithms for computing the grouping structure, analyzed its complexity,
and demonstrated its usefulness on a real-world data set (of deer, elk, and cattle).
The grouping structure was later generalized by Kostitsyna et al. for the geodesic distance~\cite{grouping2}
and by von Goethem et al. for varying parameters~\cite{grouping3}. Furthermore, Kreveld et al.~\cite{refined_grouping2016} refined the grouping structure by restricting the density connectedness to within a group. Wiratma et al.~\cite{grouping_exp2018} experimentally compared the refined and original grouping structure.

A flow diagram is a minimal (in the number of vertices) diagram representing segmentations of all input trajectories. In a flow diagram nodes represent criteria and edges transitions between criteria~\cite{flow}.
The flow diagram can be seen as generalization of the GD (after switching between vertices and edges) where criteria are more general than small distance of the trajectories.
Buchin et al. showed that deciding if a flow diagram of a certain size exists is NP-hard (even W[1]-hard in the number of trajectories). Hence, they give efficient heuristics for computing flow diagrams and evaluate these on a real-world data set (of football players).

Two further related approaches are presented in~\cite{herds} and~\cite{olap}. Huang et. al.~\cite{herds} propose the concept of a herd, which allows splitting and merging. In contrast to our approach, they use density-connected groups and define splitting and merging of groups based on a definition of recall and precision measure of a herd. Baltzer et. al.~\cite{olap} present OLAP for trajectories for detecting groups based on geographic overlap and intersection. Their approach is based on mapping the trajectories to a 2dimensional spatial grid. 

Computing a GD using the \fr\ is also highly related to map construction algorithms, where the goal is to determine the underlying network of a set of trajectories~\cite{map-construction}, as we discuss in Section~\ref{sec:fr}.
%
Similar modeling choices (i.e. choice of what constitutes edges, which similarity measure, and which minimality condition) occur in the problem of finding a representative (e.g. median, middle, ...) trajectory of a set of similar trajectories.

