\section{Introduction}\label{sec:intro}

\subsection{Definition and Terminology}\label{sec:def}
%\todo{add more motivation to the problem}
The amount of available movement data such as GPS data collected by mobile devices has increased massively during the last years. Movement ecology, for instance, is a growing field at the edge of biology and GIScience which benefits from new possibilities of tagging animals with smart devices collecting location data and several other parameters. Analysing these data poses new algorithmic challenges~\cite{cma}. One of the challenges is to represent the large amount of data, such that the representation is as compact as possible, but still preserves the information needed for further analysis and processing. In particular, this challenge arises for movement trajectories of several interacting entities.

In this paper, we take on this challenge and present a compact representation for data of groups of moving objects.

A moving object, called \emph{entity}, is described by its location at $n$ time stamps. By linearly interpolating in between each two consecutive time stamps, the corresponding trajectory becomes a polygonal line.
Given $k$ trajectories, each of complexity $n$ (number of vertices) forming one or several (overlapping, i.e., splitting and merging) groups. We introduce the \emph{group diagram} as a means of a compact representation of these groups.

%\begin{figure}
%\includegraphics[scale = 1]{Figures/groupDiagram}
%	\centering
%	\caption{Illustration of merging and splitting groups and their representation as GD.}
%	\label{fig:illustration}
%\end{figure}

First, let us introduce the terminology we use.
%\paragraph*{Terminology}
%We will use the following terms:
A \emph{trajectory} is an ordered sequence of positions in time and space.
A \emph{subtrajectory} refers to an arbitrary but connected part of the trajectory starting and ending at arbitrary points along the trajectory. We call a subtrajectory starting and ending at vertices a \emph{restricted} subtrajectory. A subtrajectory that is not necessarily restricted is an \emph{arbitrary} subtrajectory. A \emph{segment} refers to a subtrajectory consisting only of two consecutive vertices of the trajectory and the linear interpolation between them. A \emph{cluster} is a set of trajectories that are similar (under some similarity measure) to one \emph{representative} of the cluster.
%
A cluster representative is thus a subtrajectory within a cluster to which all other subtrajectories in the cluster have distance at most $d$. A \emph{cluster curve} is a subtrajectory within some cluster. % (not necessarily starting and ending at vertices).
Given a cluster representative $\tau$ we denote by $c(\tau)$ the set of all cluster curves of the corresponding cluster. For a set of representatives $A$, $c(A)$ is the union of all cluster curves represented by a representative in $A$.


We propose the following general definition for a group diagram:
\begin{definition}\label{def:gd}
 A group diagram (GD) is a geometric graph with vertices augmented by a temporal component, that represents all input trajectories $\mathcal{T}$.
 We say the graph represents a trajectory $T \in \mathcal{T}$ if there is a similar path $P$ in the graph,
 that is $T$ and (the geometric representation of) $P$ are similar under a given similarity measure.
 We say a GD is minimal if it is minimal in size, either with respect to its number
 of edges or the total length of edges.
\end{definition}

The definition allows to make several choices:
how to represent vertices and edges of the GD, the similarity measure, and whether to minimize number or (geographical) length of edges.
%
We consider GD that are built from the input trajectories, i.e., edges of the GD are represented by subtrajectories of the input and two edges share a vertex if the endpoints of the corresponding subtrajectories are within distance $d$ from each other. Endpoints of edges with no $d$-distance neighbor have degree one.
Vertices in the graph are thus embedded as the set of endpoints of incident edges.
We will use such graphs in the following.
Note that we could transform these into planar embedded graphs, for instance by connecting all points of a point set $S$ of a vertex to the midpoint of $S$.
% or its convex hull.
Note also that we require representatives to consist of parts of the input as these give realistic representatives. % (not requiring this would allow representatives to go through impassable regions).

As similarity measure, we consider three popular measures on trajectories: the \fr, equal-, and similar-time distance.
%
Figure~\ref{fig:groupDiagram} illustrates several trajectories, where the subtrajectories forming a minimal GD for the given trajectories are highlighted in red.
The generality of our definition allows to apply it in different settings, e.g. to entities moving at the same or at different times.

Minimizing the number of edges or their total length seems intuitively reasonable.
However, both can lead to unwanted effects and we need to make further requirements to prevent these.
The effects are the following (see Figure~\ref{fig:strangeEffects} for an illustration):
\begin{itemize}
\label{list:additional_conditions}
  \item When minimizing number of edges, if two trajectories are close to each other only partially, a smallest possible group diagram consist of the two trajectories rather than representing their similar parts with a joint edge.
  \item When minimizing length, even for a single trajectory the shortest representative would be a series of points at distance $2d$.
\end{itemize}

%Figure \ref{fig:strangeEffects} illustrates these effects. A minimal group diagram for the given trajectories is highlighted in red.

%\begin{figure}
%\includegraphics[scale=0.6]{Figures/strange_effects}
%\centering
%	\caption{Unintuitive group diagrams (highlighted in red) fulfilling the criteria of minimal edge length (left) and minimal number of edges (right).}
%	\label{fig:strangeEffects}
%\end{figure}

\begin{figure}
\includegraphics[width=0.32\textwidth]{Figures/groupDiagram}
	\caption{Example of a GD.}
	\label{fig:groupDiagram}
\end{figure}

\begin{figure}
\includegraphics[width=0.47\textwidth]{Figures/strange_effects}
	\caption{unintuitive GD with minimal number of edges (left) and minimal edge length (right).}
	\label{fig:strangeEffects}
\end{figure}

To prevent these effects, we introduce the following requirements for the two different minimization goals:
\begin{itemize}
  \item For minimizing number of edges we require that the minimality criterion is fulfilled also for every local part of the input: \\
Given a GD $\mathcal{G}$, an arbitrary subtrajectory $\tau$ of one of the representatives in $\mathcal{G}$, and the corresponding cluster $c(\tau)$. We use $G_\tau^* := c(c(\tau))$ to denote the union of all clusters represented by a curve in $c(\tau)$.
We intersect $G_\tau^*$ with the GD $\mathcal{G}$ to obtain all representatives $A_\tau := \mathcal{G} \cap G_\tau^*$.
The \emph{local minimality criterion} now requires that for each $\tau$ as described above the resulting set $A_\tau$ is a minimal representation for $c(A_\tau)$.

\item For minimizing length we require that no clusters are artificially split up to reduce the length.
   Formally, we require that no subgraph of the GD can be contracted, i.e., substituted by a subgraph of smaller size (but possibly larger length).
\end{itemize}

In the following, we always consider the problem with the corresponding additional requirements.
% depending on whether we measure size by number or length of edges.


\subsection{Related work}\label{sec:rw}
Two related notions to the GD are the grouping structure and flow diagrams.
The grouping structure is the unique graph representing all density-connected groups traveling at equal-time~\cite{grouping}.
It can be seen as a specialization of the GD which uses equal-time distance as similarity and density connectedness as inner group distance.
As such the grouping structure is well suited for grouping entities moving together, but not for entities travelling, e.g. commuting or migrating, at different times. Also, in some settings, such as commuting, pairwise distance is more suitable than density connectedness.
Buchin et al. gave efficient algorithms for computing the grouping structure, %analyzed its complexity
and demonstrated its usefulness on a real-world data set (of deer, elk, and cattle).
The grouping structure was later generalized by Kostitsyna et al. for the geodesic distance~\cite{grouping2}
and by von Goethem et al. for varying parameters~\cite{grouping3}.

A flow diagram is a minimal (in the number of vertices) diagram representing segmentations of all input trajectories. In a flow diagram nodes represent criteria and edges transitions between criteria~\cite{flow}.
The flow diagram can be seen as generalization of the GD (after switching between vertices and edges) where criteria are more general than small distance of the trajectories.
Buchin et al. showed that deciding if a flow diagram of a certain size exists is NP-hard (even W[1]-hard in the number of trajectories). Hence they give efficient heuristic for computing flow diagrams and evaluate these on a real-world data set (of football players).

Two further related approaches are presented in~\cite{herds} and~\cite{olap}. Huang et. al.~\cite{herds} propose the concept of a herd, which allows splitting and merging, and in contrast to our approach are based on density-connected groups. Baltzer et. al.~\cite{olap} present OLAP for trajectories, in particular for detecting groups based on geographic overlap and intersection.

Computing a GD using the \fr\ is also highly related to map construction algorithms, where the goal is to determine the underlying network of a set of trajectories~\cite{map-construction}, as we discuss in Section~\ref{sec:fr}.
%
Similar modeling choices (i.e. choice of what constitutes edges, which similarity measure, and which minimality condition) occur in the problem of finding a representative (e.g. median, middle, ...) trajectory of a set of similar trajectories.

\subsection{Computational Complexity}\label{sec:complexity} %

By a reduction from the known NP-complete \textsc{Dominating-Set} problem for a grid graph \cite{unit-disk} we can show that the decision problem for GD is NP-complete for all variants we consider.

\begin{definition}\label{def:ds}
Given a graph $G=(V,E)$. A dominating set $D$ is a subset of $V$, such that every vertex from $V$ not in $D$ is adjacent to at least one vertex in $D$. Given an integer~$s$, the \textsc{Dominating-Set} problem is to decide whether there is a dominating set of size~$j$.
\end{definition}
\textsc{Dominating-Set} is known to be NP-complete. For the proof of Theorem \ref{theorem:NP_fr} we use a special case of \textsc{Dominating-Set} which is still NP-complete but has a stronger geometrical structure which we will use for our reduction.
\begin{definition}\label{def:grid}
Let $\mathcal{D}=\{(x,y)\in \mathbb{R}^2 | \text{$x$ and $y$ are multiples of d}\}$, where $d\in \mathbb{R}$. A grid graph is a Graph $G=(V,E)$ where $V\subset \mathcal{D}$ and there is an edge between two vertices if and only if they are within distance $d$.
\end{definition}
The following result from \cite{unit-disk} is the last building block we need.

\begin{lemma}\label{lemma:ds_on_grid}
\textsc{Dominating-Set} for grid graphs is NP-complete.
\end{lemma}

%With these preliminary remarks we can prove that \textsc{Group-Diagram} is NP-complete by a reduction from \textsc{Dominating-Set} for grid graphs.

\begin{theorem}\label{theorem:NP_fr}
Given an integer $l$, deciding whether there exists a GD of size $l$ is NP-complete for both $l$ denoting the edge length and $l$ denoting the edge number, and for both \fr\ and equal-time distance as similarity criteria.
\end{theorem}

\begin{proof}
%Given a geometric graph $G$ embedded in the plane with $m$ non-crossing straight-line edges and a trajectory $\tau$ with $n$ segments. Then we can decide if there exists a path in $G$ with \fr\ at most $d$ to $\tau$ in $\mathcal{O}(mn\log n)$ time. Given a group diagram $G$ consisting of $l$ segments from the input trajectory. For each intersection of two segments and for each breakpoint of a non-straight edge we add a vertex in the graph $G$ inducing a Graph $\tilde{G}$ with at most $l^2$ non-crossing straight-line edges. Therefore we can decide whether there is a similar path in $G$ for every input trajectory in $\mathcal{O}(kl^2n\log n)$ time. Thus \textsc{Group Diagram} is in NP.
We give a proof for \fr\ as similarity measure and note that the proof for equal-time distance is similar.

First, we show that \textsc{Group Diagram} is in NP.
Let $G = (V, E)$ be a plane graph with vertices embedded as points in the plane that are connected by straight-line edges and a trajectory $\tau$ with $n$ segments. Then, we can decide if there is a path in $G$ with \fr\ at most $d$ to $\tau$ in polynomial time with respect to $n$ and the complexity of $G$ by performing a graph exploration for each segment of $\tau$ consecutively as described in \cite{map}. Given a group diagram $\mathcal{G}$ consisting of $l$ segments, we consider the planar embedded graph $G$ given by its representation augmented with a central vertex for each node. The complexity of $G$ remains $\mathcal{O}(l)$. A trajectory $\tau$ can be represented by the GD $\mathcal{G}$ if there is a similar path to it in the graph $G$, which we can test in polynomial time. Thus, \textsc{Group Diagram} is in NP.

Given an arbitrary grid graph $G=(V,E)$ with $V=\{v_1,v_2,...,v_k\}$. We construct an instance of \textsc{Group-Diagram} as follows. For each $v \in V$ place a horizontal segment with length less than $d$ in the plane where the coordinates of the starting point equals the coordinates of $v$.
See Figure~\ref{fig:reductionSketch} for an example.
\begin{figure}
\includegraphics[scale=0.6]{Figures/reductionSketch}
\caption{A grid graph and its corresponding placement of segments. The solution of the \textsc{Dominating Set} and the \textsc{Group Diagram} instances are highlighted in red.} %\maike{add some labels}
\label{fig:reductionSketch}
\end{figure}
If two vertices in $G$ are adjacent, then the corresponding segments have \fr\ at most $d$ as the segments have equal length and are parallel. On the other hand if two (sub)segments are within \fr\ at most $d$, then the corresponding vertices in $G$ are adjacent.
% This follows from the construction of the placement.
If two vertices $v_1$, $v_2$ are not adjacent in $G$, any point along the corresponding segment to $v_1$ has distance greater than $d$ to any point along the segment corresponding to the segment $v_2$.

Now, we show NP-hardness of \textsc{Group-Diagram} when minimizing the total number of edges. Suppose there is a dominating set $I=\{v_{i_1},...v_{i_l}\}$ of size $l$ for $G$. Let $\hat{I} = \{s_{i_1},...s_{i_l}\}$ denote the set of the corresponding segments. For each $s \notin \hat{I}$ there is a vertex $v\in I$ adjacent to the corresponding vertex $v_s$ and it follows that $s$ and $s_v$ are within distance $d$. Therefore, any input trajectory is represented by at least one ``path'' consisting of only one input segment. Reversely, given a set $J$ of (sub-)segments of size $l$ representing the whole data. Particularly for each starting point $p$ of the constructed segments, there is a (sub-)segment in $J$ with starting point $q$, where $dist(p,q) \leq d$. As the starting points of the segments equal the vertices in the graph, a dominating set of size $l$ for $G$ consist of vertices corresponding to the (sub-)segments of $J$.
Note that a minimal solution in this setting also fulfills the local minimality criterion. This follows from the observation that for each subtrajectory $\tau_s$ of a given trajectory $\tau$, the cluster $c(\tau_s)$ contains the same (shrunken) cluster curves as $c(\tau)$. Therefore for any representative $r$ of the group diagram the part $G^*$ relative to any subtrajectory of $r$ is represented in a minimal way by the corresponding subsegments of the group diagram.

For proving NP-hardness of \textsc{Group-Diagram} when minimizing total edge length we fix the distance $d=1$ and the length $j$ of the segments induced by the vertices of $G$ to $j = 1$. To fulfill the additional condition for group diagrams of minimal total edge length, observe that a solution consists of whole segments as the segments are parallel and starting points are within distance $d = 1 = j$. Hence, a representation of two segments within distance $d$ with possibly smaller length than $j$ must consist of more than one representative and can always be replaced by a smaller subgraph in the GD by choosing one segment as representative.  Thus, a solution of \textsc{Group-Diagram} with total edge length $l$ consists of exactly $l$ segments, where the corresponding vertices are a solution of \textsc{Dominating-Set} of size $l$ in $G$. Obviously, a solution of \textsc{Dominating-Set} of size $l$ implies a solution of \textsc{Group-Diagram} of total edge length $l$. This proves NP-hardness of the problem of finding a minimal group diagram for a given set of input trajectories. Note that NP-hardness is shown for the special case of parallel, equal size trajectories with only one segment (and in case of minimal edge length with distance $1$).

\end{proof}
In the following sections we give approximation algorithms and their experimental evaluation on a real data set. 